<?php
class Pengurus_model extends CI_model
{
  function getProkerPengurus($id){
    $user = $this->db->get_where('proker',['id_user'=> $id])->result_array();
    return $user;
  }
  function getProker($id){
    $user = $this->db->get_where('proker',['id_proker'=> $id])->row_array();
    return $user;
  }
  function updateProkerPengurus($nama,$ket,$b,$tglm,$tglp,$id){
    $this->db->set('nama_proker',$nama);
    $this->db->set('keterangan',$ket);
    $this->db->set('biaya',$b);
    $this->db->set('tgl_mulai',$tglm);
    $this->db->set('tgl_pelaksanaan',$tglp);
    $this->db->where('id_proker',$id);
    if($this->db->update('proker')){
      return true;
    }else{
      return false;
    }
  }
  function insertProker($nama,$k,$b,$tm,$tp,$iduser){
    $s = "menunggu";
    $data = array(
       'nama_proker' => $nama,
       'keterangan' =>$k,
       'status' => $s,
       'biaya' => $b,
       'tgl_mulai' =>$tm,
       'tgl_pelaksanaan' => $tp,
       'id_user' => $iduser
     );
     if($this->db->insert('proker', $data)){
       return true;
     }else{
       return false;
     }
  }
  function deleteProker($id){
    $this->db->where('id_proker',$id);
    if($this->db->delete('proker')){
      return true;
    }else{
      return false;
    }
  }
  // artikel
  function getKatgeori(){
    $user = $this->db->get('kategori')->result_array();
    return $user;
  }
  function getArtikel($id){ // tanpa lim
    $this->db->select('
         artikel.id_artikel,kategori.kategori,jabatan.jabatan,artikel.tanggal,artikel.judul,artikel.isi,
         artikel.gambar,artikel.tipe,artikel.status,artikel.tema,artikel.id_user,artikel.id_kategori,user.id_jabatan');
     $this->db->join('kategori','artikel.id_kategori = kategori.id_kategori');
     $this->db->join('user', 'artikel.id_user = user.id_user');
     $this->db->join('jabatan', 'user.id_jabatan = jabatan.id_jabatan');
     $this->db->from('artikel');
     $this->db->where('artikel.id_artikel',$id);
     return $query = $this->db->get()->row_array();
  }
  function getPengurusArtikel($id,$limit,$start){ // dengan lim
    $this->db->select('
         artikel.id_artikel,kategori.kategori,jabatan.jabatan,artikel.tanggal,artikel.judul,artikel.isi,
         artikel.gambar,artikel.tipe,artikel.status,artikel.tema,artikel.id_user,artikel.id_kategori,user.id_jabatan');
     $this->db->join('kategori','artikel.id_kategori = kategori.id_kategori');
     $this->db->join('user', 'artikel.id_user = user.id_user');
     $this->db->join('jabatan', 'user.id_jabatan = jabatan.id_jabatan');
     $this->db->from('artikel');
     $this->db->limit($limit,$start);
     $this->db->where('artikel.id_user',$id);
     $this->db->order_by('artikel.tanggal','DESC');
     return $query = $this->db->get()->result_array();
  }
  function getAllArtikel($limit,$start,$id){
    if($id == 8){
    $this->db->select('
         artikel.id_artikel,kategori.kategori,jabatan.jabatan,artikel.tanggal,artikel.judul,artikel.isi,
         artikel.gambar,artikel.tipe,artikel.status,artikel.tema,artikel.id_user,user.id_jabatan');
     $this->db->join('kategori','artikel.id_kategori = kategori.id_kategori');
     $this->db->join('user', 'artikel.id_user = user.id_user');
     $this->db->join('jabatan', 'user.id_jabatan = jabatan.id_jabatan');
     $this->db->from('artikel');
     $this->db->limit($limit,$start);
     $this->db->order_by('artikel.tanggal','DESC');
     return $query = $this->db->get()->result_array();
   }
  }
  function getCountPengurusArtikel($id){
    $this->db->select('
         artikel.id_artikel,kategori.kategori,jabatan.jabatan,artikel.tanggal,artikel.judul,artikel.isi,
         artikel.gambar,artikel.tipe,artikel.status,artikel.tema,artikel.id_user,artikel.id_kategori,user.id_jabatan');
     $this->db->join('kategori','artikel.id_kategori = kategori.id_kategori');
     $this->db->join('user', 'artikel.id_user = user.id_user');
     $this->db->join('jabatan', 'user.id_jabatan = jabatan.id_jabatan');
     $this->db->from('artikel');
     $this->db->where('artikel.id_user',$id);
     return $query = $this->db->get()->num_rows();
  }
  function getCountAllArtikel(){
    $this->db->select('
         artikel.id_artikel,kategori.kategori,jabatan.jabatan,artikel.tanggal,artikel.judul,artikel.isi,
         artikel.gambar,artikel.tipe,artikel.status,artikel.tema,artikel.id_user,user.id_jabatan');
     $this->db->join('kategori','artikel.id_kategori = kategori.id_kategori');
     $this->db->join('user', 'artikel.id_user = user.id_user');
     $this->db->join('jabatan', 'user.id_jabatan = jabatan.id_jabatan');
     $this->db->from('artikel');
     $this->db->order_by('artikel.tanggal','DESC');
     return $query = $this->db->get()->num_rows();
  }
  function updateArtikel($idc,$tgl,$judul,$isi,$gambar,$tipe,$tema,$id){
    $this->db->set('id_kategori',$idc);
    $this->db->set('tanggal',$tgl);
    $this->db->set('judul',$judul);
    $this->db->set('isi',$isi);
    $this->db->set('gambar',$gambar);
    $this->db->set('tipe',$tipe);
    $this->db->set('tema',$tema);
    $this->db->where('id_artikel',$id);
    if($this->db->update('artikel')){
      return true;
    }else{
      return false;
    }
  }
  function insertArtikel($idc,$idu,$tgl,$judul,$isi,$gambar,$tipe,$tema){
    $s = "draft";
    $data = array(
       'id_kategori' =>$idc,
       'id_user' => $idu,
       'tanggal' => $tgl,
       'judul' =>$judul,
       'isi' => $isi,
       'gambar' => $gambar,
       'tipe' =>$tipe,
       'status' => $s,
       'tema' => $tema
     );
     if($this->db->insert('artikel', $data)){
       return true;
     }else{
       return false;
     }
  }
  function deleteArtikel($id){
    $this->db->where('id_artikel',$id);
    if($this->db->delete('artikel')){
      return true;
    }else{
      return false;
    }
  }
  function updateStatusArtikel($id,$ida,$status){
    if($id == 8){
      $this->db->set('status',$status);
      $this->db->where('id_artikel',$ida);
      if($this->db->update('artikel')){
        return true;
      }else{
        return false;
      }
    }
  }
  function getPengurus($id){
    $user = $this->db->get_where('user',['id_user'=> $id])->row_array();
    return $user;
  }
  function getDepartemen($id){
    $user = $this->db->get_where('jabatan',['id_jabatan'=> $id])->row_array();
    return $user;
  }
  function getAnggota($pos,$flag,$id){
    $this->db->select('anggota.*,jabatan.*');
    $this->db->join('jabatan','anggota.id_jabatan = jabatan.id_jabatan');
    $this->db->from('anggota');
    $this->db->where('anggota.id_jabatan',$id);
    if($flag){
      $this->db->where('anggota.posisi',$pos);
    }
    return $this->db->get();
  }
  function getAnggotaByID($id){
    $this->db->select('anggota.*,jabatan.*');
    $this->db->join('jabatan','anggota.id_jabatan = jabatan.id_jabatan');
    $this->db->from('anggota');
    $this->db->where('id_anggota',$id);
    return $this->db->get()->row_array();
  }
  function insertAnggota($nama,$nik,$p,$g,$s,$idj){
    $data = array(
       'nama' => $nama,
       'NIK' => $nik,
       'id_jabatan' => $idj,
       'posisi' =>$p,
       'foto' => $g,
       'status'=> $s
     );
     if($this->db->insert('anggota', $data)){
       return true;
     }else{
       return false;
     }
  }
  function updateAnggota($nama,$nik,$p,$g,$s,$id){
     $this->db->set('nama', $nama);
     $this->db->set('NIK', $nik);
     $this->db->set('posisi', $p);
     $this->db->set('foto', $g);
     $this->db->set('status', $s);
     $this->db->where('id_anggota', $id);
     if($this->db->update('anggota')){
       return true;
     }else{
       return false;
     }
  }
  function deleteAnggota($id){
    $this->db->where('id_anggota',$id);
    if($this->db->delete('anggota')){
      return true;
    }else{
      return false;
    }
  }
  // ---- end anggota ..//
  function updateProfile($nama,$email,$telp,$foto,$id){
     $this->db->set('nama', $nama);
     $this->db->set('email', $email);
     $this->db->set('telp', $telp);
     $this->db->set('foto', $foto);
     $this->db->where('id_user', $id);
     if($this->db->update('user')){
       return true;
     }else{
       return false;
     }
  }
  function getAkses($id){
    $user = $this->db->get_where('akses',['id_user'=> $id])->row_array();
    return $user;
  }
// upload img untuk artikel
  public function uploadImage($nama)
   {
       $config['upload_path']          = './upload/artikel/';
       $config['allowed_types']        = 'gif|jpg|jpeg|png';
       $config['file_name']            = $nama;
       $config['overwrite']			= true;
       $config['max_size']             = 20240; // 1MB
       // $config['max_width']            = 1024;
       // $config['max_height']           = 768;

       $this->load->library('upload',$config);

       if ($this->upload->do_upload('foto')) {
           return $this->upload->data("file_name");
       }else{
         $error = $this->upload->display_errors();
          $this->session->set_flashdata('error', "<div class='alert alert-danger' role='alert'>".$error."</div>");
         return "default.jpg";
       }
   }

   // upload image untuk edit profile
   public function uploadImageProfile($nama)
    {
        $config['upload_path']          = './upload/adminpic/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['file_name']            = $nama;
        $config['overwrite']			= true;
        $config['max_size']             = 20240; // 1MB
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;

        $this->load->library('upload',$config);

        if ($this->upload->do_upload('foto')) {
            return $this->upload->data("file_name");
        }else{
          $error = $this->upload->display_errors();
           $this->session->set_flashdata('error', "<div class='alert alert-danger' role='alert'>".$error."</div>");
          return "default.jpg";
        }
    }
    
    function getDataExpo($limit,$start){ // dengan lim
      $this->db->select('dataexpo.*,jurusan.jurusan,jabatan.jabatan');
       $this->db->join('jurusan','jurusan.id_jurusan = dataexpo.id_jurusan');
       $this->db->join('jabatan', 'jabatan.id_jabatan = dataexpo.id_jabatan');
       $this->db->from('dataexpo');
       $this->db->order_by('dataexpo.id_dataexpo','DESC');
       $this->db->limit($limit,$start);
       return $query = $this->db->get()->result_array();
    }

    function getDataExpoById($id){
      $user = $this->db->get_where('dataexpo',['id_dataexpo'=> $id])->row_array();
      return $user;
    }

    function getAllExpo(){
      $user = $this->db->get('dataexpo');
      return $user;
    }


    
    function updateStatusExpo($bayar,$status,$id){
      $this->db->set('bayar',$bayar);
      $this->db->set('status',$status);
      $this->db->where('id_dataexpo',$id);
      if($this->db->update('dataexpo')){
        return true;
      }else{
        return false;
      }
    }
    function deleteExpo($id){
      $this->db->where('id_dataexpo',$id);
      if($this->db->delete('dataexpo')){
        return true;
      }else{
        return false;
      }
    }

}
?>
