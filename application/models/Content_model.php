<?php
class Content_model extends CI_model
{
  function getBanner($id){
    $user = $this->db->get_where('banner',['id_banner'=> $id])->row_array();
    return $user;
  }
  function getAllBanner(){
    $user = $this->db->get('banner')->result_array();
    return $user;
  }
  function getSlider($id){
    $user = $this->db->get_where('slider',['id_slider'=> $id])->row_array();
    return $user;
  }
  function getAllSlider(){
    $this->db->select('
         artikel.id_artikel,artikel.judul,artikel.gambar,kategori.kategori');
     $this->db->join('kategori','artikel.id_kategori = kategori.id_kategori');
     $this->db->from('artikel');
     $this->db->where('artikel.tipe', 'event');
     $this->db->where('kategori.kategori!=', 'Agenda');
     $this->db->where('artikel.status!=','draft');
     $this->db->limit(5,0);
     $this->db->order_by('artikel.tanggal','DESC');
     return $query = $this->db->get()->result_array();
  }
  function getAllContact(){
    $user = $this->db->get('contact')->row_array();
    return $user;
  }
  function updateBanner($nama,$g,$l,$k,$t,$id){
    $this->db->set('nama',$nama);
    $this->db->set('gambar',$g);
    $this->db->set('link',$l);
    $this->db->set('keterangan',$k);
    $this->db->set('tipe',$t);
    $this->db->where('id_banner',$id);
    if($this->db->update('banner')){
      return true;
    }else{
      return false;
    }
  }
  function insertBanner($nama,$g,$l,$k,$t){
    $data = array(
       'nama' => $nama,
       'gambar' =>$g,
       'link' => $l,
       'keterangan' => $k,
       'tipe' => $t
     );
     if($this->db->insert('banner', $data)){
       return true;
     }else{
       return false;
     }
  }
  function deletebanner($id){
    $this->db->where('id_banner',$id);
    if($this->db->delete('banner')){
      return true;
    }else{
      return false;
    }
  }
  function updateSlider($judul,$k,$g,$id){
    $this->db->set('judul',$judul);
    $this->db->set('keterangan',$k);
    $this->db->set('gambar',$g);
    $this->db->where('id_slider',$id);
    if($this->db->update('slider')){
      return true;
    }else{
      return false;
    }
  }
  function insertSlider($judul,$k,$g){
    $data = array(
       'judul' => $judul,
       'keterangan' => $k,
       'gambar' =>$g,
     );
     if($this->db->insert('slider', $data)){
       return true;
     }else{
       return false;
     }
  }
  function deleteslider($id){
    $this->db->where('id_slider',$id);
    if($this->db->delete('slider')){
      return true;
    }else{
      return false;
    }
  }
  function getIDArtikel(){
    $this->db->select('artikel.id_artikel');
    $this->db->from('artikel');
    $this->db->where('artikel.status!=','draft');
    return $query = $this->db->get()->result_array();
  }
  function getArtikel($id){

    $this->db->select('
         artikel.id_artikel,kategori.kategori,jabatan.jabatan,artikel.tanggal,artikel.judul,artikel.isi,
         artikel.gambar,artikel.tipe,artikel.status,artikel.tema,artikel.id_user,user.id_jabatan,user.foto');
     $this->db->join('kategori','artikel.id_kategori = kategori.id_kategori');
     $this->db->join('user', 'artikel.id_user = user.id_user');
     $this->db->join('jabatan', 'user.id_jabatan = jabatan.id_jabatan');
     $this->db->from('artikel');
     $this->db->where('artikel.id_artikel', $id);
     $this->db->order_by('artikel.tanggal','DESC');
     return $query = $this->db->get()->result_array();

  }
  function getAllPublishArtikel($limit,$start){

    $this->db->select('
         artikel.id_artikel,kategori.kategori,jabatan.jabatan,artikel.tanggal,artikel.judul,artikel.isi,
         artikel.gambar,artikel.tipe,artikel.status,artikel.tema,artikel.id_user,user.id_jabatan,user.foto');
     $this->db->join('kategori','artikel.id_kategori = kategori.id_kategori');
     $this->db->join('user', 'artikel.id_user = user.id_user');
     $this->db->join('jabatan', 'user.id_jabatan = jabatan.id_jabatan');
     $this->db->from('artikel');
     $this->db->where('artikel.status!=','draft');
     $this->db->limit($limit,$start);
     $this->db->order_by('artikel.tanggal','DESC');
     return $query = $this->db->get()->result_array();

  }
  function getCountAllPublishArtikel(){
    $this->db->select('
         artikel.id_artikel,kategori.kategori,jabatan.jabatan,artikel.tanggal,artikel.judul,artikel.isi,
         artikel.gambar,artikel.tipe,artikel.status,artikel.tema,artikel.id_user,user.id_jabatan');
     $this->db->join('kategori','artikel.id_kategori = kategori.id_kategori');
     $this->db->join('user', 'artikel.id_user = user.id_user');
     $this->db->join('jabatan', 'user.id_jabatan = jabatan.id_jabatan');
     $this->db->from('artikel');
     $this->db->where('artikel.status!=','draft');
     $this->db->order_by('artikel.tanggal','DESC');
     return $query = $this->db->get()->num_rows();
  }
  // by kategori
  function getArtikelByKategori($limit,$start,$kat){

    $this->db->select('
         artikel.id_artikel,kategori.kategori,jabatan.jabatan,artikel.tanggal,artikel.judul,artikel.isi,
         artikel.gambar,artikel.tipe,artikel.status,artikel.tema,artikel.id_user,user.id_jabatan,user.foto');
     $this->db->join('kategori','artikel.id_kategori = kategori.id_kategori');
     $this->db->join('user', 'artikel.id_user = user.id_user');
     $this->db->join('jabatan', 'user.id_jabatan = jabatan.id_jabatan');
     $this->db->from('artikel');
     $this->db->where('artikel.status!=','draft');
     $this->db->where('kategori.kategori',$kat);
     $this->db->limit($limit,$start);
     $this->db->order_by('artikel.tanggal','DESC');
     return $query = $this->db->get()->result_array();

  }
  function getSearchArtikel($limit,$start,$key){
    $this->db->select('
         artikel.id_artikel,kategori.kategori,jabatan.jabatan,artikel.tanggal,artikel.judul,artikel.isi,
         artikel.gambar,artikel.tipe,artikel.status,artikel.tema,artikel.id_user,user.id_jabatan,user.foto');
     $this->db->join('kategori','artikel.id_kategori = kategori.id_kategori');
     $this->db->join('user', 'artikel.id_user = user.id_user');
     $this->db->join('jabatan', 'user.id_jabatan = jabatan.id_jabatan');
     $this->db->from('artikel');
     $this->db->like('artikel.judul', $key);
     $this->db->or_like('jabatan.jabatan', $key);
     $this->db->or_like('artikel.tema', $key);
     $this->db->where('artikel.status!=','draft');
     $this->db->order_by('artikel.tanggal','DESC');
     $this->db->limit($limit,$start);
     return $query = $this->db->get()->result_array();
  }
  function getCountSearchArtikel($key){
    $this->db->select('artikel.id_artikel');
     $this->db->join('kategori','artikel.id_kategori = kategori.id_kategori');
     $this->db->join('user', 'artikel.id_user = user.id_user');
     $this->db->join('jabatan', 'user.id_jabatan = jabatan.id_jabatan');
     $this->db->from('artikel');
     $this->db->where('artikel.status!=','draft');
     $this->db->like('artikel.judul', $key);
     $this->db->or_like('jabatan.jabatan', $key);
     return $query = $this->db->get()->num_rows();
  }

  // ----///
  function getAllKategori(){
    $user = $this->db->get('kategori')->result_array();
    return $user;
  }
  function getCountKategori($idkat){
     $this->db->select('artikel.id_artikel');
     $this->db->from('artikel');
     $this->db->where('artikel.status!=','draft');
     $this->db->where('artikel.id_kategori',$idkat);
     return $query = $this->db->get()->num_rows();
  }
  function getAllAgenda(){

    $this->db->select('
         artikel.id_artikel,kategori.kategori,jabatan.jabatan,artikel.tanggal,artikel.judul,artikel.isi,
         artikel.gambar,artikel.tipe,artikel.status,artikel.tema,artikel.id_user,user.id_jabatan');
     $this->db->join('kategori','artikel.id_kategori = kategori.id_kategori');
     $this->db->join('user', 'artikel.id_user = user.id_user');
     $this->db->join('jabatan', 'user.id_jabatan = jabatan.id_jabatan');
     $this->db->from('artikel');
     $this->db->where('artikel.id_kategori',4);
     $this->db->where('artikel.status!=','draft');
     $this->db->limit(5);
     $this->db->order_by('artikel.tanggal','DESC');
     return $query = $this->db->get()->result_array();

  }
  function getArtikelProfileID(){
    $this->db->select('artikel.id_artikel,artikel.judul');
     $this->db->from('artikel');
     $this->db->where('artikel.tipe','profil');
     $this->db->where('artikel.status!=','draft');
     return $query = $this->db->get()->result_array();
  }
  function getProfile(){
    $this->db->select('
         artikel.id_artikel,kategori.kategori,jabatan.jabatan,artikel.tanggal,artikel.judul,artikel.isi,
         artikel.gambar,artikel.tipe,artikel.status,artikel.tema,artikel.id_user,user.id_jabatan');
     $this->db->join('kategori','artikel.id_kategori = kategori.id_kategori');
     $this->db->join('user', 'artikel.id_user = user.id_user');
     $this->db->join('jabatan', 'user.id_jabatan = jabatan.id_jabatan');
     $this->db->from('artikel');
     $this->db->where('artikel.status!=','draft');
     $this->db->where('artikel.tipe','profil');
     return $query = $this->db->get()->result_array();
  }
  function getAnggota($idj,$flag){
    $this->db->select('anggota.*,jabatan.*');
    $this->db->join('jabatan','anggota.id_jabatan = jabatan.id_jabatan');
    $this->db->from('anggota');
    $this->db->where('anggota.id_jabatan',$idj);
    if($flag){
      $this->db->limit(3,0);
    }else{
      $this->db->limit(3,3);
    }
    $this->db->order_by('anggota.posisi','DESC');
    return $this->db->get()->result_array();
  }
  function getCountAnggota($idj){
    $this->db->select('anggota.*,jabatan.*');
    $this->db->join('jabatan','anggota.id_jabatan = jabatan.id_jabatan');
    $this->db->from('anggota');
    $this->db->where('anggota.id_jabatan',$idj);
    return $this->db->get()->num_rows();
  }
  function getDepartemen(){
    $user = $this ->db->where('id_jabatan!=','1');
    $user = $this ->db->where('id_jabatan!=','2');
    $user = $this ->db->where('id_jabatan!=','3');
    $user = $this ->db->where('id_jabatan!=','9');
    $user = $this ->db->where('id_jabatan!=','10');
      $user = $this ->db->where('id_jabatan!=','12');
      $user = $this ->db->where('id_jabatan!=','13');
    $user = $this->db->get('jabatan')->result_array();
    return $user;
  }
  function getDepartemenByID($id){
    $this->db->select('jabatan.*,user.*');
    $this->db->join('jabatan','jabatan.id_jabatan = user.id_jabatan');
    $this->db->from('user');
    $this->db->where('user.id_jabatan',$id);
    return $this->db->get()->row_array();
  }
  function getPH(){
    $this->db->select('jabatan.*,user.*');
    $this->db->join('jabatan','jabatan.id_jabatan = user.id_jabatan');
    $this->db->from('user');
    $this->db->where('user.id_jabatan','1');
    $this->db->or_where('user.id_jabatan','2');
    $this->db->or_where('user.id_jabatan','3');
    $this->db->or_where('user.id_jabatan','12');
    return $this->db->get()->result_array();
  }
  public function uploadImage($nama,$flag)
   {
     if($flag){
        $config['upload_path']          = './upload/banner/';
     }else{
        $config['upload_path']          = './upload/slider/';
     }
       $config['allowed_types']        = 'gif|jpg|png|jpeg';
       $config['file_name']            = $nama;
       $config['overwrite']			= true;
       $config['max_size']             = 30240; // 1MB
       // $config['max_width']            = 1024;
       // $config['max_height']           = 768;

       $this->load->library('upload',$config);

       if ($this->upload->do_upload('foto')) {
           return $this->upload->data("file_name");
       }else{
         $error = $this->upload->display_errors();
          $this->session->set_flashdata('error', "<div class='alert alert-danger' role='alert'>".$error."</div>");
         return "default.jpg";
       }
   }
   
   // expo
   function getJurusan(){
     $user = $this->db->get('jurusan')->result_array();
     return $user;
   }
   function getJabatan(){
     $this->db->where('id_jabatan',4);
      $this->db->or_where('id_jabatan',5);
       $this->db->or_where('id_jabatan',6);
        $this->db->or_where('id_jabatan',7);
         $this->db->or_where('id_jabatan',8);
          $this->db->or_where('id_jabatan',13);
     $user = $this->db->get('jabatan')->result_array();
     return $user;
   }

   function insertExpo($nama,$nim,$kelas,$gender,$id_jurusan,$tgl_lahir,$alamat_asal,$alamat_sini,$id_jabatan,$no_wa,$email,$alasan,$bayar,$status){
     $data = array(
        'nama' => $nama,
        'nim' => $nim,
        'kelas' => $kelas,
        'gender' => $gender,
        'id_jurusan' => $id_jurusan,
        'tgl_lahir' => $tgl_lahir,
        'alamat_asal' => $alamat_asal,
        'alamat_sini' => $alamat_sini,
        'id_jabatan' => $id_jabatan,
        'no_wa' =>$no_wa,
        'email' => $email,
        'alasan'=> $alasan,
        'bayar' => $bayar,
        'status' => $status
      );
      if($this->db->insert('dataexpo', $data)){
        return true;
      }else{
        return false;
      }
   }
}
?>
