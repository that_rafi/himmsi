<?php

 class Admin_model extends CI_model
 {
   function getAdmin($id){
     $user = $this->db->get_where('user',['id_user'=> $id])->row_array();
     return $user;
   }
   function getLogin($id){
     $user = $this->db->get_where('user',['id_jabatan'=> $id])->row_array();
     return $user;
   }
   function getTableAdmin(){
     $this->db->select('
          user.id_user,user.nama,user.email,user.telp,jabatan.jabatan,tahun.tahun,user.foto,user.id_jabatan
      ');
      $this->db->join('jabatan', 'user.id_jabatan = jabatan.id_jabatan');
      $this->db->join('tahun', 'user.id_tahun = tahun.id_tahun');
      $this->db->from('user');
      return $query = $this->db->get()->result_array();
   }

   function getJabatan(){
     $user = $this->db->order_by('id_jabatan','ASC');
     $user = $this->db->get('jabatan')->result_array();
     return $user;
   }
   function getManageJabatan($id){
     $user = $this->db->get_where('jabatan',['id_jabatan'=> $id])->row_array();
     return $user;
   }
   function getTahun(){
     $user = $this->db->get('tahun')->result_array();
     return $user;
   }
   // TAMABAHKAN FUNCTION UNTUK MENGAHPUS DATA YANG BERHUBUNGAN DENGAN User

   function insertAdmin($nama,$email,$telp,$pass,$id_jabatan,$id_tahun,$photo){
     $data = array(
        'nama' => $nama,
        'email' =>$email,
        'telp' => $telp,
        'password' => password_hash($pass,PASSWORD_DEFAULT),
        'id_jabatan' =>$id_jabatan,
        'id_tahun' => $id_tahun,
        'foto'=> $photo
      );
      if($this->db->insert('user', $data)){
        return true;
      }else{
        return false;
      }
   }
   function updateAdmin($nama,$email,$telp,$pass,$id_jabatan,$id_tahun,$photo,$id){
      $this->db->set('nama', $nama);
      $this->db->set('email', $email);
      $this->db->set('telp', $telp);
      $this->db->set('password', $pass);
      $this->db->set('id_jabatan', $id_jabatan);
      $this->db->set('id_tahun', $id_tahun);
      $this->db->set('foto', $photo);
      $this->db->where('id_user', $id);
      if($this->db->update('user')){
        return true;
      }else{
        return false;
      }
   }
   function updatePassAdmin($pass,$id){
      $this->db->set('password', password_hash($pass,PASSWORD_DEFAULT));
      $this->db->where('id_user', $id);
      if($this->db->update('user')){
        return true;
      }else{
        return false;
      }
   }
   function deleteAdmin($id){
     $this->db->where('id_user', $id);
     if($this->db->delete('user')){
       return true;
     }else{
       return false;
     }
   }
   // FUNCTION ADD EDIT DELETE Departemen
   function insertJabatan($j,$jmlsk,$jmlst,$ket){
     $data = array(
        'jabatan' => $j,
        'jml_sekertaris' =>$jmlsk,
        'jml_staff' => $jmlst,
        'keterangan' =>$ket,
      );
      if($this->db->insert('jabatan', $data)){
        return true;
      }else{
        return false;
      }
   }
   function updateJabatan($j,$jmlsk,$jmlst,$ket,$id){
      $this->db->set('jabatan', $j);
      $this->db->set('jml_sekertaris', $jmlsk);
      $this->db->set('jml_staff', $jmlst);
      $this->db->set('keterangan', $ket);
      $this->db->where('id_jabatan', $id);
      if($this->db->update('jabatan')){
        return true;
      }else{
        return false;
      }
   }
   function deleteJabatan($id){
     $this->db->where('id_jabatan', $id);
     if($this->db->delete('jabatan')){
       return true;
     }else{
       return false;
     }
   }
   function getListField($table){
     return $this->db->field_data($table);
   }
   function addField($name){
     $this->load->dbforge();
     $fields = array(
        $name => array(
            'type' =>'TINYINT',
            'constraint' => '1',
            'default' => 0,
            'NULL' => FALSE,
        ),
      );
      if($this->dbforge->add_column('akses',$fields)){
        return true;
      }else{
        return false;
      }
   }
   function renameTable($old,$new){
     $this->load->dbforge();
     $fields = array(
        $old => array(
                'name' => $new,
                'type' =>'TINYINT',
                'constraint' => '1',
                'default' => 0,
                'NULL' => FALSE,
        ),
      );
     if($this->dbforge->modify_column('akses', $fields)){
       return true;
     }else{
       return false;
     }
   }
   function deleteTable($nama){
     $this->load->dbforge();
    if( $this->dbforge->drop_column('akses', $nama)){
      return true;
    }else{
      return false;
    }
   }
   function getAkses($id){
     $user = $this->db->get_where('akses',['id_user'=> $id])->row_array();
     return $user;
   }
   function inputAkses($data){
     if($this->db->insert('akses', $data)){
       return true;
     }else{
       return false;
     }
   }
   function updateAkses($data,$id){
     $this->db->set($data);
     $this->db->where('id_user', $id);
     if($this->db->update('akses')){
       return true;
     }else{
       return false;
     }
   }
   public function uploadImage($nama)
    {
        $config['upload_path']          = './upload/adminpic/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['file_name']            = $nama;
        $config['overwrite']			= true;
        $config['max_size']             = 20240; // 1MB
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;

        $this->load->library('upload',$config);

        if ($this->upload->do_upload('foto')) {
            return $this->upload->data("file_name");
        }else{
          $error = $this->upload->display_errors();
           $this->session->set_flashdata('error', "<div class='alert alert-danger' role='alert'>".$error."</div>");
          return "default.jpg";
        }


    }
    function updateProfile($nama,$email,$telp,$foto,$id){
       $this->db->set('nama', $nama);
       $this->db->set('email', $email);
       $this->db->set('telp', $telp);
       $this->db->set('foto', $foto);
       $this->db->where('id_user', $id);
       if($this->db->update('user')){
         return true;
       }else{
         return false;
       }
    }
 }



?>
