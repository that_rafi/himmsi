<?php
class Aspirasi_model extends CI_model
{
  function getAspirasi($id){
    $user = $this->db->get_where('aspirasi',['id_aspirasi'=> $id])->row_array();
    return $user;
  }
  function getAllAspirasi($limit,$start){
    $this->db->select('aspirasi.id_aspirasi,jurusan.jurusan,angkatan.angkatan,aspirasi.nama,aspirasi.nim,
    aspirasi.email,aspirasi.hp,aspirasi.aspirasi,aspirasi.tgl,aspirasi.jawaban,aspirasi.status');
    $this->db->join('jurusan','aspirasi.id_jurusan = jurusan.id_jurusan');
    $this->db->join('angkatan','aspirasi.id_angkatan = angkatan.id_angkatan');
    $this->db->order_by('aspirasi.tgl','DESC');
    $this->db->limit($limit,$start);
    $this->db->from('aspirasi');
    return $query = $this->db->get()->result_array();
  }
  function getCountAllAspirasi(){
    $this->db->select('aspirasi.id_aspirasi,jurusan.jurusan,angkatan.angkatan,aspirasi.nama,aspirasi.nim,
    aspirasi.email,aspirasi.hp,aspirasi.aspirasi,aspirasi.tgl,aspirasi.jawaban,aspirasi.status');
    $this->db->join('jurusan','aspirasi.id_jurusan = jurusan.id_jurusan');
    $this->db->join('angkatan','aspirasi.id_angkatan = angkatan.id_angkatan');
    $this->db->from('aspirasi');
    return $query = $this->db->get()->num_rows();
  }

  function updateStatus($id,$status){
    $this->db->set('status', $status);
    $this->db->where('id_aspirasi',$id);
    if($this->db->update('aspirasi')){
      return true;
    }else{
      return false;
    }
  }
  function insertAspirasi($idj,$ida,$nama,$nim,$email,$hp,$aspirasi,$tgl){
    $s = "draft";
    $jwbn = "-";
    $data = array(
       'id_jurusan' => $idj,
       'id_angkatan' =>$ida,
       'nama' => $nama,
       'nim' => $nim,
       'email' =>$email,
       'hp' => $hp,
       'aspirasi'=> $aspirasi,
       'tgl' =>$tgl,
       'jawaban' => $jwbn,
       'status'=> $s
     );
     if($this->db->insert('aspirasi', $data)){
       return true;
     }else{
       return false;
     }
  }
  function updateJawaban($id,$jwbn){
    $this->db->set('jawaban', $jwbn);
    $this->db->where('id_aspirasi',$id);
    if($this->db->update('aspirasi')){
      return true;
    }else{
      return false;
    }
  }
  function deleteAspirasi($id){
    $this->db->where('id_aspirasi',$id);
    if($this->db->delete('aspirasi')){
      return true;
    }else{
      return false;
    }
  }
  function getJurusan(){
    $user = $this->db->get('jurusan')->result_array();
    return $user;
  }
  function getAngkatan(){
    $user = $this->db->get('angkatan')->result_array();
    return $user;
  }
  function getJmlAngkatan(){
    $user = $this->db->get('angkatan')->num_rows();
    return $user;
  }
}
?>
