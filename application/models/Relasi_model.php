<?php
class Relasi_model extends CI_model
{
  function getRelasi($id){
    $user = $this->db->get_where('relasi',['id_relasi'=> $id])->row_array();
    return $user;
  }
  function getAllRelasi(){
    $this->db->select('relasi.id_relasi,relasi.nama_relasi,relasi.organisasi,relasi.no_hp,relasi.email,
    relasi.alamat,relasi.jurusan,relasi.kampus,relasi.keterangan,jabatan.jabatan,relasi.id_user');
     $this->db->join('user', 'relasi.id_user = user.id_user');
     $this->db->join('jabatan', 'user.id_jabatan = jabatan.id_jabatan');
     $this->db->from('relasi');
     return $query = $this->db->get()->result_array();
  }
  function updateRelasi($id,$nama,$org,$no,$e,$a,$j,$kmp,$ket){
    $this->db->set('nama_relasi', $nama);
    $this->db->set('organisasi', $org);
    $this->db->set('no_hp', $no);
    $this->db->set('email', $e);
    $this->db->set('alamat', $a);
    $this->db->set('jurusan', $j);
    $this->db->set('kampus', $kmp);
    $this->db->set('keterangan', $ket);;
    $this->db->where('id_relasi',$id);
    if($this->db->update('relasi')){
      return true;
    }else{
      return false;
    }
  }
  function insertRelasi($nama,$org,$no,$e,$a,$j,$kmp,$ket,$idu){
    $data = array(
       'nama_relasi' => $nama,
       'organisasi' =>$org,
       'no_hp' => $no,
       'email' => $e,
       'alamat' =>$a,
       'jurusan' => $j,
       'kampus' => $kmp,
       'keterangan' => $ket,
       'id_user' => $idu,
     );
     if($this->db->insert('relasi', $data)){
       return true;
     }else{
       return false;
     }
  }
  function deleteRelasi($id){
    $this->db->where('id_relasi',$id);
    if($this->db->delete('relasi')){
      return true;
    }else{
      return false;
    }
  }
  function updateContact($addname,$add1,$add2,$add3,$email,$cn1,$c1,$cn2,$c2,$linky,$linkf,$linki,$id){
    $this->db->set('address_name', $addname);
    $this->db->set('address_1', $add1);
    $this->db->set('address_2', $add2);
    $this->db->set('address_3', $add3);
    $this->db->set('email', $email);
    $this->db->set('contactname_1', $cn1);
    $this->db->set('contact_1', $c1);
    $this->db->set('contactname_2', $cn2);
    $this->db->set('contact_2', $c2);
    $this->db->set('link_youtube', $linky);
    $this->db->set('link_facebook', $linkf);
    $this->db->set('link_ig', $linki);
    $this->db->where('id_contact',$id);
    if($this->db->update('contact')){
      return true;
    }else{
      return false;
    }
  }
}
?>
