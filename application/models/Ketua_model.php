<?php
class Ketua_model extends CI_model
{
  function getProker($id){
    $user = $this->db->get_where('proker',['id_proker'=> $id])->row_array();
    return $user;
  }
  function getAllProker(){
    $user = $this->db->get('proker')->result_array();
    return $user;
  }
  function updateStatus($id,$status){
    $this->db->set('status', $status);
    $this->db->where('id_proker',$id);
    if($this->db->update('proker')){
      return true;
    }else{
      return false;
    }
  }
  function deleteProker($id){
    $this->db->where('id_proker',$id);
    if($this->db->delete('proker')){
      return true;
    }else{
      return false;
    }
  }
}
?>
