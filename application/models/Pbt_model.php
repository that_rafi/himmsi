<?php
class Pbt_model extends CI_model
{
  function getPbt($id){
    $user = $this->db->get_where('pbt',['id_pbt'=> $id])->row_array();
    return $user;
  }
  function getPesertaPbt($id){
    $user = $this->db->get_where('datapbt',['id_mhs'=> $id])->row_array();
    return $user;
  }
  function getAllPbt($limit,$start){
    $this->db->limit($limit,$start);
    $this->db->order_by('tanggal','DESC');
    $user = $this->db->get('pbt')->result_array();
    return $user;
  }
  function getCountAllPbt(){
    $user = $this->db->get('pbt')->num_rows();
    return $user;
  }
  function getMateri($limit,$start,$sms,$jurusan){
    $this->db->from('pbt');
    $this->db->where('jurusan',$jurusan);
    $this->db->where('mhs_semester',$sms);
    $this->db->order_by('tanggal','DESC');
    $this->db->limit($limit,$start);
    $user = $this->db->get()->result_array();
    return $user;
  }
  function getMateriByKategori($limit,$start,$sms,$jurusan,$field,$kat){
    $this->db->from('pbt');
    $this->db->where('jurusan',$jurusan);
    $this->db->where('mhs_semester',$sms);
    $this->db->where($field,$kat);
    $this->db->order_by('tanggal','DESC');
    $this->db->limit($limit,$start);
    $user = $this->db->get()->result_array();
    return $user;
  }
  function getCountMateri($sms,$jurusan){
    $this->db->from('pbt');
    $this->db->where('jurusan',$jurusan);
    $this->db->where('mhs_semester',$sms);
    $user = $this->db->get()->num_rows();
    return $user;
  }
  function getCountMateriByKategori($sms,$jurusan,$field,$kat){
    $this->db->from('pbt');
    $this->db->where('jurusan',$jurusan);
    $this->db->where('mhs_semester',$sms);
    $this->db->where($field,$kat);
    $user = $this->db->get()->num_rows();
    return $user;
  }
  function getAllPesertaPbt($limit,$start){
    $this->db->limit($limit,$start);
    $this->db->order_by('tanggal','DESC');
    $user = $this->db->get('datapbt')->result_array();
    return $user;
  }
  function getCountAllPesertaPbt(){
    $user = $this->db->get('datapbt')->num_rows();
    return $user;
  }
  function getKategoriMateri($kat){
    $this->db->select("$kat,COUNT(id_pbt)");
     $this->db->from('pbt');
     $this->db->group_by($kat);
     return $this->db->get();
   }
   function getDataForKategori($kat,$jur,$mhs){
     $this->db->select("$kat,COUNT(id_pbt)");
      $this->db->from('pbt');
      $this->db->where('jurusan',$jur);
      $this->db->where('mhs_semester',$mhs);
      $this->db->group_by($kat);
      return $this->db->get();
    }

  function updatePbt($matkul,$j,$mhss,$k,$tgl,$v,$id){
    $this->db->set('matakuliah',$matkul);
    $this->db->set('jurusan',$j);
    $this->db->set('mhs_semester',$mhss);
    $this->db->set('keterangan',$k);
    $this->db->set('tanggal',$tgl);
    $this->db->set('video',$v);
    $this->db->where('id_pbt',$id);
    if($this->db->update('pbt')){
      return true;
    }else{
      return false;
    }
  }function updatePesertaPbt($nama,$n,$j,$tgl,$no,$id){
    $this->db->set('nama',$nama);
    $this->db->set('nim',$n);
    $this->db->set('jurusan',$j);
    $this->db->set('tanggal',$tgl);
    $this->db->set('no_hp',$no);
    $this->db->where('id_mhs',$id);
    if($this->db->update('datapbt')){
      return true;
    }else{
      return false;
    }
  }
  function insertPbt($matkul,$j,$mhss,$k,$tgl,$v){
    $data = array(
       'matakuliah' => $matkul,
       'jurusan' =>$j,
       'mhs_semester' => $mhss,
       'keterangan' => $k,
       'tanggal' =>$tgl,
       'video' => $v,
     );
     if($this->db->insert('pbt', $data)){
       return true;
     }else{
       return false;
     }
  }
  function insertPesertaPbt($nama,$n,$j,$tgl,$no){
    $data = array(
       'nama' => $nama,
       'nim' =>$n,
       'jurusan' => $j,
       'tanggal' =>$tgl,
       'no_hp' =>$no,
     );
     if($this->db->insert('datapbt', $data)){
       return true;
     }else{
       return false;
     }
  }
  function deletePbt($id){
    $this->db->where('id_pbt',$id);
    if($this->db->delete('pbt')){
      return true;
    }else{
      return false;
    }
  }
  function deletePesertaPbt($id){
    $this->db->where('id_mhs',$id);
    if($this->db->delete('datapbt')){
      return true;
    }else{
      return false;
    }
  }
  function getJurusan(){
    $user = $this->db->get('jurusan')->result_array();
    return $user;
  }
  public function uploadImage($nama)
   {
       $config['upload_path']          = './upload/Pbt/';
       $config['allowed_types']        = 'mp4|jpg|png|jpeg';
       $config['file_name']            = $nama;
       $config['overwrite']			= true;
       $config['max_size']             = 30240; // 1MB
       // $config['max_width']            = 1024;
       // $config['max_height']           = 768;

       $this->load->library('upload',$config);

       if ($this->upload->do_upload('foto')) {
           return $this->upload->data("file_name");
       }else{
         $error = $this->upload->display_errors();
          $this->session->set_flashdata('error', "<div class='alert alert-danger' role='alert'>".$error."</div>");
         return "default.jpg";
       }


   }
}
?>
