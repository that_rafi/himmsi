<?php
class Sekertaris_model extends CI_model
{
  function getBarang($id){
    $user = $this->db->get_where('barang',['id_barang'=> $id])->row_array();
    return $user;
  }
  function getAllBarang(){
    $user = $this->db->get('barang')->result_array();
    return $user;
  }
  function updateBarang($nama,$j,$g,$k,$h,$t,$id){
    $this->db->set('nama',$nama);
    $this->db->set('jumlah',$j);
    $this->db->set('gambar',$g);
    $this->db->set('keterangan',$k);
    $this->db->set('harga',$h);
    $this->db->set('tipe',$t);
    $this->db->where('id_barang',$id);
    if($this->db->update('barang')){
      return true;
    }else{
      return false;
    }
  }
  function insertBarang($nama,$j,$g,$k,$h,$t){
    $data = array(
       'nama' => $nama,
       'jumlah' =>$j,
       'gambar' => $g,
       'keterangan' => $k,
       'harga' =>$h,
       'tipe' => $t,
     );
     if($this->db->insert('barang', $data)){
       return true;
     }else{
       return false;
     }
  }
  function deletebarang($id){
    $this->db->where('id_barang',$id);
    if($this->db->delete('barang')){
      return true;
    }else{
      return false;
    }
  }
  public function uploadImage($nama)
   {
       $config['upload_path']          = './upload/barang/';
       $config['allowed_types']        = 'gif|jpg|png|jpeg';
       $config['file_name']            = $nama;
       $config['overwrite']			= true;
       $config['max_size']             = 20240; // 1MB
       // $config['max_width']            = 1024;
       // $config['max_height']           = 768;

       $this->load->library('upload',$config);

       if ($this->upload->do_upload('foto')) {
           return $this->upload->data("file_name");
       }else{
         $error = $this->upload->display_errors();
          $this->session->set_flashdata('error', "<div class='alert alert-danger' role='alert'>".$error."</div>");
         return "default.jpg";
       }


   }
   // pinjaman
   function getPinjam($id){
     $this->db->select('
          pinjaman.*,barang.nama
      ');
      $this->db->join('barang', 'pinjaman.id_barang = barang.id_barang');
      $this->db->from('pinjaman');
      $this->db->where('id_pinjam', $id);
      return $query = $this->db->get()->row_array();
   }
   function getAllPinjaman($limit,$start){
     $this->db->select('
          pinjaman.*,barang.nama
      ');
      $this->db->join('barang', 'pinjaman.id_barang = barang.id_barang');
      $this->db->limit($limit,$start);
      $this->db->from('pinjaman');
      return $query = $this->db->get()->result_array();
   }
   function getCountAllPinjaman(){
     $this->db->select('
          pinjaman.*,barang.nama
      ');
      $this->db->join('barang', 'pinjaman.id_barang = barang.id_barang');
      $this->db->from('pinjaman');
      return $query = $this->db->get()->num_rows();
   }
   function updatePinjaman($idb,$nama,$tglp,$tglk,$kp,$kk,$jmlp,$jmlk,$idp){
     $this->db->set('id_barang',$idb);
     $this->db->set('nama_peminjam',$nama);
     $this->db->set('tgl_pinjam',$tglp);
     $this->db->set('tgl_kembali',$tglk);
     $this->db->set('keadaan_pinjam',$kp);
     $this->db->set('keadaan_kembali',$kk);
     $this->db->set('jml_pinjam',$jmlp);
     $this->db->set('jml_kembali',$jmlk);
     $this->db->where('id_pinjam',$idp);
     if($this->db->update('pinjaman')){
       return true;
     }else{
       return false;
     }
   }
   function insertPinjaman($idb,$nama,$tglp,$tglk,$kp,$kk,$jmlp,$jmlk){
     $data = array(
        'id_barang' => $idb,
        'nama_peminjam' =>$nama,
        'tgl_pinjam' => $tglp,
        'tgl_kembali' => $tglk,
        'keadaan_pinjam' =>$kp,
        'keadaan_kembali' => $kk,
        'jml_pinjam' =>$jmlp,
        'jml_kembali' => $jmlk,
      );
      if($this->db->insert('pinjaman', $data)){
        return true;
      }else{
        return false;
      }
   }
   function deletePinjaman($id){
     $this->db->where('id_pinjam',$id);
     if($this->db->delete('pinjaman')){
       return true;
     }else{
       return false;
     }
   }
}
?>
