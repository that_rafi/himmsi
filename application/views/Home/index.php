
    <!--================ End Header Area =================-->

    <!--================ Start banner Area =================-->
    <section class="home-banner-area relative">
      <div class="container-fluid">
        <div class="row">
          <div class="owl-carousel home-banner-owl">
            <?php foreach($slider as $s) : ?>
            <div class="banner-img">
              <img class="img-fluid" style="width : 725px; height:440px" src="<?php echo base_url();
?>upload/artikel/<?= $s['gambar']; ?>" alt="" />
              <div class="text-wrapper">
                <a href="<?php echo base_url();?>Artikel/detail/<?= $s['kategori']; ?>/<?= $s['id_artikel']; ?>" class="d-flex">
                  <h1>
                    <?= $s['judul']; ?> <br />
                  </h1>
                </a>
              </div>
            </div>
          <?php endforeach;?>
            </div>
          </div>
        </div>
      </div>
      <div class="social-icons">
        <ul>
          <li>
            <a href="<?= $contact['link_facebook']; ?> "><i class="fa fa-facebook"></i></a>
          </li>
          <li>
            <a href="<?= $contact['link_ig']; ?> "><i class="fa fa-instagram"></i></a>
          </li>
          <li class="diffrent">Find us</li>
        </ul>
      </div>
    </section>
    <!--================ End banner Area =================-->

    <!--================ Start Blog Post Area =================-->
    <section class="blog-post-area section-gap relative">
      <div class="container">
        <div class="row">
          <div class="col-lg-8">
            <?php if($search){ ?>
              <h1>Search Result</h1>
              <p></p><p>Nothing here matches your search, Try other keywords!</p>
            <?php }?>
            <div class="row">
              <?php foreach($artikel as $a):?>
              <div class="col-lg-6 col-md-6">
                <div class="single-amenities " >
                  <div class="amenities-thumb">
                    <img
                      class="img-fluid w-100"
                      src="<?php echo base_url();
?>upload/artikel/<?= $a['gambar']; ?>"
                      alt=""
                    />
                  </div>
                  <div class="amenities-details">
                    <h5>
                      <a href="<?php echo base_url();?>Artikel/detail/<?= $a['kategori']; ?>/<?= $a['id_artikel']; ?>"
                        ><?= $a['judul']; ?></a
                      >
                    </h5>
                    <div class="amenities-meta mb-10">
                      <a href="<?php echo base_url();?>Artikel/detail/<?= $a['kategori']; ?>/<?= $a['id_artikel']; ?>" class="">
                        <span class="ti-calendar"></span><?= $a['tanggal'] ?>
                      </a>
                    </div>

                     <p><?php $str = mb_strimwidth($a['isi'], 0, 5, "...");
                      echo $str;
                      ?></p>

                    <div class="d-flex justify-content-between mt-20">
                      <div>
                        <a href="<?php echo base_url();?>Artikel/detail/<?= $a['kategori']; ?>/<?= $a['id_artikel']; ?>" class="blog-post-btn">
                          Read More <span class="ti-arrow-right"></span>
                        </a>
                      </div>
                      <div class="category">
                        <a href="<?php echo base_url();?>Himmsi/index/<?= $a['kategori']; ?>/<?= $a['kategori']; ?>">
                          <span class="ti-folder mr-1"></span> <?= $a['kategori']; ?>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            <?php endforeach;?>
            </div>
            <div class="row">
              <div class="col-lg-12">
                <?= $this->pagination->create_links(); ?>

              </div>
            </div>
          </div>

          <!-- Start Blog Post Siddebar -->
          <div class="col-lg-4 sidebar-widgets">
              <div class="widget-wrap">
                <div class="single-sidebar-widget search-widget">
                  <form class="search-form" action="<?php echo base_url();?>Himmsi/Search/" method="post">
                    <input placeholder="Search Posts" name="search" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Search Posts'">
                    <button type="submit"><i class="fa fa-search"></i></button>
                  </form>
                </div>


                <div class="single-sidebar-widget post-category-widget">
                  <h4 class="category-title">Catgories</h4>
                  <ul class="cat-list mt-20">
                    <li>
                      <a href="<?= base_url(); ?>Himmsi/index/0" class="d-flex justify-content-between">
                        <p>All</p>
                        <p><?= $countall ?></p>
                      </a>
                    </li>
                    <?php $i=0; foreach($kat as $k): ?>
                    <li>
                      <a href="<?php echo base_url();?>Himmsi/index/<?= $k['kategori']; ?>" class="d-flex justify-content-between">
                        <p><?= $k['kategori'] ;?></p>
                        <p><?= $count[$i]; $i++;?></p>
                      </a>
                    </li>
                  <?php endforeach; ?>
                  </ul>
                </div>

                <div class="single-sidebar-widget popular-post-widget">
                  <h4 class="popular-title">Agenda</h4>
                  <div class="popular-post-list">
                    <?php foreach($agenda as $agd): ?>
                    <div class="single-post-list">
                      <div class="thumb">
                        <img class="img-fluid" src="<?php echo base_url();
?>upload/artikel/<?= $agd['gambar']; ?>" alt="">
                      </div>
                      <div class="details mt-20">
                        <a href="<?php echo base_url();?>Artikel/detail/<?= $agd['kategori']?>/<?= $agd['id_artikel']; ?>">
                          <h6><?= $agd['judul']; ?>"</h6>
                        </a>
                        <p><?= $agd['jabatan']; ?> | <?= $agd['tanggal']; ?></p>
                      </div>
                    </div>
                  <?php endforeach; ?>
                  </div>
                </div>

                </div>
              </div>
            </div>
          <!-- End Blog Post Siddebar -->
          <!-- -->
        </div>
      </div>
    </section>
      <section class="blog-post-area section-gap relative"  style="background-image: url('public/img/banner/yt.jpeg'); background-size:cover" >
    <div class="container">
  		<div class="row">
  			<div class="col-lg-6  col-md-6 col-sm-6">
  			  <div class="single-sidebar-widget post-category-widget">
  					  <h4 class="category-title" style="color: white" >HIMMSI ON YOUTUBE
              <?php
              $channel =trim($contact['link_youtube'],"https://www.youtube.com/channel/");
              $idyt = substr($channel, 0, 2);
              if($idyt == "UC"){
                $fix = "UU";
                $link = str_replace($idyt,$fix,$channel);
              }
               ?></h4>
              <hr color="white" style="opacity: 0.50">
              <?php $i =0?>
              <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="http://www.youtube.com/embed/videoseries?list=<?= $link; ?>&index=<?= $i?>" allowfullscreen></iframe>
              </div>
  				</div>
  			</div>

  			<div class="col-lg-6  col-md-6 col-sm-6" >
  				<div class="single-footer-widget mail-chimp mt-50" style="margin-left:20px">
  					<ul class="youtubefeed">
              <?php $i++; while($i<3){ ?>
  						<li><iframe class="embed-responsive-item" src="http://www.youtube.com/embed/videoseries?list=<?= $link; ?>&index=<?= $i?>" allowfullscreen></iframe></li>
            <?php $i++; }?>
  					</ul>
            <a href="<?= $contact['link_youtube']; ?>" class="blog-post-btn" style="color: white;">
              See More <span class="ti-arrow-right"></span>
            </a>
  				</div>
  			</div>
  		</div>
  	</div>
  </section>
    <!--================ End Blog Post Area =================-->
