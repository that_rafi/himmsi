
<div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-5 d-none d-lg-block "style="background-image: url('<?php echo base_url()?>public/img/<?= $pic; ?>')"></div>
          <div class="col-lg-7">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4"><?= $title ?></h1>
              </div>
                <?= $this->session->flashdata('message'); ?>
                <?= $this->session->flashdata('error'); ?>
              <form class="user" method="post" action="<?php echo base_url();?>Akademik/form_aspirasi" enctype="multipart/form-data">
                <div class="form-group">
                  <input type="text" name="nama" class="single-input" style="background-color : #F4F4F4" id="exampleFirstName" placeholder="Nama Mahasiswa" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Nama Mahasiswa'" value="<?php echo set_value('nama'); ?>"  >
                  <?= form_error("nama",'<small class="text-danger pl-3">','</small>'); ?>
                </div>
                <div class="form-group">
                  <input type="text" name="nim" class="single-input" style="background-color : #F4F4F4" id="nimmhs" placeholder="Nim Mahasiswa" value="<?php echo set_value('nim'); ?>">
                  <?= form_error("nim",'<small class="text-danger pl-3">','</small>'); ?>
                </div>
                <div class="input-group-icon mb-15">
                  <div class="icon"><i class="fa fa-at" aria-hidden="true"></i></div>
                  <input type="text" name="email" class="single-input" style="background-color : #F4F4F4" id="exampleFirstName" placeholder="Email Mahasiswa" value="<?php echo set_value('email'); ?>">
                  <?= form_error("email",'<small class="text-danger pl-3">','</small>'); ?>
                </div>
                <div class="input-group-icon mb-15">
                  <div class="icon"><i class="fa fa-phone" aria-hidden="true"></i></div>
                  <input type="tel" name="tel" class="single-input" style="background-color : #F4F4F4" id="exampleFirstName" placeholder="No Hp Mahasiswa" value="<?php echo set_value('tel'); ?>">
                </div>
                <div id="jurmhs">
                <select class="form-select mb-3"  name="jurusan">
                  
                    <option value="0">Jurusan</option>
                  <?php foreach($jurusan as $mjr ):?>
                    <option value="<?= $mjr['id_jurusan']; ?>"  ><?= $mjr['jurusan']; ?></option>
                  <?php  endforeach; ?>
                 
                </select>
                </div>
                <input type="hidden" id="jmlang" value="<?= $jmlang ?>"/>
                <div id="angmhs">                 
                <select class="form-select mb-3" name="angkatan">
                    <option value="0" selected="selected" >Angkatan</option>
                  <?php foreach($angkatan as $ang ): ?>
                    <option value="<?= $ang['id_angkatan']; ?>" ><?= $ang['angkatan']; ?></option>
                  <?php  endforeach; ?>
                  </select>
                  
                  </div>
                  <?= form_error('jurusan','<small class="text-danger pl-3">','</small>'); ?>
                  <?= form_error('angkatan','<small class="text-danger pl-3">','</small>'); ?>
                <div class="form-group">
                  <textarea class="single-input" style="background-color : #F4F4F4" name="aspirasi" id='aspirasi' placeholder="Tulis Aspirasimu disini..." ><?php echo set_value('aspirasi'); ?></textarea>
                  <?= form_error('aspirasi','<small class="text-danger pl-3">','</small>'); ?>
                </div>
                 
                     <div class="g-recaptcha" data-sitekey="6LeBx68UAAAAANmor0iV7Cqq3bwFFXZ1CXHBq7Fz"></div>
                
                <div class="form-group">
                    <input type="button" value="Kirim Aspirasi" class="genric-btn success circle arrow mt-10 "  data-toggle="modal" data-target="#message"></input>
                </div>
                
                <div class="modal fade" id="message" tabindex="-1" role="dialog" aria-labelledby="messageTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="messageTitle">Confirmation</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      Apakah anda yakin untuk mengirim Aspirasi!?
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="genric-btn danger circle arrow mt-10 " data-dismiss="modal">Tidak</button>
                      <button type="submit" name="submit" class="genric-btn info circle arrow mt-10 ">Ya! Saya yakin</button>
                    </div>
                  </div>
                </div>
              </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
 <script>   
   $( document ).ready(function() {
    
    var tempData = $('#jurmhs').html(); 
    var tempAng = $('#angmhs').html();
    var jmlang = $('#jmlang').val();

    function setSelectedAng(ang){
      ang = ang - 10
     var selectedData = '<select class="form-select mb-3" name="angkatan"><option value="0">Angkatan</option>'
     var thun = 2011;
     for (var i=0;i<jmlang;i++){
       if(ang==(i+1)){
        selectedData += '<option value="'+(i+1)+'" selected="selected" >'+thun+'</option>'
       }else{
        selectedData += '<option value="'+(i+1)+'" >'+thun+'</option>'
       }    
      thun++
     }
     selectedData += '</select>'
    return selectedData
    }  

    function setSelected(jur){
      if(jur=="1"){
        var selectedSI = 'selected="selected"';
        var selectedMI = '';
      }else if(jur=="2"){
        var selectedSI = '';
        var selectedMI = 'selected="selected"';
      }
      var selectedData = '<select class="form-select mb-3" name="jurusan">'+
    '<option value="0">Jurusan</option>'+
    '<option value="1" '+selectedSI+'>S1 - SI</option>'+
    '<option value="2" '+selectedMI+'>D3 - MI</option></select>'
      return selectedData
    }

    function check(el){
      if(el.length > 1){
            var ang = el.substr(0,2);  // ambil 2 di awal
          if(el.indexOf('.') != -1){
            var jur = el.substr(3,2); // ambil ang
          }else{
            var jur = el.substr(2,2); // ambil ang
          }
          // checking ang
          $('#angmhs').html(setSelectedAng(ang))      
          // checking jur
          if(jur.indexOf('62') != -1 || jur.indexOf('12') != -1 ){
            $('#jurmhs').html(setSelected(1))
          }else if(jur.indexOf('02') != -1){
            $('#jurmhs').html(setSelected(2))
          }else{
            $('#jurmhs').html(tempData)
          }
        }else{
          $('#angmhs').html(tempAng)
        }
    }
    

      $('#nimmhs').keypress(function(e) { // text written
         check($(this).val());       
      });
    $('#nimmhs').keyup(function(e) {
        if (e.keyCode == 8 || e.keyCode == 46) { //backspace and delete key
            check($(this).val());
        } else { // rest ignore
            e.preventDefault();
        }
    }); 
    var nim = $('#nimmhs').val();
      if(nim!=""){
     check(nim)
    }  

    $('#nimmhs').on("change",function(){
      var nim = $('#nimmhs').val();
      if(nim!=""){
        check(nim)
      }  
    })


   });

   

   
   
 </script>