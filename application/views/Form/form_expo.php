<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Colorlib Templates">
    <meta name="author" content="Colorlib">
    <meta name="keywords" content="Colorlib Templates">

    <!-- Title Page-->
    <title>HIMMSI AA Registration</title>

    <!-- Icons font CSS-->
    <link href="<?= base_url(); ?>public/expo/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="<?= base_url(); ?>public/expo/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">

    <!-- Vendor CSS-->
    <link href="<?= base_url(); ?>public/expo/vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="<?= base_url(); ?>public/expo/vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="<?= base_url(); ?>public/expo/css/main.css" rel="stylesheet" media="all">
</head>

<body>
    <div class="page-wrapper bg-blue p-t-100 p-b-100 font-robo" style="background-image: url('<?php echo base_url()?>public/img/bg1.jpg')">
        <div class="wrapper wrapper--w680">
            <div class="card card-1">
                <div class="card-heading"></div>
                <div class="card-body">
                    <h2 class="title">Data Diri Anggota Aktif</h2>
                    <?= $this->session->flashdata('message'); ?>
                    <form method="POST" action="<?= base_url()?>Pendaftaran_expo">
                        <div class="input-group">
                            <input class="input--style-1" type="text" placeholder="NAME" name="name" value="<?php echo set_value('name'); ?>">
                        </div>
                          <?= form_error("name",'<small class="text-danger pl-3">','</small>'); ?>
						<div class="input-group">
                            <input class="input--style-1" type="text" placeholder="NIM" name="nim" value="<?php echo set_value('nim'); ?>">
                        </div>
                        <?= form_error("nim",'<small class="text-danger pl-3">','</small>'); ?>
						<div class="input-group">
                            <input class="input--style-1" type="text" placeholder="KELAS" name="kelas" value="<?php echo set_value('kelas'); ?>">
                        </div>
                        <?= form_error("kelas",'<small class="text-danger pl-3">','</small>'); ?>
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <input class="input--style-1 js-datepicker" type="text" placeholder="BIRTHDATE" name="birthday" value="<?php echo set_value('birthday'); ?>">
                                    <i class="zmdi zmdi-calendar-note input-icon js-btn-calendar"></i>
                                </div>
                                <?= form_error("birthday",'<small class="text-danger pl-3">','</small>'); ?>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <div class="rs-select2 js-select-simple select--no-search">
                                        <select name="gender" value="<?php echo set_value('gender'); ?>">
                                            <option disabled="disabled" selected="selected">GENDER</option>
                                            <option>Male</option>
                                            <option>Female</option>
                                        </select>
                                        <div class="select-dropdown"></div>
                                    </div>
                                </div>
                            </div>
                            <?= form_error("gender",'<small class="text-danger pl-3">','</small>'); ?>
                        </div>
                        <div class="input-group">
                            <input class="input--style-1" type="text" placeholder="NO HP/WA" name="nomerhp" value="<?php echo set_value('nomerhp'); ?>">
                        </div>
                        <?= form_error("nomorhp",'<small class="text-danger pl-3">','</small>'); ?>
                        <div class="input-group">
                            <input class="input--style-1" type="email" placeholder="Email" name="email" value="<?php echo set_value('email'); ?>">
                        </div>
                        <?= form_error("email",'<small class="text-danger pl-3">','</small>'); ?>
						<div class="input-group">
                            <input class="input--style-1" type="text" placeholder="ALAMAT ASAL" name="alamat_asal" value="<?php echo set_value('alamat_asal'); ?>">
                        </div>
						<div class="input-group">
                            <input class="input--style-1" type="text" placeholder="ALAMAT TINGGAL" name="alamat_tinggal" value="<?php echo set_value('alamat_tinggal'); ?>">
                        </div>
                        <div class="input-group">
                            <div class="rs-select2 js-select-simple select--no-search">
                                <select name="prodi" value="<?php echo set_value('prodi'); ?>">
                                    <option disabled="disabled" selected="selected">Study Program</option>
                                    <?php foreach($jurusan as $j): ?>
                                    <option value="<?= $j['id_jurusan'] ?>"><?= $j['jurusan']; ?></option>
                                  <?php endforeach;?>
                                </select>
                                <div class="select-dropdown"></div>
                            </div>
                        </div>
                        <?= form_error("prodi",'<small class="text-danger pl-3">','</small>'); ?>
                        <div class="input-group">
                            <div class="rs-select2 js-select-simple select--no-search">
                                <select name="departemen" value="<?php echo set_value('departemen'); ?>">
                                    <option disabled="disabled" selected="selected">Departemen HIMMSI</option>
                                    <?php foreach($jabatan as $j): ?>
                                    <option value="<?= $j['id_jabatan'] ?>"><?= $j['jabatan']; ?></option>
                                  <?php endforeach;?>
                                </select>
                                <div class="select-dropdown"></div>
                            </div>
                        </div>
                        <?= form_error("departemen",'<small class="text-danger pl-3">','</small>'); ?>
						<div class="input-group">
                            <input class="input--style-1" type="text" placeholder="ALASAN" name="alasan" value="<?php echo set_value('alasan'); ?>">
                        </div>
                        <div class="p-t-20">
                            <button class="btn btn--radius btn--green" type="submit">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Jquery JS-->
    <script src="<?= base_url(); ?>public/expo/vendor/jquery/jquery.min.js"></script>
    <!-- Vendor JS-->
    <script src="<?= base_url(); ?>public/expo/vendor/select2/select2.min.js"></script>
    <script src="<?= base_url(); ?>public/expo/vendor/datepicker/moment.min.js"></script>
    <script src="<?= base_url(); ?>public/expo/vendor/datepicker/daterangepicker.js"></script>

    <!-- Main JS-->
    <script src="<?= base_url(); ?>public/expo/js/global.js"></script>

</body><!-- This templates was made by Colorlib (https://colorlib.com) -->

</html>
<!-- end document-->
