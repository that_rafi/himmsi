<div class="container">
    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-5 d-none d-lg-block "style="background-image: url('<?php echo base_url()?>public/img/<?= $pic; ?>'); background-size:cover"></div>
          <div class="col-lg-7">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4"><?= $title ?></h1>
              </div>
                <?= $this->session->flashdata('message'); ?>
                <?= $this->session->flashdata('error'); ?>
              <form class="user" method="post" action="<?php echo base_url();?>Akademik/pbt_online" enctype="multipart/form-data">
                <div class="form-group">
                  <input type="text" name="nama" class="single-input" style="background-color : #F4F4F4" id="exampleFirstName" placeholder="Nama Mahasiswa" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Nama Mahasiswa'" value="<?php echo set_value('nama'); ?>"  >
                  <?= form_error("nama",'<small class="text-danger pl-3">','</small>'); ?>
                </div>
                <select class="form-select mb-3" name="jurusan">
                    <option value="0" selected="selected" >Jurusan</option>
                  <?php foreach($jurusan as $mjr ):?>
                    <option value="<?= $mjr['jurusan']; ?>" ><?= $mjr['jurusan']; ?></option>
                  <?php  endforeach; ?>
                </select>
                <?= form_error('jurusan','<small class="text-danger pl-3">','</small>'); ?>
                <div class="form-group">
                  <input type="text" name="nim" class="single-input" style="background-color : #F4F4F4" id="exampleFirstName" placeholder="Nim Mahasiswa" value="<?php echo set_value('nim'); ?>">
                  <?= form_error("nim",'<small class="text-danger pl-3">','</small>'); ?>
                </div>
                <div class="form-group">
                  <input type="text" name="nohp" class="single-input" style="background-color : #F4F4F4" id="exampleFirstnohp" placeholder="No Hp Mahasiswa" value="<?php echo set_value('nohp'); ?>">
                  <?= form_error("nohp",'<small class="text-danger pl-3">','</small>'); ?>
                </div>
                
                     <div class="g-recaptcha" data-sitekey="6LeBx68UAAAAANmor0iV7Cqq3bwFFXZ1CXHBq7Fz">
                </div>
                <div class="form-group">
                     <input type="button" value="Daftar" class="genric-btn success circle arrow mt-10 "  data-toggle="modal" data-target="#message"></input>
                </div>
                <div class="modal fade" id="message" tabindex="-1" role="dialog" aria-labelledby="messageTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="messageTitle">Confirmation</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      Apakah anda yakin untuk mengirim Data Anda!?
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="genric-btn danger circle arrow mt-10 " data-dismiss="modal">Tidak</button>
                      <button type="submit" name="submit" class="genric-btn info circle arrow mt-10 ">Ya! Saya yakin</button>
                    </div>
                  </div>
                </div>
              </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
