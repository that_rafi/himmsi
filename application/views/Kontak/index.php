<!--================ Start banner Area =================-->
<section class="banner-area relative">
  <div class="overlay overlay-bg"></div>
  <div class="banner-content text-center">
    <h1>Kontak</h1>
  </div>
</section>

<!--================ End banner Area =================-->

<!-- Start contact-page Area -->
<section class="contact-page-area section-gap">
  <div class="container">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15813.126792419771!2d110.40895769597755!3d-7.759920021030415!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7a599a32cdadab%3A0xea28bb67d2fdc471!2sBSC+Universitas+Amikom!5e0!3m2!1sid!2sid!4v1565775938873!5m2!1sid!2sid" class="embed-responsive embed-responsive-16by9 mb-20" height="480" frameborder="0" style="border:0" allowfullscreen></iframe>
    <div class="row">
      <div class="col-md-4 d-flex flex-column address-wrap ">
        <div class="single-contact-address d-flex flex-row">
          <div class="icon"><span class="lnr lnr-home"></span></div>
          <div class="contact-details">
            <h5><?= $contact['address_name']; ?></h5>
              <?= $contact['address_1']; ?><p>
              <?= $contact['address_2']; ?><br>
              <?= $contact['address_3']; ?>
          </div>
        </div>


      </div>
      <div class="col-md-4">
        <div class="single-contact-address d-flex flex-row">
          <div class="icon">
            <span class="lnr lnr-phone-handset"></span>
          </div>
          <div class="contact-details ml-30">
            <h5><?= $contact['contact_1']; ?></h5>
            <p>(<?= $contact['contactname_1']; ?>)</p>
            <h5><?= $contact['contact_2']; ?></h5>
            <p>(<?= $contact['contactname_2']; ?>)</p>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="single-contact-address d-flex flex-row">
          <div class="icon"><span class="lnr lnr-envelope"></span></div>
          <div class="contact-details ml-30">
            <h5><?= $contact['email']; ?></h5>
          </div>
        </div>
      </div>

    </div>
  </div>
</section>
<!-- End contact-page Area -->
