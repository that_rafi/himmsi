<div class="container">
  <div class="btn-group dropright">
  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Departemen
  </button>
  <div class="dropdown-menu">
    <!-- Dropdown menu links -->
    <?php foreach($departemen as $d): ?>
    <a class="dropdown-item" href="<?= base_url(); ?>admin/Table_admin/edithakakses/<?= $d['id_jabatan']; ?>"><?= $d['jabatan']; ?></a>
    <?php endforeach; ?>
  </div>
  </div>
    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-5 d-none d-lg-block " style="background-image: url('<?php echo base_url()?>upload/randompic/cats_white.jpg'); background-size:cover"></div>
          <div class="col-lg-7">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4"><?= $title; ?></h1>
              </div>
                <?= $this->session->flashdata('message'); ?>
                <?= $this->session->flashdata('error'); ?>
              <form class="profile" method="post" action="<?php echo base_url();?>admin/Table_admin/edithakakses/<?= $id ?>" enctype="multipart/form-data">
                <div class="table-responsive" >
                  <table class="table table-sm table-bordered" id="dataTable" width="100%" cellspacing="0" >
                    <thead>
                      <tr>
                        <th>Nama Akses</th>
                        <th>Hak Akses</th>
                      </tr>
                    </thead>
                    <tfoot>
                      <tr>
                        <th></th>
                        <th><input type="submit" name="submit" value="Update Akses" class="btn btn-primary btn-profile btn-block"></input></th>
                      </tr>
                    </tfoot>
                    <tbody> <!-- Tabel Relasi-->
                    <!-- Tabel Relasi-->


                  <?php foreach($header as $h): ?>
                  <tr>
                  <?php if($h->name == "id_user" || $h->name == "id_akses" ){?>

                <?php }else{?>
                  <th><?= trim($h->name,"m_"); ?></th>
                  <td>
                    <div class="form-group">

                      <div class="custom-control custom-checkbox small">

                        <?php $opo = "";if($access[$h->name] ==1 ){ $opo = "checked"; }?>
                        <input type="checkbox" name="<?= $h->name; ?>" class="custom-control-input" id="customCheck<?= $h->name; ?>" <?= $opo ?> >
                        <label class="custom-control-label" for="customCheck<?= $h->name; ?>"></label>
                      </div>

                    </div>
                  </td>

                  <!--<td>
                    <a href="<?php echo base_url();?>admin/Table_admin/editnamaakses/<?= $h->name; ?>"class="badge badge-info">Edit</a>
                      <a href="<?php echo base_url();?>admin/Table_admin/deletehakakses/<?= $h->name; ?>" class="badge badge-danger" onclick="return confirm('Are You Sure?')">Delete</a>
                  </td>-->
                <?php }?>
                  </tr>
                <?php endforeach;?>

                    </tbody>
                  </table>
                </div>
                <h6 class="m-0 font-weight-bold text-primary" ><a href="<?php echo base_url();?>admin/Table_admin/inputakses">Insert New Access</a></h6>
              </form>
          </div>
        </div>
      </div>
    </div>

  </div>
