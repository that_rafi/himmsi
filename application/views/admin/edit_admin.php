<div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <?php
          if($user['foto'] != null){
            $pic = $user['foto'];
          }else{
            $pic = "default.jpg";
          }
          ?>
          <div class="col-lg-5 d-none d-lg-block " style="background-image: url('<?php echo base_url()?>upload/adminpic/<?=$pic;  ?>')"></div>
          <div class="col-lg-7">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Edit Admin</h1>
              </div>
                <?= $this->session->flashdata('message'); ?>
                <?= $this->session->flashdata('error'); ?>
              <form class="user" method="post" action="<?php echo base_url();?>admin/Table_admin/editadmin/<?= $user['id_user']; ?>" enctype="multipart/form-data">
                <div class="form-group">
                    <input type="text" name="nama" class="form-control form-control-user" id="exampleFirstName" placeholder="Full Name" value="<?= $user['nama']; ?>">
                    <?= form_error('nama','<small class="text-danger pl-3">','</small>'); ?>
                </div>
                <div class="form-group">
                  <input type="email" name="email" class="form-control form-control-user" id="exampleInputEmail" placeholder="Email Address" value="<?= $user['email']; ?>">
                  <?= form_error('email','<small class="text-danger pl-3">','</small>'); ?>
                </div>
                <div class="form-group">
                  <input type="tel" name="telp" class="form-control form-control-user"  placeholder="Phone number" value="<?= $user['telp']; ?>">
                  <?= form_error('telp','<small class="text-danger pl-3">','</small>'); ?>
                </div>
                <div class="form-group row">
                  <div class="col-sm-6">
                    <div class="row">
                    <div class="col-sm-6 ">  <div class="text ml-2" >Position</div></div>
                    <div class="col-sm-6">
                      <select class="custom-select" name="jabatan">
                        <option value="0" selected="selected">Position</option>
                        <?php foreach ($jabatan as $pos) : ?>
                        <?php if($pos['id_jabatan'] == $user['id_jabatan']){ ?>
                        <option value="<?= $pos['id_jabatan']; ?>" selected="selected"><?=$pos['jabatan']; ?></option>
                      <?php }else{ ?>
                        <option value="<?= $pos['id_jabatan']; ?>" ><?=$pos['jabatan']; ?></option>
                      <?php }?>
                      <?php endforeach; ?>
                      </select>
                    </div>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="row">
                    <div class="col-sm-6">  <div class="text ml-2">Year</div></div>
                    <div class="col-sm-6">
                      <select class="custom-select" name="year">
                        <option value="0" selected="selected">Year</option>
                        <?php foreach ($tahun as $thn) : ?>
                          <?php if($thn['id_tahun'] == $user['id_tahun']){ ?>
                          <option value="<?= $thn['id_tahun']; ?>" selected="selected"><?=$thn['tahun']; ?></option>
                        <?php }else{ ?>
                          <option value="<?= $thn['id_tahun']; ?>" ><?=$thn['tahun']; ?></option>
                        <?php }?>
                        <?php endforeach; ?>
                      </select>
                    </div>
                    </div>
                </div>
                </div>
                <?= form_error('jabatan','<small class="text-danger pl-3">','</small>'); ?>
                <?= form_error('year','<small class="text-danger pl-3">','</small>'); ?>
                <div class="row mb-3">
                  <div class="col-lg-6" ><div class="text ml-2">Photo Profile</div></div>
                  <div class="col-lg-6">
                    <div class="custom-file">
                      <input type="hidden" name="pass1" class="custom-file-input" id="customFile" accept="image/*" value="<?= $user['password']; ?>">
                      <input type="hidden" name="old_image" class="custom-file-input" id="customFile" accept="image/*" value="<?= $user['foto']; ?>">
                      <input type="file" name="foto" class="custom-file-input" id="customFile" accept="image/*">
                      <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                  </div>
                </div>
                <?= form_error('foto','<small class="text-danger pl-3">','</small>'); ?>
                <input type="submit" value="Update Admin" class="btn btn-primary btn-user btn-block"></input>
              </form>
              <hr>
              <div class="text-center">
                <a class="small" href="<?php echo base_url();?>admin/Table_admin/changepass/<?= $user['id_user'] ?>">Change Password?</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
