<!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800"><?= $tabletitle ?></h1>
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary" ><a href="<?php echo base_url();?>admin/Table_admin/inputdepartemen">Tambah Departemen</a></h6>
            </div>
            <div class="card-body">
              <?= $this->session->flashdata('message'); ?>
              <div class="table-responsive" >
                <table class="table table-sm table-bordered" id="dataTable" width="100%" cellspacing="0" >
                  <thead>
                    <tr>
                      <?php for($i=0;$i<count($header);$i++){ ?>
                      <th><?= $header[$i]; ?></th>
                    <?php }?>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <?php for($i=0;$i<count($header);$i++){ ?>
                      <th><?= $header[$i]; ?></th>
                    <?php }?>
                    </tr>
                  </tfoot>
                  <tbody> <!-- Tabel Relasi-->
                    <?php if($profile['id_jabatan']==0 ){ ?><!-- Tabel Relasi-->
              <?php $count=1; foreach($jabatan as $data): ?>
              <tr>
                <td><?= $count; ?></td>
                <td><?= $data['jabatan']; ?></td>
                <td><?= $data['jml_sekertaris']; ?></td>
                <td><?= $data['jml_staff']; ?></td>
                <td>
                    <a href="<?php echo base_url();?>admin/Table_admin/editdepartemen/<?= $data['id_jabatan']; ?>" class="badge badge-info" >Edit</a>
                    <a href="<?php echo base_url();?>admin/Table_admin/deletedepartemen/<?= $data['id_jabatan']; ?>" class="badge badge-danger" onclick="return confirm('Are You Sure?')">Delete</a>
                  </td>
              </tr>
            <?php $count++; endforeach;?>
            <?php }?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
