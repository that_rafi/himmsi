<!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Data <?= $tabletitle ?></h1>
          <p class="mb-4">Description.</p>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary" ><a href="<?php echo base_url();?>admin/Barang/inputbarang">Insert New Barang</a></h6>
            </div>
            <div class="card-body">
              <?= $this->session->flashdata('message'); ?>
              <div class="table-responsive" >
                <table class="table table-sm table-bordered" id="dataTable" width="100%" cellspacing="0" >
                  <thead>
                    <tr>
                      <?php for($i=0;$i<count($header);$i++){ ?>
                      <th><?= $header[$i]; ?></th>
                    <?php }?>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <?php for($i=0;$i<count($header);$i++){ ?>
                      <th><?= $header[$i]; ?></th>
                    <?php }?>
                    </tr>
                  </tfoot>
                  <tbody> <!-- Tabel Relasi-->
                  <?php if($akses['m_inventaris'] ==1){ $collect = $AllBarang;?>
                    <?php foreach($collect as $data): ?>
                    <tr>
                      <td><?= $data['id_barang']; ?></td>
                      <td><?= $data['nama']; ?></td>
                      <td><?= $data['jumlah']; ?></td>
                      <td><img src="<?php echo base_url()?>upload/barang/<?= $data['gambar']; ?>" width="50px" height="50px"></td>
                      <td><?= $data['keterangan']; ?></td>
                      <td><?= $data['harga']; ?></td>
                      <td><?= $data['tipe']; ?></td>
                        <td><a href="<?php echo base_url();?>admin/Barang/editbarang/<?= $data['id_barang'] ?>"class="badge badge-info">Edit</a>
                          <a href="<?php echo base_url();?>admin/Barang/deletebarang/<?= $data['id_barang'] ?>" class="badge badge-danger" onclick="return confirm('Are You Sure?')">Delete</a></td>
                    </tr>
                  <?php endforeach;?>
                <?php }?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
