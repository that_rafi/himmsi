<!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800"><?= $tabletitle ?></h1>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary" ><a href="<?php echo base_url();?>admin/Aspirasi/inputaspirasi">Tambah Aspirasi</a></h6>
            </div>
            <div class="card-body">
              <?= $this->session->flashdata('message'); ?>
              <div class="table-responsive" >
                <table class="table table-sm table-bordered" id="dataTable" width="100%" cellspacing="0" >
                  <thead>
                    <tr>
                      <?php for($i=0;$i<count($header);$i++){ ?>
                      <th><?= $header[$i]; ?></th>
                    <?php }?>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <?php for($i=0;$i<count($header);$i++){ ?>
                      <th><?= $header[$i]; ?></th>
                    <?php }?>
                    </tr>
                  </tfoot>
                  <tbody>
                    <?php if($akses['m_aspirasi'] ==1){ ?>
              <?php $count=1; foreach($aspirasi as $data): ?>
              <tr>
                <td><?= $count; ?></td>
                <td><?= $data['jurusan']; ?></td>
                <td><?= $data['angkatan']; ?></td>
                <td><?= $data['nama']; ?></td>
                <td><?= $data['nim']; ?></td>
                <td><?= $data['email']; ?></td>
                <td><?= $data['hp']; ?></td>
                <td><?= $data['aspirasi']; ?></td>
                <td><?= $data['tgl']; ?></td>
                <td><?= $data['jawaban']; ?></td>
                <td><?= $data['status']; ?></td>
                <td>
                  <?php if($data['status']=='tolak' || ($data['status']=='setujui' && $data['jawaban']!="-") ){?>
                    <a href="<?php echo base_url();?>admin/Aspirasi/deleteaspirasi/<?= $data['id_aspirasi']; ?>" class="badge badge-danger" onclick="return confirm('Are You Sure?')">Delete</a>
                  <?php }else if($data['status']=='setujui' && $data['jawaban']=="-"){?>
                    <a href="<?php echo base_url();?>admin/Aspirasi/updatejawaban/<?= $data['id_aspirasi']; ?>"class="badge badge-info">Update Jawaban</a>
                  <?php }else{?>
                      <a href="<?php echo base_url();?>admin/Aspirasi/updatestatus/<?= $data['id_aspirasi']; ?>"class="badge badge-info">Edit Status</a>
                  <?php }?>
                  </td>
              </tr>
            <?php $count++; endforeach;?>
            <?php }?>
                  </tbody>
                </table>
                  <?= $this->pagination->create_links(); ?>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
