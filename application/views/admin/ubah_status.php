<div class="container">

  <!-- Outer Row -->
  <div class="row justify-content-center">

    <div class="col-xl-10 col-lg-12 col-md-9">

      <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
          <!-- Nested Row within Card Body -->
          <div class="row">
            <div class="col-lg-6 d-none d-lg-block " style="background-image: url('<?php echo base_url()?>upload/randompic/cats_white.jpg')"></div>
            <div class="col-lg-6">
              <div class="p-5">
                <div class="text-center">
                  <h1 class="h4 text-gray-900 mb-2"><?= $title; ?></h1>
                  <p class="mb-4"></p>
                </div>
                  <?= $this->session->flashdata('message'); ?>
                  <?php if($title =="Edit Status Artikel"){$id = $result['id_artikel'];}else if($title=="Edit Status Aspirasi"){$id = $result['id_aspirasi'];}else{ $id =$result['id_proker'];}?>
                <form class="user" method="post" action="<?php echo base_url();?>admin/<?= $path; ?><?= $id; ?>">
                  <select class="custom-select mb-3" name="status">
                    <option value="0" selected="selected">Status</option>
                    <?php for($i=0;$i<count($status);$i++){
                      if($result['status']==$status[$i]){?>
                        <option value="<?= $status[$i]; ?>"selected="selected" ><?= $status[$i];?></option>
                    <?php }else{ ?>
                        <option value="<?= $status[$i]; ?>" ><?= $status[$i];?></option>
                    <?php }} ?>
                  </select>
                    <?= form_error('status','<small class="text-danger pl-3">','</small>'); ?>
                  <input type="submit" value="Change Status" name="submit" class="btn btn-primary btn-user btn-block"></input>
                </form>
                <hr>
                <div class="text-center">
                  <a class="small" href="<?php echo base_url();?>admin/<?= $home;?>">Back to Home</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>

  </div>

</div>
