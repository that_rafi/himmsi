<!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Data <?= $tabletitle ?></h1>
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
            </div>
            <div class="card-body">
              <?= $this->session->flashdata('message'); ?>
              <div class="table-responsive" >
                <table class="table table-sm table-bordered" id="dataTable" width="100%" cellspacing="0" >
                  <thead>
                    <tr>
                      <?php for($i=0;$i<count($header);$i++){ ?>
                      <th><?= $header[$i]; ?></th>
                    <?php }?>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <?php for($i=0;$i<count($header);$i++){ ?>
                      <th><?= $header[$i]; ?></th>
                    <?php }?>
                    </tr>
                  </tfoot>
                  <tbody> <!-- Tabel Relasi-->
              <?php foreach($AllDataExpo as $data): ?>
              <tr>
                <td><?= $data['id_dataexpo']; ?></td>
                <td><?= $data['nama']; ?></td>
                <td><?= $data['nim']; ?></td>
                <td><?= $data['kelas']; ?></td>
                <td><?= $data['gender']; ?></td>
                <td><?= $data['jurusan']; ?></td>
                <td><?= $data['tgl_lahir']; ?></td>
                <td><?= $data['alamat_asal']; ?></td>
                <td><?= $data['alamat_sini']; ?></td>
                <td><?= $data['jabatan']; ?></td>
                <td><?= $data['no_wa']; ?></td>
                <td><?= $data['email']; ?></td>
                <td><?= $data['alasan']; ?></td>
                <td><?= $data['bayar']; ?></td>
                <td><?= $data['status']; ?></td>
                <td>
                    <a href="<?php echo base_url();?>admin/Data_Expo/update_status_expo/<?= $data['id_dataexpo']; ?>" class="badge badge-info" >Update Status</a>
                    <a href="<?php echo base_url();?>admin/Data_Expo/delete_expo/<?= $data['id_dataexpo']; ?>" class="badge badge-danger" onclick="return confirm('Are You Sure?')">Delete</a>
                  </td>
              </tr>
            <?php endforeach;?>
                  </tbody>
                </table>
                  <?= $this->pagination->create_links(); ?>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
