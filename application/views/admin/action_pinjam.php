<div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-5 d-none d-lg-block "style="background-image: url('<?php echo base_url()?>upload/randompic/<?= $pic; ?>')"></div>
          <div class="col-lg-7">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4"><?= $title ?></h1>
              </div>
                <?= $this->session->flashdata('message'); ?>
                <?= $this->session->flashdata('error'); ?>
              <form class="user" method="post" action="<?php echo base_url();?>admin/<?= $path ?>" enctype="multipart/form-data">
                <select class="custom-select mb-3" name="barang">
                    <option value="0" selected="selected" >Barang</option>
                  <?php foreach($barang as $p ):
                    if($p['nama']==$edit['nama']){?>
                      <option value="<?= $p['id_barang']; ?>" selected="selected"><?=$p['nama']; ?></option>
                  <?php }else{?>
                    <option value="<?= $p['id_barang']; ?>" ><?=$p['nama']; ?></option>
                  <?php  }endforeach; ?>
                </select>
                  <?= form_error('barang','<small class="text-danger pl-3">','</small>'); ?>
                <div class="form-group">
                  <input type="text" name="nama" class="form-control form-control-user" id="exampleFirstName" placeholder="Nama Peminjam" value="<?php if($edit == null){echo set_value('nama');}else{echo $edit['nama_peminjam'];}  ?>"  >
                  <?= form_error("nama",'<small class="text-danger pl-3">','</small>'); ?>
                </div>
                <div class="form-group">
                  <input type="date" name="datep" class="form-control form-control-user"  placeholder="Tanggal Pinjam" value="<?php if($edit == null){echo set_value('datep');}else{echo $edit['tgl_pinjam'];}  ?>">
                  <?= form_error('datep','<small class="text-danger pl-3">','</small>'); ?>
                </div>
                <div class="form-group">
                  <input type="date" name="datek" class="form-control form-control-user"  placeholder="Tanggal Kembali" value="<?php if($edit == null){echo set_value('datek');}else{echo $edit['tgl_kembali'];}  ?>">
                </div>
                <div class="form-group">
                  <textarea class="form-control form-control-user" name="kpinjam" placeholder="Keadaan Pinjam..." ><?php if($edit == null){echo set_value('kpinjam');}else{echo $edit['keadaan_pinjam'];}  ?></textarea>
                  <?= form_error('kpinjam','<small class="text-danger pl-3">','</small>'); ?>
                </div>
                <div class="form-group">
                  <textarea class="form-control form-control-user" name="kkembali" placeholder="Keadaan Kembali..." ><?php if($edit == null){echo set_value('kkembali');}else{echo $edit['keadaan_kembali'];}  ?></textarea>
                </div>
                <div class="form-group">
                  <input type="number" name="jmlp" class="form-control form-control-user" id="exampleFirstName" placeholder="Jumlah Pinjaman" value="<?php if($edit == null){echo set_value('jmlp');}else{echo $edit['jml_pinjam'];}  ?>"  >
                  <?= form_error("jmlp",'<small class="text-danger pl-3">','</small>'); ?>
                </div>
                <div class="form-group">
                  <input type="text" name="jmlk" class="form-control form-control-user" id="exampleFirstName" placeholder="Jumlah Kembali" value="<?php if($edit == null){echo set_value('jmlk');}else{echo $edit['jml_kembali'];}  ?>"  >
                </div>
                <input type="submit" value="<?= $btn; ?>" class="btn btn-primary btn-user btn-block"></input>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
