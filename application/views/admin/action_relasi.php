<div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-5 d-none d-lg-block "style="background-image: url('<?php echo base_url()?>upload/randompic/<?= $pic; ?>')"></div>
          <div class="col-lg-7">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4"><?= $titleproker ?></h1>
              </div>
                <?= $this->session->flashdata('message'); ?>
                <?= $this->session->flashdata('error'); ?>
              <form class="user" method="post" action="<?php echo base_url();?>admin/<?= $path ?>" enctype="multipart/form-data">
                <div class="form-group">
                    <input type="text" name="nama" class="form-control form-control-user" id="exampleFirstName" placeholder="Relation Name" value="<?= $edit['nama_relasi']; ?>" >
                    <?= form_error('nama','<small class="text-danger pl-3">','</small>'); ?>
                </div>
                <div class="form-group">
                    <input type="text" name="org" class="form-control form-control-user" id="exampleFirstName" placeholder="Organization" value="<?= $edit['organisasi']; ?>" >
                    <?= form_error('org','<small class="text-danger pl-3">','</small>'); ?>
                </div>
                <div class="form-group">
                    <input type="tel" name="no" class="form-control form-control-user" id="exampleFirstName" placeholder="Phone No" value="<?= $edit['no_hp']; ?>" >
                    <?= form_error('no','<small class="text-danger pl-3">','</small>'); ?>
                </div>
                <div class="form-group">
                    <input type="email" name="email" class="form-control form-control-user" id="exampleFirstName" placeholder="Email" value="<?= $edit['email']; ?>" >
                    <?= form_error('email','<small class="text-danger pl-3">','</small>'); ?>
                </div>
                <div class="form-group">
                  <textarea class="form-control form-control-user" name="address" placeholder="Address"><?= $edit['alamat']; ?></textarea>
                  <?= form_error('address','<small class="text-danger pl-3">','</small>'); ?>
                </div>
                <div class="form-group">
                    <input type="text" name="jurusan" class="form-control form-control-user" id="exampleFirstName" placeholder="Jurusan" value="<?= $edit['jurusan']; ?>" >
                    <?= form_error('jurusan','<small class="text-danger pl-3">','</small>'); ?>
                </div>
                <div class="form-group">
                    <input type="text" name="kampus" class="form-control form-control-user" id="exampleFirstName" placeholder="Kampus" value="<?= $edit['kampus']; ?>" >
                    <?= form_error('kampus','<small class="text-danger pl-3">','</small>'); ?>
                </div>
                <div class="form-group">
                  <textarea class="form-control form-control-user" name="ket" placeholder="Detail"><?= $edit['keterangan']; ?></textarea>
                  <?= form_error('ket','<small class="text-danger pl-3">','</small>'); ?>
                </div>
                <input type="submit" value="<?= $btn; ?>" class="btn btn-primary btn-user btn-block"></input>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
