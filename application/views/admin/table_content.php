<!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Data <?= $tabletitle ?></h1>
          <p class="mb-4">Description.</p>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary" ><a href="<?php echo base_url();?>admin/Content/inputcontent/<?= strtolower($tabletitle); ?>">Insert New <?= $tabletitle ?></a></h6>
            </div>
            <div class="card-body">
              <?= $this->session->flashdata('message'); ?>
              <div class="table-responsive" >
                <table class="table table-sm table-bordered" id="dataTable" width="100%" cellspacing="0" >
                  <thead>
                    <tr>
                      <?php for($i=0;$i<count($header);$i++){ ?>
                      <th><?= $header[$i]; ?></th>
                    <?php }?>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <?php for($i=0;$i<count($header);$i++){ ?>
                      <th><?= $header[$i]; ?></th>
                    <?php }?>
                    </tr>
                  </tfoot>
                  <tbody> <!-- Tabel Relasi-->
                    <?php if($akses['m_banner'] ==1 ){?><!-- Tabel Artikel-->
                  <?php if($flag){   ?>
              <?php foreach($content as $data): ?>
              <tr>
                <td><?= $data['id_banner']; ?></td>
                <td><?= $data['nama']; ?></td>
                <td><img src="<?php echo base_url()?>upload/banner/<?= $data['gambar']; ?>" width="50px" height="50px"></td>
                <td><?= $data['link']; ?></td>
                <td><?= $data['keterangan']; ?></td>
                <td><?= $data['tipe']; ?></td>
                <td><?php if($iduser == 8){?>
                      <a href="<?php echo base_url();?>admin/Content/editcontent/banner/<?= $data['id_banner']; ?>"class="badge badge-info">Edit</a>
                  <?php }?>
                      <a href="<?php echo base_url();?>admin/Content/deletecontent/banner/<?= $data['id_banner']; ?>" class="badge badge-danger" onclick="return confirm('Are You Sure?')">Delete</a></td>
              </tr>
            <?php endforeach;?>
          <?php }else{?>
            <?php foreach($content as $data): ?>
            <tr>
              <td><?= $data['id_slider']; ?></td>
              <td><?= $data['judul']; ?></td>
              <td><?= $data['keterangan']; ?></td>
              <td><img src="<?php echo base_url()?>upload/slider/<?= $data['gambar']; ?>" width="50px" height="50px"></td>
              <td><?php if($iduser == 8){?>
                    <a href="<?php echo base_url();?>admin/Content/editcontent/slider/<?= $data['id_slider']; ?>"class="badge badge-info">Edit</a>
                <?php }?>
                    <a href="<?php echo base_url();?>admin/Content/deletecontent/slider/<?= $data['id_slider']; ?>" class="badge badge-danger" onclick="return confirm('Are You Sure?')">Delete</a></td>
            </tr>
          <?php endforeach;}}?>
                  </tbody>
                </table>

              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
