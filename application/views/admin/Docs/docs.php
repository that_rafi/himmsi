

<body>    
    <header class="header fixed-top">	    
        <div class="branding docs-branding">
            <div class="container-fluid position-relative py-2">
                <div class="docs-logo-wrapper">
	                <div class="site-logo"><a class="navbar-brand" href="index.html"><img class="logo-icon mr-2" src="<?= base_url() ?>public/documentation/assets/images/coderdocs-logo.svg" alt="logo"><span class="logo-text">HIMMSI<span class="text-alt">Docs</span></span></a></div>    
                </div><!--//docs-logo-wrapper-->
	            
            </div><!--//container-->
        </div><!--//branding-->
    </header><!--//header-->
    
    
    <!-- <div class="page-header theme-bg-dark py-5 text-center position-relative">
	    <div class="theme-bg-shapes-right"></div>
	    <div class="theme-bg-shapes-left"></div>
	    <div class="container">
		    <h1 class="page-heading single-col-max mx-auto">Documentation</h1>
		    <div class="page-intro single-col-max mx-auto">Everything you need to get your software documentation online.</div>
		    <div class="main-search-box pt-3 d-block mx-auto">
                 <form class="search-form w-100">
		            <input type="text" placeholder="Search the docs..." name="search" class="form-control search-input">
		            <button type="submit" class="btn search-btn" value="Search"><i class="fas fa-search"></i></button>
		        </form>
             </div>
	    </div>
	</div> -->
	<!--//page-header-->
    <div class="page-content">
	    <div class="container">
		    <div class="docs-overview py-5">
			    <div class="row justify-content-center">
				    <div class="col-12 col-lg-4 py-3">
					    <div class="card shadow-sm">
						    <div class="card-body">
							    <h5 class="card-title mb-3">
								    <span class="theme-icon-holder card-icon-holder mr-2">
								        <i class="fas fa-map-signs"></i>
							        </span><!--//card-icon-holder-->
							        <span class="card-title-text">Pengenalan</span>
							    </h5>
							    <div class="card-text">
								    Berisi informasi teknologi yang digunakan dan kebutuhan fungsional website himmsi
							    </div>
							    <a class="card-link-mask" href="<?= base_url() ?>admin/Docs/page#section-1"></a>
						    </div><!--//card-body-->
					    </div><!--//card-->
				    </div><!--//col-->
				    <div class="col-12 col-lg-4 py-3">
					    <div class="card shadow-sm">
						    <div class="card-body">
							    <h5 class="card-title mb-3">
								    <span class="theme-icon-holder card-icon-holder mr-2">
								        <i class="fas fa-arrow-down"></i>
							        </span><!--//card-icon-holder-->
							        <span class="card-title-text">Installation</span>
							    </h5>
							    <div class="card-text">
								    Cara instalasi website mulai dari hosting,backup,dan database
							    </div>
							    <a class="card-link-mask" href="<?= base_url() ?>admin/Docs/page#section-2"></a>
						    </div><!--//card-body-->
					    </div><!--//card-->
				    </div><!--//col-->
				    <div class="col-12 col-lg-4 py-3">
					    <div class="card shadow-sm">
						    <div class="card-body">
							    <h5 class="card-title mb-3">
								    <span class="theme-icon-holder card-icon-holder mr-2">
								        <i class="fas fa-cogs fa-fw"></i>
							        </span><!--//card-icon-holder-->
							        <span class="card-title-text">Integrations</span>
							    </h5>
							    <div class="card-text">
								   Integrasi dengan GIT HUB,Aplikasi Pihak ke 3 yang digunakan website HIMMSI					    
								</div>
							    <a class="card-link-mask" href="<?= base_url() ?>admin/Docs/page#section-4"></a>
						    </div><!--//card-body-->
					    </div><!--//card-->
				    </div><!--//col-->
				    <div class="col-12 col-lg-4 py-3">
					    <div class="card shadow-sm">
						    <div class="card-body">
							    <h5 class="card-title mb-3">
								    <span class="theme-icon-holder card-icon-holder mr-2">
								        <i class="fas fa-book"></i>
							        </span>
							        <span class="card-title-text">Article</span>
							    </h5>
							    <div class="card-text">
								    Informasi mengenai pengelolaan Artikel HIMMSI					    
								</div>
							    <a class="card-link-mask" href="<?= base_url() ?>admin/Docs/page#section-5"></a>
						    </div>
					    </div>
				    </div>
				    <div class="col-12 col-lg-4 py-3">
					    <div class="card shadow-sm">
						    <div class="card-body">
							    <h5 class="card-title mb-3">
								    <span class="theme-icon-holder card-icon-holder mr-2">
								        <i class="fas fa-laptop-code"></i>
							        </span><!--//card-icon-holder-->
							        <span class="card-title-text">PBT Online</span>
							    </h5>
							    <div class="card-text">
								    Informasi mengenai PBT Online dan pengelolaanya						    
								</div>
							    <a class="card-link-mask" href="<?= base_url() ?>admin/Docs/page#section-6"></a>
						    </div><!--//card-body-->
					    </div><!--//card-->
				    </div><!--//col-->
				    <div class="col-12 col-lg-4 py-3">
					    <div class="card shadow-sm">
						    <div class="card-body">
							    <h5 class="card-title mb-3">
								    <span class="theme-icon-holder card-icon-holder mr-2">
								        <i class="fas fa-book-reader"></i>
							        </span><!--//card-icon-holder-->
							        <span class="card-title-text">Aspirasi Online</span>
							    </h5>
							    <div class="card-text">
								    Informasi mengenai Aspirasi Online dan pengelolaanya					    
								</div>
							    <a class="card-link-mask" href="<?= base_url() ?>admin/Docs/page#section-8"></a>
						    </div><!--//card-body-->
					    </div><!--//card-->
				    </div><!--//col-->
				    <div class="col-12 col-lg-4 py-3">
					    <div class="card shadow-sm">
						    <div class="card-body">
							    <h5 class="card-title mb-3">
								    <span class="theme-icon-holder card-icon-holder mr-2">
								        <i class="fas fa-lightbulb"></i>
							        </span><!--//card-icon-holder-->
							        <span class="card-title-text">FAQs</span>
							    </h5>
							    <div class="card-text">
								    Pertanyaan - pertanyaan umum					    
								</div>
							    <a class="card-link-mask" href="<?= base_url() ?>admin/Docs/page#section-9"></a>
						    </div><!--//card-body-->
					    </div><!--//card-->
				    </div><!--//col-->
			    </div><!--//row-->
		    </div><!--//container-->
		</div><!--//container-->
    </div><!--//page-content-->
               
 

