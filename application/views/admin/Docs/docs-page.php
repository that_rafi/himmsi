

<!--//header-->
<style >
@media only screen and (max-width: 600px) {
  section img {
    width:300px;
	height:120px;
  }
}
</style>
    <div class="docs-wrapper">
	    <div id="docs-sidebar" class="docs-sidebar">
		    <div class="top-search-box d-lg-none p-3">
                <form class="search-form">
		            <input type="text" placeholder="Search the docs..." name="search" class="form-control search-input">
		            <button type="submit" class="btn search-btn" value="Search"><i class="fas fa-search"></i></button>
		        </form>
            </div>
		    <nav id="docs-nav" class="docs-nav navbar">
			    <ul class="section-items list-unstyled nav flex-column pb-3">
				    <li class="nav-item section-title"><a class="nav-link scrollto active" href="#section-1"><span class="theme-icon-holder mr-2"><i class="fas fa-map-signs"></i></span>Pengenalan</a></li>
				    <li class="nav-item"><a class="nav-link scrollto" href="#item-1-1">Section Item 1.1</a></li>
				    <li class="nav-item"><a class="nav-link scrollto" href="#item-1-2">Section Item 1.2</a></li>
				    <li class="nav-item section-title mt-3"><a class="nav-link scrollto" href="#section-2"><span class="theme-icon-holder mr-2"><i class="fas fa-arrow-down"></i></span>Installation</a></li>
				    <li class="nav-item"><a class="nav-link scrollto" href="#item-2-1">Clone Project</a></li>
				    <li class="nav-item"><a class="nav-link scrollto" href="#item-2-2">Upload ke Hosting</a></li>
				    <li class="nav-item section-title mt-3"><a class="nav-link scrollto" href="#section-4"><span class="theme-icon-holder mr-2"><i class="fas fa-cogs"></i></span>Integrations</a></li>
				    <li class="nav-item"><a class="nav-link scrollto" href="#item-4-1">Section Item 4.1</a></li>
					<li class="nav-item section-title mt-3"><a class="nav-link scrollto" href="#section-5"><span class="theme-icon-holder mr-2"><i class="fas fa-book"></i></span>Artikel</a></li>
				    <li class="nav-item"><a class="nav-link scrollto" href="#item-5-1">Olah Artikel HIMMSI</a></li>
				    <li class="nav-item"><a class="nav-link scrollto" href="#item-5-2">Update Status Tayangan Artikel</a></li>
					<li class="nav-item section-title mt-3"><a class="nav-link scrollto" href="#section-6"><span class="theme-icon-holder mr-2"><i class="fas fa-laptop-code"></i></span>PBT Online</a></li>
				    <li class="nav-item"><a class="nav-link scrollto" href="#item-6-1">Olah Materi PBT</a></li>
				    <li class="nav-item"><a class="nav-link scrollto" href="#item-6-2">Olah Peserta PBT</a></li>
				    <li class="nav-item section-title mt-3"><a class="nav-link scrollto" href="#section-8"><span class="theme-icon-holder mr-2"><i class="fas fa-book-reader"></i></span>Aspirasi Online</a></li>
				    <li class="nav-item"><a class="nav-link scrollto" href="#item-8-1">Update Status Aspirasi</a></li>
				    <li class="nav-item"><a class="nav-link scrollto" href="#item-8-2">Update Jawaban Aspirasi ( Follow up )</a></li>
				    <li class="nav-item section-title mt-3"><a class="nav-link scrollto" href="#section-9"><span class="theme-icon-holder mr-2"><i class="fas fa-lightbulb"></i></span>FAQs</a></li>
				    <li class="nav-item"><a class="nav-link scrollto" href="#item-9-1">Section Item 9.1</a></li>
				    <li class="nav-item"><a class="nav-link scrollto" href="#item-9-2">Section Item 9.2</a></li>
				    <li class="nav-item"><a class="nav-link scrollto" href="#item-9-3">Section Item 9.3</a></li>
			    </ul>

		    </nav><!--//docs-nav-->
	    </div><!--//docs-sidebar-->
	    <div class="docs-content">
		    <div class="container">
			    <article class="docs-article" id="section-1">
				    <header class="docs-header">
					    <h1 class="docs-heading">Introduction <span class="docs-time">Last updated: 2019-06-01</span></h1>
					    <section class="docs-intro">
						    <p>Section intro goes here. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque finibus condimentum nisl id vulputate. Praesent aliquet varius eros interdum suscipit. Donec eu purus sed nibh convallis bibendum quis vitae turpis. Duis vestibulum diam lorem, vitae dapibus nibh facilisis a. Fusce in malesuada odio.</p>
						</section><!--//docs-intro-->
						
						<h5>Github Code Example:</h5>
						<p>You can <a class="theme-link" href="https://gist.github.com/"  target="_blank">embed your code snippets using Github gists</a></p>
						<div class="docs-code-block">
							<!-- ** Embed github code starts ** -->
							<script src="https://gist.github.com/xriley/fce6cf71edfd2dadc7919eb9c98f3f17.js"></script>
							<!-- ** Embed github code ends ** -->
						</div><!--//docs-code-block-->
						
					     <h5>Highlight.js Example:</h5>
						<p>You can <a class="theme-link" href="https://github.com/highlightjs/highlight.js" target="_blank">embed your code snippets using highlight.js</a> It supports <a class="theme-link" href="https://highlightjs.org/static/demo/" target="_blank">185 languages and 89 styles</a>.</p>
						<p>This template uses <a class="theme-link" href="https://highlightjs.org/static/demo/" target="_blank">Atom One Dark</a> style for the code blocks: <br><code>&#x3C;link rel=&#x22;stylesheet&#x22; href=&#x22;//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.2/styles/atom-one-dark.min.css&#x22;&#x3E;</code></p>
						<div class="docs-code-block">
							<pre class="shadow-lg rounded"><code class="json hljs">[
  {
    <span class="hljs-attr">"title"</span>: <span class="hljs-string">"apples"</span>,
    <span class="hljs-attr">"count"</span>: [<span class="hljs-number">12000</span>, <span class="hljs-number">20000</span>],
    <span class="hljs-attr">"description"</span>: {<span class="hljs-attr">"text"</span>: <span class="hljs-string">"..."</span>, <span class="hljs-attr">"sensitive"</span>: <span class="hljs-literal">false</span>}
  },
  {
    <span class="hljs-attr">"title"</span>: <span class="hljs-string">"oranges"</span>,
    <span class="hljs-attr">"count"</span>: [<span class="hljs-number">17500</span>, <span class="hljs-literal">null</span>],
    <span class="hljs-attr">"description"</span>: {<span class="hljs-attr">"text"</span>: <span class="hljs-string">"..."</span>, <span class="hljs-attr">"sensitive"</span>: <span class="hljs-literal">false</span>}
  }
]


</code></pre>
						</div><!--//docs-code-block-->
						
						
				    </header>
				    <section class="docs-section" id="item-1-1">
						<h2 class="section-heading">Section Item 1.1</h2>
						<p>Vivamus efficitur fringilla ullamcorper. Cras condimentum condimentum mauris, vitae facilisis leo. Aliquam sagittis purus nisi, at commodo augue convallis id. </p>
						<p>Code Example: <code>npm install &lt;package&gt;</code></p>
						<h5>Unordered List Examples:</h5>
						<ul>
						    <li><strong class="mr-1">HTML5:</strong> <code>&lt;div id="foo"&gt;</code></li>
						    <li><strong class="mr-1">CSS:</strong> <code>#foo { color: red }</code></li>
						    <li><strong class="mr-1">JavaScript:</strong> <code>console.log(&#x27;#foo\bar&#x27;);</code></li>
						</ul>
						<h5>Ordered List Examples:</h5>
						<ol>
							<li>Download lorem ipsum dolor sit amet.</li>
							<li>Click ipsum faucibus venenatis.</li>
							<li>Configure fermentum malesuada nunc.</li>
							<li>Deploy donec non ante libero.</li>
						</ol>
                        <h5>Callout Examples:</h5>
                        <div class="callout-block callout-block-info">
                            
                            <div class="content">
                                <h4 class="callout-title">
	                                <span class="callout-icon-holder mr-1">
		                                <i class="fas fa-info-circle"></i>
		                            </span><!--//icon-holder-->
	                                Note
	                            </h4>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium <code>&lt;code&gt;</code> , Nemo enim ipsam voluptatem quia voluptas <a href="#">link example</a> sit aspernatur aut odit aut fugit.</p>
                            </div><!--//content-->
                        </div><!--//callout-block-->
                        
                        <div class="callout-block callout-block-warning">
                            <div class="content">
                                <h4 class="callout-title">
	                                <span class="callout-icon-holder mr-1">
		                                <i class="fas fa-bullhorn"></i>
		                            </span><!--//icon-holder-->
	                                Warning
	                            </h4>
                                <p>Nunc hendrerit odio quis dignissim efficitur. Proin ut finibus libero. Morbi posuere fringilla felis eget sagittis. Fusce sem orci, cursus in tortor <a href="#">link example</a> tellus vel diam viverra elementum.</p>
                            </div><!--//content-->
                        </div><!--//callout-block-->
                        
                        <div class="callout-block callout-block-success">
                            <div class="content">
                                <h4 class="callout-title">
	                                <span class="callout-icon-holder mr-1">
		                                <i class="fas fa-thumbs-up"></i>
		                            </span><!--//icon-holder-->
	                                Tip
	                            </h4>
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. <a href="#">Link example</a> aenean commodo ligula eget dolor.</p>
                            </div><!--//content-->
                        </div><!--//callout-block-->
                        
                        <div class="callout-block callout-block-danger mr-1">
                            <div class="content">
                                <h4 class="callout-title">
	                                <span class="callout-icon-holder">
		                                <i class="fas fa-exclamation-triangle"></i>
		                            </span><!--//icon-holder-->
	                                Danger
	                            </h4>
                                <p>Morbi eget interdum sapien. Donec sed turpis sed nulla lacinia accumsan vitae ut tellus. Aenean vestibulum <a href="#">Link example</a> maximus ipsum vel dignissim. Morbi ornare elit sit amet massa feugiat, viverra dictum ipsum pellentesque. </p>
                            </div><!--//content-->
                        </div><!--//callout-block-->
                        
                        <h5 class="mt-5">Alert Examples:</h5>
                        <div class="alert alert-primary" role="alert">
						  This is a primary alert—check it out!
						</div>
						<div class="alert alert-secondary" role="alert">
						  This is a secondary alert—check it out!
						</div>
						<div class="alert alert-success" role="alert">
						  This is a success alert—check it out!
						</div>
						<div class="alert alert-danger" role="alert">
						  This is a danger alert—check it out!
						</div>
						<div class="alert alert-warning" role="alert">
						  This is a warning alert—check it out!
						</div>
						<div class="alert alert-info" role="alert">
						  This is a info alert—check it out!
						</div>
						<div class="alert alert-light" role="alert">
						  This is a light alert—check it out!
						</div>
						<div class="alert alert-dark" role="alert">
						  This is a dark alert—check it out!
						</div>
                        
                       
					</section><!--//section-->
					
					<section class="docs-section" id="item-1-2">
						<h2 class="section-heading">Section Item 1.2</h2>
						<p>Vivamus efficitur fringilla ullamcorper. Cras condimentum condimentum mauris, vitae facilisis leo. Aliquam sagittis purus nisi, at commodo augue convallis id. Sed interdum turpis quis felis bibendum imperdiet. Mauris pellentesque urna eu leo gravida iaculis. In fringilla odio in felis ultricies porttitor. Donec at purus libero. Vestibulum libero orci, commodo nec arcu sit amet, commodo sollicitudin est. Vestibulum ultricies malesuada tempor.</p>
						<h5>Image Lightbox Example:</h5>
						<figure class="figure docs-figure py-3">
						    <a href="assets/images/features/appify-theme-projects-overview-lg.jpg" data-title="Image Lightbox Example" data-toggle="lightbox"><img class="figure-img img-fluid shadow rounded" src="assets/images/features/appify-add-members.gif" alt="" style="width: 600px;"></a>
						    <figcaption class="figure-caption mt-3"><i class="fas fa-info-circle mr-2"></i>Credit: the above screencast is taken from <a  class="theme-link" href="https://themes.3rdwavemedia.com/bootstrap-templates/product/appify-bootstrap-4-admin-template-for-app-developers/" target="_blank"><i class="fas fa-external-link-alt"></i>Appify theme</a>.</figcaption>
						</figure>
						
						<h5>Custom Table:</h5>
						<div class="table-responsive my-4">
							<table class="table table-bordered">
								<tbody>
								    <tr>
									    <th class="theme-bg-light"><a class="theme-link" href="#">Option 1</a></th>
									    <td>Option 1 desc lorem ipsum dolor sit amet, consectetur adipiscing elit. </td>
									</tr>
									<tr>
									      <th class="theme-bg-light"><a class="theme-link" href="#">Option 2</a></th>
									      <td>Option 2 desc lorem ipsum dolor sit amet, consectetur adipiscing elit. </td>
									</tr>
									
									<tr>
									    <th class="theme-bg-light"><a class="theme-link" href="#">Option 3</a></th>
									    <td>Option 3 desc lorem ipsum dolor sit amet, consectetur adipiscing elit. </td>
									</tr>
									
									<tr>
									    <th class="theme-bg-light"><a class="theme-link" href="#">Option 4</a></th>
									    <td>Option 4 desc lorem ipsum dolor sit amet, consectetur adipiscing elit. </td>
									</tr>
								</tbody>
							</table>
						</div><!--//table-responsive-->
						<h5>Stripped Table:</h5>
						<div class="table-responsive my-4">
							<table class="table table-striped">
								<thead>
									<tr>
										<th scope="col">#</th>
										<th scope="col">First</th>
										<th scope="col">Last</th>
										<th scope="col">Handle</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<th scope="row">1</th>
										<td>Mark</td>
										<td>Otto</td>
										<td>@mdo</td>
									</tr>
									<tr>
										<th scope="row">2</th>
										<td>Jacob</td>
										<td>Thornton</td>
										<td>@fat</td>
									</tr>
									<tr>
										<th scope="row">3</th>
										<td>Larry</td>
										<td>the Bird</td>
										<td>@twitter</td>
									</tr>
								</tbody>
							</table>
						</div><!--//table-responsive-->
						<h5>Bordered Dark Table:</h5>
						<div class="table-responsive my-4">
							<table class="table table-bordered table-dark">
								<thead>
									<tr>
										<th scope="col">#</th>
										<th scope="col">First</th>
										<th scope="col">Last</th>
										<th scope="col">Handle</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<th scope="row">1</th>
										<td>Mark</td>
										<td>Otto</td>
										<td>@mdo</td>
									</tr>
									<tr>
										<th scope="row">2</th>
										<td>Jacob</td>
										<td>Thornton</td>
										<td>@fat</td>
									</tr>
									<tr>
										<th scope="row">3</th>
										<td>Larry</td>
										<td>the Bird</td>
										<td>@twitter</td>
									</tr>
								</tbody>
							</table>
						</div><!--//table-responsive-->
						
						
					</section><!--//section-->
					
					<section class="docs-section" id="item-1-3">
						<h2 class="section-heading">Section Item 1.3</h2>
						<p>Vivamus efficitur fringilla ullamcorper. Cras condimentum condimentum mauris, vitae facilisis leo. Aliquam sagittis purus nisi, at commodo augue convallis id. Sed interdum turpis quis felis bibendum imperdiet. Mauris pellentesque urna eu leo gravida iaculis. In fringilla odio in felis ultricies porttitor. Donec at purus libero. Vestibulum libero orci, commodo nec arcu sit amet, commodo sollicitudin est. Vestibulum ultricies malesuada tempor.</p>
						<h5>Badges Examples:</h5>
						<div class="my-4">
						    <span class="badge badge-primary">Primary</span>
							<span class="badge badge-secondary">Secondary</span>
							<span class="badge badge-success">Success</span>
							<span class="badge badge-danger">Danger</span>
							<span class="badge badge-warning">Warning</span>
							<span class="badge badge-info">Info</span>
							<span class="badge badge-light">Light</span>
							<span class="badge badge-dark">Dark</span>
						</div>
						<h5>Button Examples:</h5>
						<div class="row my-3">
                        <div class="col-md-6 col-12">
                                <ul class="list list-unstyled pl-0">
                                    <li><a href="#" class="btn btn-primary">Primary Button</a></li>
                                    <li><a href="#" class="btn btn-secondary">Secondary Button</a></li>
                                    <li><a href="#" class="btn btn-light">Light Button</a></li>
                                    <li><a href="#" class="btn btn-success">Succcess Button</a></li>
                                    <li><a href="#" class="btn btn-info">Info Button</a></li>
                                    <li><a href="#" class="btn btn-warning">Warning Button</a></li>
                                    <li><a href="#" class="btn btn-danger">Danger Button</a></li>
                                </ul>
                            </div>
                            <div class="col-md-6 col-12">
                                <ul class="list list-unstyled pl-0">
                                    <li><a href="#" class="btn btn-primary"><i class="fas fa-download mr-2"></i> Download Now</a></li>
                                    <li><a href="#" class="btn btn-secondary"><i class="fas fa-book mr-2"></i> View Docs</a></li>
                                    <li><a href="#" class="btn btn-light"><i class="fas fa-arrow-alt-circle-right mr-2"></i> View Features</a></li>
                                    <li><a href="#" class="btn btn-success"><i class="fas fa-code-branch mr-2"></i> Fork Now</a></li>
                                    <li><a href="#" class="btn btn-info"><i class="fas fa-play-circle mr-2"></i> Find Out Now</a></li>
                                    <li><a href="#" class="btn btn-warning"><i class="fas fa-bug mr-2"></i> Report Bugs</a></li>
                                    <li><a href="#" class="btn btn-danger"><i class="fas fa-exclamation-circle mr-2"></i> Submit Issues</a></li>
                                </ul>
                            </div>
                        </div><!--//row-->
                        
                        <h5>Progress Examples:</h5>
                        <div class="my-4">
	                        <div class="progress my-4">
								<div class="progress-bar bg-success" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
							<div class="progress my-4">
							    <div class="progress-bar bg-info" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
							<div class="progress my-4">
							    <div class="progress-bar bg-warning" role="progressbar" style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
							<div class="progress my-4">
							    <div class="progress-bar bg-danger" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
                        </div>
					</section><!--//section-->
					
					<section class="docs-section" id="item-1-4">
						<h2 class="section-heading">Section Item 1.4</h2>
						<p>Vivamus efficitur fringilla ullamcorper. Cras condimentum condimentum mauris, vitae facilisis leo. Aliquam sagittis purus nisi, at commodo augue convallis id. Sed interdum turpis quis felis bibendum imperdiet. Mauris pellentesque urna eu leo gravida iaculis. In fringilla odio in felis ultricies porttitor. Donec at purus libero. Vestibulum libero orci, commodo nec arcu sit amet, commodo sollicitudin est. Vestibulum ultricies malesuada tempor.</p>
						
						
						<h5>Pagination Example:</h5>
						<nav aria-label="Page navigation example">
						    <ul class="pagination pl-0">
							    <li class="page-item disabled"><a class="page-link" href="#">Previous</a></li>
							    <li class="page-item active"><a class="page-link" href="#">1</a></li>
							    <li class="page-item"><a class="page-link" href="#">2</a></li>
							    <li class="page-item"><a class="page-link" href="#">3</a></li>
							    <li class="page-item"><a class="page-link" href="#">Next</a></li>
						    </ul>
						</nav>
						
						<p>Vivamus efficitur fringilla ullamcorper. Cras condimentum condimentum mauris, vitae facilisis leo. Aliquam sagittis purus nisi, at commodo augue convallis id. Sed interdum turpis quis felis bibendum imperdiet. Mauris pellentesque urna eu leo gravida iaculis. In fringilla odio in felis ultricies porttitor. Donec at purus libero. Vestibulum libero orci, commodo nec arcu sit amet, commodo sollicitudin est. Vestibulum ultricies malesuada tempor.</p>
						
					</section><!--//section-->
					<section class="docs-section" id="item-1-5">
						<h2 class="section-heading">Section Item 1.5</h2>
						<p>Vivamus efficitur fringilla ullamcorper. Cras condimentum condimentum mauris, vitae facilisis leo. Aliquam sagittis purus nisi, at commodo augue convallis id. Sed interdum turpis quis felis bibendum imperdiet. Mauris pellentesque urna eu leo gravida iaculis. In fringilla odio in felis ultricies porttitor. Donec at purus libero. Vestibulum libero orci, commodo nec arcu sit amet, commodo sollicitudin est. Vestibulum ultricies malesuada tempor.</p>
					</section><!--//section-->
					<section class="docs-section" id="item-1-6">
						<h2 class="section-heading">Section Item 1.6</h2>
						<p>Vivamus efficitur fringilla ullamcorper. Cras condimentum condimentum mauris, vitae facilisis leo. Aliquam sagittis purus nisi, at commodo augue convallis id. Sed interdum turpis quis felis bibendum imperdiet. Mauris pellentesque urna eu leo gravida iaculis. In fringilla odio in felis ultricies porttitor. Donec at purus libero. Vestibulum libero orci, commodo nec arcu sit amet, commodo sollicitudin est. Vestibulum ultricies malesuada tempor.</p>
					</section><!--//section-->
					
			    </article>
			    
			    <article class="docs-article" id="section-2">
				    <header class="docs-header">
					    <h1 class="docs-heading">Installation</h1>
					    <section class="docs-intro">
						    <p>Panduan bagi pengurus baru untuk menginstall dan mengolah website HIMMSI. Tolong diperhatikan perbaruan/pengelolaan website hanya melalui <b>GitLab</b>, tidak diperkenankan mengubah codingan secara langsung melalui <b>Hosting</b></p>
						</section><!--//docs-intro-->
				    </header>
				     <section class="docs-section" id="item-2-1">
						<h2 class="section-heading">Clone Project GitLab</h2>
						<ul>
  							<li>Buatlah akun GitLab melalui <a href="https://gitlab.com">link ini</a></li>
							<img src="<?= base_url()?>upload/docs/git/gitlab.png" width="800px" height="400px" />
							<li>Jika sudah pastikan akun gitlab sudah <b>Terverifikasi</b> melalui email</li>
							<li>Minta <b>SuperAdmin</b> untuk menambahkan akun ke project HIMMSI di gitlab</li>
							<li>Langkah selanjutnya akan di jelaskan pada video berikut ini</li>
							<p><b>Persiapan :</b></p>
							<li>Gitbash ( untuk pengguna Window ), Mac OS tidak perlu </li>
							<li>XAMPP</li>
							<br/>
							<li>Video Installasi</li>
							<iframe src="https://drive.google.com/file/d/1RBKxuPjOYq3vppyzSuytPLP5V4KeNSEV/preview" ></iframe>
						</ul>
						<p></p>
					</section><!--//section-->
					
					<section class="docs-section" id="item-2-2">
						<h2 class="section-heading">Upload Ke Hosting</h2>
						<p>Berikut adalah panduan untuk mengupload hasil codingan/update fitur melalui gitlab, <b>Tidak diperkenankan mengubah coding melalui hosting secara langsung!!</b></p>
						<ul>
						<p><b>Persiapan :</b></p>
							<li>Sudah mendapat izin dan masuk ke dalam project HIMMSI di gitlab</li>
							<li>Gitbash ( untuk pengguna Window ), Mac OS tidak perlu </li>
							<li>XAMPP</li>
							<br/>
							<li>Video Installasi</li>
							<iframe src="https://drive.google.com/file/d/1PBOSxv7Rny_mW-jPgeeiZz9wRkN0_bCs/preview" ></iframe>
						</ul>
					</section><!--//section-->
			    </article><!--//docs-article-->
			    
			    
			    <article class="docs-article" id="section-3">
				    <header class="docs-header">
					    <h1 class="docs-heading">APIs</h1>
					    <section class="docs-intro">
						    <p>Section intro goes here. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque finibus condimentum nisl id vulputate. Praesent aliquet varius eros interdum suscipit. Donec eu purus sed nibh convallis bibendum quis vitae turpis. Duis vestibulum diam lorem, vitae dapibus nibh facilisis a. Fusce in malesuada odio.</p>
						</section><!--//docs-intro-->
				    </header>
				     <section class="docs-section" id="item-3-1">
						<h2 class="section-heading">Section Item 3.1</h2>
						<p>Vivamus efficitur fringilla ullamcorper. Cras condimentum condimentum mauris, vitae facilisis leo. Aliquam sagittis purus nisi, at commodo augue convallis id. Sed interdum turpis quis felis bibendum imperdiet. Mauris pellentesque urna eu leo gravida iaculis. In fringilla odio in felis ultricies porttitor. Donec at purus libero. Vestibulum libero orci, commodo nec arcu sit amet, commodo sollicitudin est. Vestibulum ultricies malesuada tempor.</p>
					</section><!--//section-->
					
					<section class="docs-section" id="item-3-2">
						<h2 class="section-heading">Section Item 3.2</h2>
						<p>Vivamus efficitur fringilla ullamcorper. Cras condimentum condimentum mauris, vitae facilisis leo. Aliquam sagittis purus nisi, at commodo augue convallis id. Sed interdum turpis quis felis bibendum imperdiet. Mauris pellentesque urna eu leo gravida iaculis. In fringilla odio in felis ultricies porttitor. Donec at purus libero. Vestibulum libero orci, commodo nec arcu sit amet, commodo sollicitudin est. Vestibulum ultricies malesuada tempor.</p>
					</section><!--//section-->
					
					<section class="docs-section" id="item-3-3">
						<h2 class="section-heading">Section Item 3.3</h2>
						<p>Vivamus efficitur fringilla ullamcorper. Cras condimentum condimentum mauris, vitae facilisis leo. Aliquam sagittis purus nisi, at commodo augue convallis id. Sed interdum turpis quis felis bibendum imperdiet. Mauris pellentesque urna eu leo gravida iaculis. In fringilla odio in felis ultricies porttitor. Donec at purus libero. Vestibulum libero orci, commodo nec arcu sit amet, commodo sollicitudin est. Vestibulum ultricies malesuada tempor.</p>
					</section><!--//section-->
			    </article><!--//docs-article-->
			    
			    <article class="docs-article" id="section-4">
				    <header class="docs-header">
					    <h1 class="docs-heading">Intergrations</h1>
					    <section class="docs-intro">
						    <p>Section intro goes here. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque finibus condimentum nisl id vulputate. Praesent aliquet varius eros interdum suscipit. Donec eu purus sed nibh convallis bibendum quis vitae turpis. Duis vestibulum diam lorem, vitae dapibus nibh facilisis a. Fusce in malesuada odio.</p>
						</section><!--//docs-intro-->
				    </header>
				     <section class="docs-section" id="item-4-1">
						<h2 class="section-heading">Section Item 4.1</h2>
						<p>Vivamus efficitur fringilla ullamcorper. Cras condimentum condimentum mauris, vitae facilisis leo. Aliquam sagittis purus nisi, at commodo augue convallis id. Sed interdum turpis quis felis bibendum imperdiet. Mauris pellentesque urna eu leo gravida iaculis. In fringilla odio in felis ultricies porttitor. Donec at purus libero. Vestibulum libero orci, commodo nec arcu sit amet, commodo sollicitudin est. Vestibulum ultricies malesuada tempor.</p>
					</section><!--//section-->
					
					<section class="docs-section" id="item-4-2">
						<h2 class="section-heading">Section Item 4.2</h2>
						<p>Vivamus efficitur fringilla ullamcorper. Cras condimentum condimentum mauris, vitae facilisis leo. Aliquam sagittis purus nisi, at commodo augue convallis id. Sed interdum turpis quis felis bibendum imperdiet. Mauris pellentesque urna eu leo gravida iaculis. In fringilla odio in felis ultricies porttitor. Donec at purus libero. Vestibulum libero orci, commodo nec arcu sit amet, commodo sollicitudin est. Vestibulum ultricies malesuada tempor.</p>
					</section><!--//section-->
					
					<section class="docs-section" id="item-4-3">
						<h2 class="section-heading">Section Item 4.3</h2>
						<p>Vivamus efficitur fringilla ullamcorper. Cras condimentum condimentum mauris, vitae facilisis leo. Aliquam sagittis purus nisi, at commodo augue convallis id. Sed interdum turpis quis felis bibendum imperdiet. Mauris pellentesque urna eu leo gravida iaculis. In fringilla odio in felis ultricies porttitor. Donec at purus libero. Vestibulum libero orci, commodo nec arcu sit amet, commodo sollicitudin est. Vestibulum ultricies malesuada tempor.</p>
					</section><!--//section-->
			    </article><!--//docs-article-->
			    
			    <article class="docs-article" id="section-5">
				    <header class="docs-header">
					    <h1 class="docs-heading">Artikel</h1>
					    <section class="docs-intro">
						    <p>Panduan untuk pengurus semua departemen dalam mengolah Artikel di website HIMMSI</p>
						</section><!--//docs-intro-->
				    </header>
				     <section class="docs-section" id="item-5-1">
						<h2 class="section-heading">Olah Artikel HIMMSI</h2>
						<p>Masuk ke website admin HIMMSI Kemudian klik menu di samping halaman <b>Artikel</b></p>
						<img src="<?= base_url()?>upload/docs/artikel/iconartikel.png" width="200px" height="50px" />
						<p/>
						<h3>1. Tambah Artikel</h3>
						<ul>
  							<li>Klik <b>Insert New Artikel</b></li>
							  <img src="<?= base_url()?>upload/docs/artikel/dataartikel.png" width="700px" height="250px" />
							  <li>Isikan Data Materi, Pastikan semua terisi</li>
							  <img src="<?= base_url()?>upload/docs/artikel/insertartikel.png" width="700px" height="300px" />
							  <li><b>Tema</b> artikel bisa diisikan kata kunci yang bisa mewakili artikel tersebut</li>
							  <li>Untuk <b>Kategori</b> artikel terdapat beberapa pilihan</li>
							  <img src="<?= base_url()?>upload/docs/artikel/kategori.png" width="700px" height="150px" />
							  <li><b>Kegiatan</b>, jika berkaitan dengan event HIMMSI <b>yang sudah diselenggarakan</b></li>
							  <li><b>Jurusan</b>, jika berkaitan dengan keakademikan prodi SI & MI</li>
							  <li><b>Umum</b>, jika berkaitan dengan artikel umum</li>
							  <li><b>Agenda</b>, jika berkaitan dengan event HIMMSI <b>yang akan diselenggarakan</b></li>
							  <li>Untuk <b>Tipe</b> artikel terdapat beberapa pilihan</li>
							  <img src="<?= base_url()?>upload/docs/artikel/tipeartikel.png" width="700px" height="150px" />
							  <li><b>Artikel</b>, jika artikel tidak ingin di tampilkan di slider website</li>
							  <li><b>Event</b>, jika artikel berupa artikel event dan ingin ditampilkan di slider</li>
							  <li><b>Profil</b>, jika artikel ingin di tampilkan di halaman profil HIMMSI</li>
							  <li>Kemudian klik <b>Add Artikel</b> untuk menambahkan artikel</li>
							  <li>Maka Artikel <b>Suksess</b> ditambahkan dan pastikan seperti gambar dibawah ini</li>
							  <img src="<?= base_url()?>upload/docs/artikel/successartikel.png" width="800px" height="200px" />
						</ul>
						<h3>2. Edit Artikel</h3>
						<ul>
  							<li>Pilih Artikel yang ingin di edit, Kemudian klik tombol <b>Edit</b></li> 
							  <li>Ubah data artikel, Pastikan semua terisi, Kemudian klik <b>Update Artikel</b></li>
							  <img src="<?= base_url()?>upload/docs/artikel/editartikel.png" width="700px" height="400px" />
							  <li>Maka Artikel <b>Suksess</b> di Edit </li>
						</ul>
						<h3>3. Hapus Artikel</h3>
						<ul>
  							<li>Pilih Artikel yang ingin di hapus, Kemudian klik tombol <b>Hapus</b></li>
							  <li>Maka akan muncul Pop Up, Kemudian Klik <b>Ok</b> untuk hapus <b>Cancel</b> untuk membatalkan </li>
							  <img src="<?= base_url()?>upload/docs/pbt/popupdelete.png" width="600px" height="200px" />
							  <li>Maka Artikel <b>Suksess</b> di hapus </li>
						</ul>
					</section><!--//section-->
					<section class="docs-section" id="item-5-2">
						<h2 class="section-heading">Update Status Tayangan Artikel HIMMSI</h2>
						<p>Panduan <b>Pengurus Departemen Eksternal</b> untuk mengubah status tayangan artikel yang telah diajukan oleh pengurus lainya untuk ditayangkan di website HIMMSI</p>
						<li>Pilihlah Artikel yang ingin diupdate status tayanganya, kemudian klik <b>Edit Status</b></li>
						<img src="<?= base_url()?>upload/docs/artikel/dataupdateartikel.png" width="800px" height="200px" />
						<li>Maka akan muncul form untuk mengubah status artikel tersebut, silahkan pertimbangkan dengan matang</li>
						<img src="<?= base_url()?>upload/docs/artikel/statusartikel.png" width="400px" height="200px" />
						<li>Kemudian klik <b>change status</b> untuk mengubah status tayangan artikel </li>
						<li>Maka status tayangan artikel <b>Sukses</b> diubah dan artikel telah tayang di website HIMMSI</li>
					    <img src="<?= base_url()?>upload/docs/artikel/successubah.png" width="800px" height="200px" />
					</section><!--//section-->
			    </article><!--//docs-article-->
			    
			    
		        <article class="docs-article" id="section-6">
				    <header class="docs-header">
					    <h1 class="docs-heading">PBT Online</h1>
					    <section class="docs-intro">
						    <p>Panduan Departemen Ristek mengolah PBT Online di halaman admin website HIMMSI</p>
						</section><!--//docs-intro-->
				    </header>
				     <section class="docs-section" id="item-6-1">
						<h2 class="section-heading">Olah Materi PBT</h2>
						<p>Masuk ke website admin HIMMSI dan Pastikan login sebagai <b>Pengurus RISTEK</b> Kemudian klik menu di samping halaman <b>Materi PBT</b></p>
						<img src="<?= base_url()?>upload/docs/pbt/iconmateri.png" width="200px" height="50px" />
						<p/>
						<h3>1. Tambah Materi PBT</h3>
						<ul>
  							<li>Klik <b>Insert New Materi</b></li>
							  <img src="<?= base_url()?>upload/docs/pbt/datamateri.PNG" width="700px" height="400px" />
							  <li>Isikan Data Materi, Pastikan semua terisi</li>
							  <img src="<?= base_url()?>upload/docs/pbt/isimaterinourl.png" width="700px" height="400px" />
							  <li>Untuk link banner pastikan merupakan <b>URL VIDEO GOOGLE DRIVE</b></li>
							  <img src="<?= base_url()?>upload/docs/pbt/drive.png" width="700px" height="400px" />
							  <li>Pastikan link url di set <b>PUBLIC</b> atau <b>On Universitas AMIKOM</b></li>
							  <img src="<?= base_url()?>upload/docs/pbt/drive2.png" width="700px" height="500px" />
							  <li>Copykan link ke form <b>Link banner</b> pada halaman tambah materi, Kemudian klik <b>add materi</b></li>
							  <img src="<?= base_url()?>upload/docs/pbt/isimateriurl.png" width="700px" height="400px" />
							  <li>Maka Materi <b>Suksess</b> ditambahkan dan Pastikan <b>Thumbnail Video</b> seperti gambar dibawah ini</li>
							  <img src="<?= base_url()?>upload/docs/pbt/successmateri.png" width="800px" height="200px" />
						</ul>
						<h3>2. Edit Materi PBT</h3>
						<ul>
  							<li>Pilih Materi yang ingin di edit, Kemudian klik tombol <b>Edit</b></li>
							  <img src="<?= base_url()?>upload/docs/pbt/datamateriedit.png" width="800px" height="60px" /> 
							  <li>Ubah data materi, Pastikan semua terisi, Kemudian klik <b>Edit Materi</b></li>
							  <img src="<?= base_url()?>upload/docs/pbt/editmateri.png" width="700px" height="400px" />
							  <li>Maka Materi <b>Suksess</b> di Edit </li>
							  <img src="<?= base_url()?>upload/docs/pbt/successeditmateri.png" width="700px" height="100px" />
						</ul>
						<h3>3. Hapus Materi PBT</h3>
						<ul>
  							<li>Pilih Materi yang ingin di hapus, Kemudian klik tombol <b>Hapus</b></li>
							  <img src="<?= base_url()?>upload/docs/pbt/datamateriedit.png" width="800px" height="60px" /> 
							  <li>Maka akan muncul Pop Up, Kemudian Klik <b>Ok</b> untuk hapus <b>Cancel</b> untuk membatalkan </li>
							  <img src="<?= base_url()?>upload/docs/pbt/popupdelete.png" width="600px" height="200px" />
							  <li>Maka Materi <b>Suksess</b> di hapus </li>
							  <img src="<?= base_url()?>upload/docs/pbt/successdelete.png" width="700px" height="100px" />
						</ul>
					</section><!--//section-->
					
					<section class="docs-section" id="item-6-2">
						<h2 class="section-heading">Olah Data Peserta</h2>
						<p>Masuk ke website admin HIMMSI dan Pastikan login sebagai <b>Pengurus RISTEK</b> Kemudian klik menu di samping halaman <b>Peserta PBT</b></p>
						<img src="<?= base_url()?>upload/docs/pbt/iconpeserta.png" width="200px" height="50px" />
						<p/>
						Maka akan muncul Data Peserta yang telah mengikuti PBT Online, Silahkan lakukan <b>Edit</b> atau <b>Hapus</b> data , <b>Jika diperlukan</b>
						<img src="<?= base_url()?>upload/docs/pbt/datapeserta.png" width="700px" height="400px" />
					</section><!--//section-->
			    </article><!--//docs-article-->
			    
			    <article class="docs-article" id="section-8">
				    <header class="docs-header">
					    <h1 class="docs-heading">Aspirasi Online</h1>
					    <section class="docs-intro">
						<p>Panduan Departemen Aspirasi dalam mengolah Aspirasi yang dikirim oleh mahasiswa program studi D3 Manajemen Informatika & S1 Sistem Informasi  melalui website HIMMSI</p>
						</section><!--//docs-intro-->
				    </header>
				     <section class="docs-section" id="item-8-1">
						<h2 class="section-heading">Update Status Aspirasi</h2>
						<p>Masuk ke website admin HIMMSI dan Pastikan login sebagai <b>Pengurus Departemen Aspirasi</b> Kemudian klik menu di samping halaman <b>Aspirasi Online</b></p>
						<img src="<?= base_url()?>upload/docs/aspirasi/iconaspirasi.png" width="200px" height="50px" />
						<p/>
						<p>Maka akan muncul Data Aspirasi yang telah terrecord ke sistem, data teratas merupakan <b>Aspirasi Mahasiswa yang terbaru </b></p>
						<img src="<?= base_url()?>upload/docs/aspirasi/dataaspirasi.png" width="800px" height="400px" />
						<ul>
  							<li>Pilih Aspirasi yang ingin di update statusnya pastikan statusnya masih <b>draft</b>, Kemudian klik tombol <b>Edit</b></li>
							  <li>Status dapat diubah menjadi <b>setujui,tolak,</b>atau <b> draft</b></li>
							  <img src="<?= base_url()?>upload/docs/aspirasi/statusaspirasi.png" width="500px" height="300px" />
							  <li>Pilih <b>setujui</b> jika aspirasi layak untuk diajukan ke lembaga ( membahas akademik, tidak vulgar,dll ) </li>
							  <li>Pilih <b>tolak</b> jika aspirasi tidak layak untuk diajukan ke lembaga ( mengandung unsur SARA, tidak membahak keakademikan, dll ) </li>
							  <li>Pilih <b>draft</b> jika ingin menyimpan aspirasi sebagai draft </li>
							  <li>Kemudian klik <b>change status</b> jika ingin mengubah status aspirasi </li>
							  <li>Maka Status aspirasi <b>Sukses</b> diubah dan email notifikasi akan dikirimkan ke email mahasiswa</li>
							  <img src="<?= base_url()?>upload/docs/aspirasi/successaspirasi.png" width="800px" height="200px" />
						</ul>
					</section><!--//section-->
					
					<section class="docs-section" id="item-8-2">
						<h2 class="section-heading">Update Jawaban Aspirasi ( Follow up )</h2>
						<p>Aspirasi mahasiswa yang <b>disetujui</b> dapat ditindaklanjuti dengan cara mengirimkan jawaban dari pihak lembaga atas aspirasi yang sudah dikirimkan. Kemudian 
						jawaban dari aspirasi ini nantinya akan dikirimkan ke email mahasiswa sesuai yang diinputkan pada saat mengirimkan aspirasi
						</p>
						<li>Pilihlah aspirasi yang ingin diupdate jawabanya, kemudian klik <b>update jawaban</b></li>
						<img src="<?= base_url()?>upload/docs/aspirasi/datadisetujui.png" width="800px" height="100px" />
						<li>Maka akan muncul form untuk mengisikan jawaban dari Aspirasi tersebut, silahkan isikan dengan benar</li>
						<img src="<?= base_url()?>upload/docs/aspirasi/formjawaban.png" width="400px" height="300px" />
						<li>Kemudian klik <b>change response</b> untuk mengirimkan jawaban </li>
						<li>Perlu diperhatikan, pastikan jawaban <b>diisi dengan benar</b> karena jawaban akan langsung dikirimkan ke <b>email mahasiswa</b></li>
						<li>Maka Jawaban aspirasi <b>Sukses</b> diubah dan email notifikasi akan dikirimkan ke email mahasiswa</li>
					    <img src="<?= base_url()?>upload/docs/aspirasi/successjawaban.png" width="800px" height="200px" />
					</section><!--//section-->
			    </article><!--//docs-article-->
			    
			    
			    <article class="docs-article" id="section-9">
				    <header class="docs-header">
					    <h1 class="docs-heading">FAQs</h1>
					    <section class="docs-intro">
						    <p>Section intro goes here. You can list all your FAQs using the format below.</p>
						</section><!--//docs-intro-->
				    </header>
				     <section class="docs-section" id="item-9-1">
						<h2 class="section-heading">Section Item 9.1 <small>(FAQ Category One)</small></h2>
						<h5 class="pt-3"><i class="fas fa-question-circle mr-1"></i>What's sit amet quam eget lacinia?</h5>
						<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.</p>
						<h5 class="pt-3"><i class="fas fa-question-circle mr-1"></i>How to ipsum dolor sit amet quam tortor?</h5>
						<p>Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. </p>
						<h5 class="pt-3"><i class="fas fa-question-circle mr-1"></i>Can I  bibendum sodales?</h5>
						<p>Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. </p>
						<h5 class="pt-3"><i class="fas fa-question-circle mr-1"></i>Where arcu sed urna gravida?</h5>
						<p>Aenean et sodales nisi, vel efficitur sapien. Quisque molestie diam libero, et elementum diam mollis ac. In dignissim aliquam est eget ullamcorper. Sed id sodales tortor, eu finibus leo. Vivamus dapibus sollicitudin justo vel fermentum. Curabitur nec arcu sed urna gravida lobortis. Donec lectus est, imperdiet eu viverra viverra, ultricies nec urna. </p>
					</section><!--//section-->
					
					<section class="docs-section" id="item-9-2">
						<h2 class="section-heading">Section Item 9.2 <small>(FAQ Category Two)</small></h2>
						<h5 class="pt-3"><i class="fas fa-question-circle mr-1"></i>What's sit amet quam eget lacinia?</h5>
						<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.</p>
						<h5 class="pt-3"><i class="fas fa-question-circle mr-1"></i>How to ipsum dolor sit amet quam tortor?</h5>
						<p>Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. </p>
						<h5 class="pt-3"><i class="fas fa-question-circle mr-1"></i>Can I  bibendum sodales?</h5>
						<p>Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. </p>
						<h5 class="pt-3"><i class="fas fa-question-circle mr-1"></i>Where arcu sed urna gravida?</h5>
						<p>Aenean et sodales nisi, vel efficitur sapien. Quisque molestie diam libero, et elementum diam mollis ac. In dignissim aliquam est eget ullamcorper. Sed id sodales tortor, eu finibus leo. Vivamus dapibus sollicitudin justo vel fermentum. Curabitur nec arcu sed urna gravida lobortis. Donec lectus est, imperdiet eu viverra viverra, ultricies nec urna. </p>
					</section><!--//section-->
					
					<section class="docs-section" id="item-9-3">
							<h2 class="section-heading">Section Item 9.3 <small>(FAQ Category Three)</small></h2>
						    <h5 class="pt-3"><i class="fas fa-question-circle mr-1"></i>How to dapibus sollicitudin justo vel fermentum?</h5>
							<p>Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. </p>
							<h5 class="pt-3"><i class="fas fa-question-circle mr-1"></i>How long bibendum sodales?</h5>
							<p>Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. </p>
							<h5 class="pt-3"><i class="fas fa-question-circle mr-1"></i>Where dapibus sollicitudin?</h5>
							<p>Aenean et sodales nisi, vel efficitur sapien. Quisque molestie diam libero, et elementum diam mollis ac. In dignissim aliquam est eget ullamcorper. Sed id sodales tortor, eu finibus leo. Vivamus dapibus sollicitudin justo vel fermentum. Curabitur nec arcu sed urna gravida lobortis. Donec lectus est, imperdiet eu viverra viverra, ultricies nec urna. </p>
					</section><!--//section-->
				</article><!--//docs-article-->
				



