<div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <?php
          if($profile['foto'] != null){
            $pic = $profile['foto'];
          }else{
            $pic = "default.jpg";
          }
          ?>
          <div class="col-lg-5 d-none d-lg-block " style="background-image: url('<?php echo base_url()?>upload/adminpic/<?=$pic;  ?>'); background-size:cover"></div>
          <div class="col-lg-7">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4"><?= $title; ?></h1>
              </div>
                <?= $this->session->flashdata('message'); ?>
                <?= $this->session->flashdata('error'); ?>
              <form class="profile" method="post" action="<?php echo base_url();?>admin/<?= $role; ?>/updateprofile/<?= $profile['id_user']; ?>" enctype="multipart/form-data">
                <div class="form-group">
                    <input type="text" name="nama" class="form-control form-control-profile" id="exampleFirstName" placeholder="Full Name" value="<?= $profile['nama']; ?>">
                    <?= form_error('nama','<small class="text-danger pl-3">','</small>'); ?>
                </div>
                <div class="form-group">
                  <input type="email" name="email" class="form-control form-control-profile" id="exampleInputEmail" placeholder="Email Address" value="<?= $profile['email']; ?>">
                  <?= form_error('email','<small class="text-danger pl-3">','</small>'); ?>
                </div>
                <div class="form-group">
                  <input type="tel" name="telp" class="form-control form-control-profile"  placeholder="Phone number" value="<?= $profile['telp']; ?>">
                  <?= form_error('telp','<small class="text-danger pl-3">','</small>'); ?>
                </div>
                <div class="row mb-3">
                  <div class="col-lg-6" ><div class="text ml-2">Photo Profile</div></div>
                  <div class="col-lg-6">
                    <div class="custom-file">
                      <input type="hidden" name="old_image" class="custom-file-input" id="customFile" accept="image/*" value="<?= $profile['foto']; ?>">
                      <input type="file" name="foto" class="custom-file-input" id="customFile" accept="image/*">
                      <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                  </div>
                </div>
                <?= form_error('foto','<small class="text-danger pl-3">','</small>'); ?>
                <input type="submit" value="Update Profile" class="btn btn-primary btn-profile btn-block"></input>
              </form>
          </div>
        </div>
      </div>
    </div>

  </div>
