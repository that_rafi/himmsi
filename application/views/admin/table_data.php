<!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800"><?= $tabletitle ?></h1>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <?php if($profile['id_jabatan']==0) {?>
              <h6 class="m-0 font-weight-bold text-primary" ><a href="<?php echo base_url();?>admin/Table_admin/insertadmin">Tambah Admin</a></h6>
            <?php }else if($akses['m_prokermanage'] ==1){ ?>
              <h6 class="m-0 font-weight-bold text-primary" ><a href="<?php echo base_url();?>admin"></a></h6>
            <?php }else if($akses['m_proker'] == 1){?>
              <h6 class="m-0 font-weight-bold text-primary" ><a href="<?php echo base_url();?>admin/Proker/inputproker">Tambah Proker</a></h6>
            <?php }else{ ?>
              <h6 class="m-0 font-weight-bold text-primary" ><a href="<?php echo base_url();?>admin/">Insert New Data</a></h6>
            <?php }?>
            </div>
            <div class="card-body">
              <?= $this->session->flashdata('message'); ?>
              <div class="table-responsive" >
                <table class="table table-sm table-bordered" id="dataTable" width="100%" cellspacing="0" >
                  <thead>
                    <tr>
                      <?php for($i=0;$i<count($header);$i++){ ?>
                      <th><?= $header[$i]; ?></th>
                    <?php }?>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <?php for($i=0;$i<count($header);$i++){ ?>
                      <th><?= $header[$i]; ?></th>
                    <?php }?>
                    </tr>
                  </tfoot>
                  <tbody> <!-- Tabel Admin-->
                      <?php if($profile['id_jabatan']==0) {?>
                    <?php $count=1; foreach($admin_data as $data): ?>
                    <tr>
                      <td><?= $count; ?></td>
                      <td><?= $data['nama']; ?></td>
                      <td><?= $data['email']; ?></td>
                      <td><?= $data['telp']; ?></td>
                      <td><?= $data['jabatan']; ?></td>
                        <td><a href="<?php echo base_url();?>admin/Table_admin/editadmin/<?= $data['id_user'] ?>"class="badge badge-info">Edit</a>
                            <a href="<?php echo base_url();?>admin/Table_admin/deleteadmin/<?= $data['id_user'] ?>" class="badge badge-danger" onclick="return confirm('Are You Sure?')">Delete</a></td>
                    </tr>
                  <?php $count++; endforeach;?> <!-- Tabel Proker-->
                <?php }else if($akses['m_proker'] == 1 || $akses['m_prokermanage'] ==1){
                  if($akses['m_prokermanage'] ==1){ $proker = $Allproker; } $edit =""; $delete=""; ?>
                  <?php foreach($proker as $data): ?>
                  <tr>
                    <td><?= $data['id_proker']; ?></td>
                    <td><?= $data['nama_proker']; ?></td>
                    <td><?= $data['keterangan']; ?></td>
                    <td><?= $data['status']; ?></td>
                    <td><?= $data['biaya']; ?></td>
                    <td><?= $data['tgl_mulai']; ?></td>
                    <td><?= $data['tgl_pelaksanaan']; ?></td>
                    <td><?= $data['id_user']; ?></td>
                    <?php if($akses['m_prokermanage'] ==1){?>
                      <?php $edit = "ketua/updatestatus/".$data['id_proker']; $delete ="ketua/deletestatus/".$data['id_proker']; ?>
                    <?php }else{ ?>
                      <?php $edit = "proker/updateproker/".$data['id_proker']; $delete ="proker/deleteproker/".$data['id_proker']; ?>
                      <?php }?>
                    <td><a href="<?php echo base_url();?>admin/<?= $edit; ?>"class="badge badge-info">Edit</a>
                    <a href="<?php echo base_url();?>admin/<?= $delete; ?>" class="badge badge-danger" onclick="return confirm('Are You Sure?')">Delete</a></td>
                  </tr>
                <?php endforeach;?> <!-- Tabel sekertaris-->
              <?php }?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
