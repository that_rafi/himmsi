<div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-5 d-none d-lg-block "style="background-image: url('<?php echo base_url()?>upload/randompic/<?= $pic; ?>')"></div>
          <div class="col-lg-7">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4"><?= $titleproker ?></h1>
              </div>
                <?= $this->session->flashdata('message'); ?>
                <?= $this->session->flashdata('error'); ?>
              <form class="user" method="post" action="<?php echo base_url();?>admin/<?= $path ?>" enctype="multipart/form-data">
                <div class="form-group">
                    <input type="text" name="nama" class="form-control form-control-user" id="exampleFirstName" placeholder="Proker Name" value="<?= $edit['nama_proker']; ?>" >
                    <?= form_error('nama','<small class="text-danger pl-3">','</small>'); ?>
                </div>
                <div class="form-group">
                  <textarea class="form-control form-control-user" name="ket" placeholder="Detail"><?= $edit['keterangan']; ?></textarea>
                  <?= form_error('ket','<small class="text-danger pl-3">','</small>'); ?>
                </div>
                <div class="form-group">
                  <input type="number" name="price" class="form-control form-control-user"  placeholder="Rp " value="<?= $edit['biaya']; ?>">
                  <?= form_error('price','<small class="text-danger pl-3">','</small>'); ?>
                </div>
                <div class="form-group">
                  <input type="date" name="date1" class="form-control form-control-user"  placeholder="Start Date" value="<?= $edit['tgl_mulai']; ?>">
                  <?= form_error('date1','<small class="text-danger pl-3">','</small>'); ?>
                </div>
                <div class="form-group">
                  <input type="date" name="date2" class="form-control form-control-user"  placeholder="Implementation Date" value="<?= $edit['tgl_pelaksanaan']; ?>" >
                  <?= form_error('date2','<small class="text-danger pl-3">','</small>'); ?>
                </div>
                <input type="submit" value="<?= $btn; ?>" class="btn btn-primary btn-user btn-block"></input>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
