<div class="container">

  <!-- Outer Row -->
  <div class="row justify-content-center">

    <div class="col-xl-10 col-lg-12 col-md-9">

      <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
          <!-- Nested Row within Card Body -->
          <div class="row">
            <div class="col-lg-6 d-none d-lg-block " style="background-image: url('<?php echo base_url()?>upload/randompic/cats_white.jpg')"></div>
            <div class="col-lg-6">
              <div class="p-5">
                <div class="text-center">
                  <h1 class="h4 text-gray-900 mb-2"><?= $title; ?></h1>
                  <p class="mb-4"></p>
                </div>
                  <?= $this->session->flashdata('message'); ?>
                <form class="user" method="post" action="<?php echo base_url();?>admin/<?= $path; ?><?= $result['id_aspirasi']; ?>">
                  <div class="form-group">
                    <textarea class="form-control form-control-user" name="jwb" placeholder="Tulis Jawaban disini..." ><?php echo set_value('jawaban'); ?></textarea>
                  </div>
                    <?= form_error('jwb','<small class="text-danger pl-3">','</small>'); ?>
                  <input type="submit" value="Change Response" name="submit" class="btn btn-primary btn-user btn-block"></input>
                </form>
                <hr>
                <div class="text-center">
                  <a class="small" href="<?php echo base_url();?>admin/<?= $home;?>">Back to Home</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>

  </div>

</div>
