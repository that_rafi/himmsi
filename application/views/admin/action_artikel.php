<div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-5 d-none d-lg-block "style="background-image: url('<?php echo base_url()?>upload/randompic/<?= $pic; ?>')"></div>
          <div class="col-lg-7">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4"><?= $titleartikel ?></h1>
              </div>
                <?= $this->session->flashdata('message'); ?>
                <?= $this->session->flashdata('error'); ?>
              <form class="user" method="post" action="<?php echo base_url();?>admin/<?= $path ?>" enctype="multipart/form-data">
                <div class="form-group">
                    <input type="text" name="judul" class="form-control form-control-user" id="exampleFirstName" placeholder="Judul" value="<?= $edit['judul']; ?>" >
                    <?= form_error('judul','<small class="text-danger pl-3">','</small>'); ?>
                </div>
                <div class="form-group">
                    <input type="text" name="tema" class="form-control form-control-user" id="exampleFirstName" placeholder="Tema" value="<?= $edit['tema']; ?>" >
                    <?= form_error('tema','<small class="text-danger pl-3">','</small>'); ?>
                </div>
                <select class="custom-select mb-3" name="cat">
                    <option value="0" selected="selected" >Kategori</option>
                  <?php foreach($kategori as $kat) :
                    if($kat['kategori']==$edit['kategori']){?>
                      <option value="<?= $kat['id_kategori']; ?>" selected="selected"><?=$kat['kategori']; ?></option>
                  <?php }else{?>
                    <option value="<?= $kat['id_kategori']; ?>" ><?=$kat['kategori']; ?></option>
                  <?php }endforeach;?>
                </select>
                <?= form_error('cat','<small class="text-danger pl-3">','</small>'); ?>
                <div class="form-group">
                  <input type="date" name="date1" class="form-control form-control-user"  placeholder="Tanggal" value="<?= $edit['tanggal']; ?>">
                  <?= form_error('date1','<small class="text-danger pl-3">','</small>'); ?>
                </div>
                <div class="form-group">
                  <textarea class="form-control form-control-user" name="isi" placeholder="Isi"><?= $edit['isi']; ?></textarea>
                  <?= form_error('isi','<small class="text-danger pl-3">','</small>'); ?>
                </div>
                <div class="row mb-3">
                  <div class="col-lg-6" ><div class="text ml-2">Photo Artikel</div></div>
                  <div class="col-lg-6">
                    <div class="custom-file">
                      <input type="file" name="foto" class="custom-file-input" id="customFile" accept="image/*">
                        <input type="hidden" name="old_foto" class="custom-file-input" id="customFile" accept="image/*" value="<?= $edit['gambar']; ?>">
                      <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                  </div>
                </div>
                <?= form_error('foto','<small class="text-danger pl-3">','</small>'); ?>
                <select class="custom-select mb-3" name="tipe">
                    <option value="0" selected="selected" >Tipe</option>
                  <?php for($i=0;$i<count($tipe);$i++){
                    if($tipe[$i]==$edit['tipe']){?>
                      <option value="<?= $tipe[$i]; ?>" selected="selected"><?=$tipe[$i]; ?></option>
                  <?php }else{?>
                    <option value="<?= $tipe[$i]; ?>" ><?=$tipe[$i]; ?></option>
                  <?php }}?>
                </select>
                <?= form_error('tipe','<small class="text-danger pl-3">','</small>'); ?>
                <input type="submit" value="<?= $btn; ?>" class="btn btn-primary btn-user btn-block"></input>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
