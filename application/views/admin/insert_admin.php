<div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-5 d-none d-lg-block bg-register-image"></div>
          <div class="col-lg-7">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Add Admin</h1>
              </div>
                <?= $this->session->flashdata('message'); ?>
                <?= $this->session->flashdata('error'); ?>
              <form class="user" method="post" action="insertadmin" enctype="multipart/form-data">
                <div class="form-group">
                    <input type="text" name="nama" class="form-control form-control-user" id="exampleFirstName" placeholder="Full Name">
                    <?= form_error('nama','<small class="text-danger pl-3">','</small>'); ?>
                </div>
                <div class="form-group">
                  <input type="email" name="email" class="form-control form-control-user" id="exampleInputEmail" placeholder="Email Address">
                  <?= form_error('email','<small class="text-danger pl-3">','</small>'); ?>
                </div>
                <div class="form-group">
                  <input type="tel" name="telp" class="form-control form-control-user"  placeholder="Phone number">
                  <?= form_error('telp','<small class="text-danger pl-3">','</small>'); ?>
                </div>
                <div class="form-group row">
                  <div class="col-sm-6">
                    <div class="row">
                    <div class="col-sm-6 ">  <div class="text ml-2" >Position</div></div>
                    <div class="col-sm-6">
                      <select class="custom-select" name="jabatan">
                        <option value="0" selected="selected">Position</option>
                        <?php foreach ($jabatan as $pos) : ?>
                        <option value="<?= $pos['id_jabatan']; ?>"><?=$pos['jabatan']; ?></option>
                      <?php endforeach; ?>
                      </select>
                    </div>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="row">
                    <div class="col-sm-6">  <div class="text ml-2">Year</div></div>
                    <div class="col-sm-6">
                      <select class="custom-select" name="year">
                        <option value="0" selected="selected">Year</option>
                        <?php foreach ($tahun as $thn) : ?>
                        <option value="<?= $thn['id_tahun']; ?>"><?=$thn['tahun']; ?></option>
                      <?php endforeach; ?>
                      </select>
                    </div>
                    </div>
                </div>
                </div>
                <?= form_error('jabatan','<small class="text-danger pl-3">','</small>'); ?>
                <?= form_error('year','<small class="text-danger pl-3">','</small>'); ?>
                <div class="row mb-3">
                  <div class="col-lg-6" ><div class="text ml-2">Photo Profile</div></div>
                  <div class="col-lg-6">
                    <div class="custom-file">
                      <input type="file" name="foto" class="custom-file-input" id="customFile" accept="image/*">
                      <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                  </div>
                </div>
                <?= form_error('foto','<small class="text-danger pl-3">','</small>'); ?>
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="password" name="pass1" class="form-control form-control-user" id="exampleInputPassword" placeholder="Password">
                  </div>
                  <div class="col-sm-6">
                    <input type="password" name="pass2" class="form-control form-control-user" id="exampleRepeatPassword" placeholder="Repeat Password">
                  </div>
                </div>
                <?= form_error('pass1','<small class="text-danger pl-3">','</small>'); ?>
                <?= form_error('pass2','<small class="text-danger pl-3">','</small>'); ?>
                <input type="submit" value="Add Admin" class="btn btn-primary btn-user btn-block"></input>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
