<div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-5 d-none d-lg-block "style="background-image: url('<?php echo base_url()?>upload/randompic/<?= $pic; ?>')"></div>
          <div class="col-lg-7">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4"><?= $titlecontent ?></h1>
              </div>
                <?= $this->session->flashdata('message'); ?>
                <?= $this->session->flashdata('error'); ?>
              <form class="user" method="post" action="<?php echo base_url();?>admin/<?= $path ?>" enctype="multipart/form-data">
                <?php if($flag){$name="nama"; $value = $edit['nama'];}else{$name = "judul"; $value= $edit['judul'];} ?>
                <div class="form-group">
                    <input type="text" name="<?= $name ?>" class="form-control form-control-user" id="exampleFirstName" placeholder="<?=$name?>" value="<?= $value; ?>" >
                    <?= form_error("$name",'<small class="text-danger pl-3">','</small>'); ?>
                </div>
                <?php if($flag){ ?>
                <div class="form-group">
                    <input type="url" name="link" class="form-control form-control-user" id="exampleFirstName" placeholder="Link Banner" value="<?= $edit['link']; ?>" >
                    <?= form_error('link','<small class="text-danger pl-3">','</small>'); ?>
                </div>
              <?php }?>
                <div class="form-group">
                  <textarea class="form-control form-control-user" name="ket" placeholder="Keterangan"><?= $edit['keterangan']; ?></textarea>
                  <?= form_error('ket','<small class="text-danger pl-3">','</small>'); ?>
                </div>
                <div class="row mb-3">
                  <div class="col-lg-6" ><div class="text ml-2">Photo <?=$class;?></div></div>
                  <div class="col-lg-6">
                    <div class="custom-file">
                      <input type="file" name="foto" class="custom-file-input" id="customFile" accept="image/*">
                        <input type="hidden" name="old_foto" class="custom-file-input" id="customFile" accept="image/*" value="<?= $edit['gambar']; ?>">
                      <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                  </div>
                </div>
                <?= form_error('foto','<small class="text-danger pl-3">','</small>'); ?>
                <?php if($flag){ ?>
                <select class="custom-select mb-3" name="tipe">
                    <option value="0" selected="selected" >Tipe</option>
                  <?php for($i=0;$i<count($tipe);$i++){
                    if($tipe[$i]==$edit['tipe']){?>
                      <option value="<?= $tipe[$i]; ?>" selected="selected"><?=$tipe[$i]; ?></option>
                  <?php }else{?>
                    <option value="<?= $tipe[$i]; ?>" ><?=$tipe[$i]; ?></option>
                  <?php }}?>
                </select>
                <?= form_error('tipe','<small class="text-danger pl-3">','</small>'); ?>
              <?php }?>
                <input type="submit" value="<?= $btn; ?>" class="btn btn-primary btn-user btn-block"></input>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
