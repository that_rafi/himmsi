<div class="container">
  <?php if($valid){?>
      <?php if($countquery['jml_sekertaris']== null ||$countquery['jml_staff']== null){
        $countquery['jml_sekertaris'] = 0;
        $countquery['jml_staff'] = 0;
      }
      if(($currentcountsk< (int)$countquery['jml_sekertaris'] || (int)$countquery['jml_sekertaris'] == 0) && ($currentcountst< (int)$countquery['jml_staff'] || (int)$countquery['jml_staff'] == 0)  ) //  jika jml skrtrs dan staff msih dibwh max
      { $posisi =  array("sekertaris","staff");}
      else if(($currentcountsk>  (int)$countquery['jml_sekertaris'])|| ($currentcountst< (int)$countquery['jml_staff']) || ((int)$countquery['jml_sekertaris'] == 0) ) //  jika jml skrtrs diatas max dan staff msih dibwh max
      {$posisi =  array("staff");}
      else if($currentcountst>  (int)$countquery['jml_staff'] || $currentcountsk< (int)$countquery['jml_sekertaris'] || (int)$countquery['jml_sekertaris'] == 0 ) //  jika jml skrtrs diatas max dan staff msih dibwh max
      {$posisi =  array("sekertaris");}
      else{
       $posisi = null;
      }
    }?> <!-- Kalau staff -->

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
            <?php if( $edit['foto']== null || $edit['foto'] ==""){?>
                <div class="col-lg-5 d-none d-lg-block "style="background-image: url('<?php echo base_url()?>upload/randompic/<?= $pic; ?>')"></div>
                <?php }else{?>
                <div class="col-lg-5 d-none d-lg-block "style="background-image: url('<?php echo base_url()?>upload/adminpic/<?= $edit['foto']; ?>');background-size:cover"></div>
                <?php }?>
          <div class="col-lg-7">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4"><?= $title; ?></h1>
              </div>
                <?= $this->session->flashdata('message'); ?>
                <?= $this->session->flashdata('error'); ?>
              <form class="user" method="post" action="<?php echo base_url();?>admin/<?= $path ?>" enctype="multipart/form-data">
                <div class="form-group">
                    <input type="text" name="nama" class="form-control form-control-user" id="exampleFirstName" placeholder="Nama Anggota" value="<?= $edit['nama']; ?>" >
                    <?= form_error('nama','<small class="text-danger pl-3">','</small>'); ?>
                </div>
                <div class="form-group">
                    <input type="text" name="nik" class="form-control form-control-user" id="exampleFirstName" placeholder="NIK Anggota" value="<?= $edit['NIK']; ?>" >
                    <?= form_error('nik','<small class="text-danger pl-3">','</small>'); ?>
                </div>
                <select class="custom-select mb-3" name="posisi">
                    <option value="0" selected="selected" >Posisi</option>
                  <?php for($i=0;$i<count($posisi);$i++){
                    if($posisi[$i]==$edit['posisi']){?>
                      <option value="<?= $posisi[$i]; ?>" selected="selected"><?=$posisi[$i]; ?></option>
                  <?php }else{?>
                    <option value="<?= $posisi[$i]; ?>" ><?=$posisi[$i]; ?></option>
                  <?php }}?>
                </select>
                <?= form_error('posisi','<small class="text-danger pl-3">','</small>'); ?>
                <div class="row mb-3">
                  <div class="col-lg-6" ><div class="text ml-2">Photo Anggota</div></div>
                  <div class="col-lg-6">
                    <div class="custom-file">
                      <input type="file" name="foto" class="custom-file-input" id="customFile" accept="image/*">
                        <input type="hidden" name="old_foto" class="custom-file-input" id="customFile" accept="image/*" value="<?= $edit['foto']; ?>">
                      <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                  </div>
                </div>
                <?= form_error('foto','<small class="text-danger pl-3">','</small>'); ?>
                <select class="custom-select mb-3" name="status">
                    <option value="0" selected="selected" >Status</option>
                  <?php for($i=0;$i<count($status);$i++){
                    if($status[$i]==$edit['status']){?>
                      <option value="<?= $status[$i]; ?>" selected="selected"><?=$status[$i]; ?></option>
                  <?php }else{?>
                    <option value="<?= $status[$i]; ?>" ><?=$status[$i]; ?></option>
                  <?php }}?>
                </select>
                <?= form_error('status','<small class="text-danger pl-3">','</small>'); ?>
                <input type="submit" value="<?= $btn; ?>" class="btn btn-primary btn-user btn-block"></input>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
