<!-- Begin Page Content -->
<div class="container" style="margin-bottom:60px">
<h1 class="h3 mb-2 text-gray-800">Data <?= $tabletitle ?></h1>
<div class="btn-group dropright">
<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
  Posisi
</button>
<div class="dropdown-menu">
  <!-- Dropdown menu links -->
  <a class="dropdown-item" href="<?= base_url(); ?>admin/Pengurus/anggota/all">All</a>
  <a class="dropdown-item" href="<?= base_url(); ?>admin/Pengurus/anggota/sekretaris">Sekertaris</a>
  <a class="dropdown-item" href="<?= base_url(); ?>admin/Pengurus/anggota/staff">Staff</a>
</div>
</div>
</div>
        <div class="container-fluid">

          <!-- Page Heading -->


          <!-- DataTales Example -->

          <div class="card shadow mb-4">
            <div class="card-header py-3">
            <div class="container" style="display: flex;align-items: flex-end;justify-content: flex-end;">
            <form action="<?= base_url(); ?>admin/Pengurus/print_data/<?= $pos?>" method="post">
            <button class="btn success" style="margin:0px;padding:0px">  <i class="fa fa-print" style="text-align:start" aria-hidden="true"></i></button>
            </form>
            </div>
              <?php if($flagcount){ ?> <!-- Kalau sekertaris / staff -->
                <?php if($flagjml){?> <!-- Kalau sekertaris -->
                  <?php if($currentcount< $countquery['jml_sekertaris']){?>
                  <h6 class="m-0 font-weight-bold text-primary" ><a href="<?php echo base_url();?>admin/Pengurus/inputanggota/">Insert New anggota</a></h6>
                  <?php }?>
                <?php }else{?> <!-- Kalau staff -->
                  <?php if($currentcount< $countquery['jml_staff']){?>
                  <h6 class="m-0 font-weight-bold text-primary" ><a href="<?php echo base_url();?>admin/Pengurus/inputanggota">Insert New anggota</a></h6>
                  <?php }?>
                <?php }?>
          <?php }else{?>
                <?php if($currentcount< $countall){?> <!-- kalau all -->
                <h6 class="m-0 font-weight-bold text-primary" ><a href="<?php echo base_url();?>admin/Pengurus/inputanggota">Insert New anggota</a></h6>
                <?php }?>
          <?php }?>
            </div>
            <div class="card-body">
              <?= $this->session->flashdata('message'); ?>
              <div class="table-responsive">
                <table class="table table-sm table-bordered" id="dataTable" width="100%" cellspacing="0" >
                  <thead>
                    <tr>
                      <?php for($i=0;$i<count($header);$i++){ ?>
                      <th><?= $header[$i]; ?></th>
                    <?php }?>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <?php for($i=0;$i<count($header);$i++){ ?>
                      <th><?= $header[$i]; ?></th>
                    <?php }?>
                    </tr>
                  </tfoot>
                  <tbody> <!-- Tabel Relasi-->
                    <?php if($profile['id_jabatan']!=0 || $profile['id_jabatan']!=1 || $profile['id_jabatan']!=2 || $profile['id_jabatan']!=3  ){ ?><!-- Tabel Relasi-->
              <?php foreach($anggota as $data): ?>
              <tr>
                <td><?= $data['id_anggota']; ?></td>
                <td><?= $data['nama']; ?></td>
                <td><?= $data['NIK']; ?></td>
                <td><?= $data['posisi']; ?></td>
                <td><?= $data['foto']; ?></td>
                <td><?= $data['status']; ?></td>
                <td>
                    <a href="<?php echo base_url();?>admin/Pengurus/editanggota/<?= $data['id_anggota']; ?>" class="badge badge-info" >Edit</a>
                    <a href="<?php echo base_url();?>admin/Pengurus/deleteanggota/<?= $data['id_anggota']; ?>" class="badge badge-danger" onclick="return confirm('Are You Sure?')">Delete</a>
                  </td>
              </tr>
            <?php endforeach;?>
            <?php }?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
