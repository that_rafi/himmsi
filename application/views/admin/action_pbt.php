<div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-5 d-none d-lg-block "style="background-image: url('<?php echo base_url()?>upload/randompic/<?= $pic; ?>')"></div>
          <div class="col-lg-7">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4"><?= $titlepbt ?></h1>
              </div>
                <?= $this->session->flashdata('message'); ?>
                <?= $this->session->flashdata('error'); ?>
              <form class="user" method="post" action="<?php echo base_url();?>admin/<?= $path ?>" enctype="multipart/form-data">
                <?php if($flag){ ?> <!-- if the system display materi input -->
                  <div class="form-group">
                    <input type="text" name="matakuliah" class="form-control form-control-user" id="exampleFirstName" placeholder="Matakuliah" value="<?= $edit['matakuliah'] ?>" >
                    <?= form_error("matakuliah",'<small class="text-danger pl-3">','</small>'); ?>
                  </div>
                  <div class="form-group">
                        <input type="url" name="video" class="form-control form-control-user" id="exampleFirstName" placeholder="Link Banner" value="<?= $edit['video']; ?>" >
                        <?= form_error('video','<small class="text-danger pl-3">','</small>'); ?>
                  </div>
                  <select class="custom-select mb-3" name="ket">
                      <option value="0" selected="selected" >Keterangan</option>
                    <?php for($i=0;$i<count($keterangan);$i++){
                      if($keterangan[$i]==$edit['keterangan']){?>
                        <option value="<?= $keterangan[$i]; ?>" selected="selected"><?=$keterangan[$i]; ?></option>
                    <?php }else{?>
                      <option value="<?= $keterangan[$i]; ?>" ><?=$keterangan[$i]; ?></option>
                    <?php }}?>
                  </select>
                  <?= form_error('ket','<small class="text-danger pl-3">','</small>'); ?>
                  <select class="custom-select mb-3" name="mhs_semester">
                      <option value="0" selected="selected" >Semester</option>
                    <?php for($i=0;$i<count($semester);$i++){
                      if($semester[$i]==$edit['mhs_semester']){?>
                        <option value="<?= $semester[$i]; ?>" selected="selected"><?=$semester[$i]; ?></option>
                    <?php }else{?>
                      <option value="<?= $semester[$i]; ?>" ><?=$semester[$i]; ?></option>
                    <?php }}?>
                  </select>
                  <?= form_error('mhs_semester','<small class="text-danger pl-3">','</small>'); ?>
                  <select class="custom-select mb-3" name="jurusan">
                      <option value="0" selected="selected" >Jurusan</option>
                    <?php foreach($jurusan as $mjr ):
                      $j = ($mjr['jurusan'] == "S1 - SI") ? "Sistem Informasi" : "Manajemen Informatika";
                      if($j==$edit['jurusan'] ){?>
                        <option value="<?= $j; ?>" selected="selected"><?= $j; ?></option>
                    <?php }else{?>
                      <option value="<?= $j; ?>" ><?= $j; ?></option>
                    <?php  }endforeach; ?>
                  </select>
                  <?= form_error('jurusan','<small class="text-danger pl-3">','</small>'); ?>
                <?php }else{?> <!-- if the system display peserta input -->
                  <div class="form-group">
                    <input type="text" name="nama" class="form-control form-control-user" id="exampleFirstName" placeholder="Nama Mahasiswa" value="<?= $edit['nama'] ?>" >
                    <?= form_error("nama",'<small class="text-danger pl-3">','</small>'); ?>
                  </div>
                  <div class="form-group">
                    <input type="text" name="nim" class="form-control form-control-user" id="exampleFirstName" placeholder="Nim Mahasiswa" value="<?= $edit['nim'] ?>" >
                    <?= form_error("nim",'<small class="text-danger pl-3">','</small>'); ?>
                  </div>
                  <div class="form-group">
                    <input type="text" name="nohp" class="form-control form-control-user" id="exampleFirstName" placeholder="No Hp Mahasiswa" value="<?= $edit['no_hp'] ?>" >
                    <?= form_error("nohp",'<small class="text-danger pl-3">','</small>'); ?>
                  </div>
                  <select class="custom-select mb-3" name="jurusan">
                      <option value="0" selected="selected" >Jurusan</option>
                    <?php foreach($jurusan as $mjr ):
                      if($mjr['jurusan']==$edit['jurusan']){?>
                        <option value="<?= $mjr['jurusan']; ?>" selected="selected"><?= $mjr['jurusan']; ?></option>
                    <?php }else{?>
                      <option value="<?= $mjr['jurusan']; ?>" ><?= $mjr['jurusan']; ?></option>
                    <?php  }endforeach; ?>
                  </select>
                  <?= form_error('jurusan','<small class="text-danger pl-3">','</small>'); ?>
                <?php }?>
              <div class="form-group">
                <input type="date" name="date1" class="form-control form-control-user"  placeholder="Tanggal" value="<?= $edit['tanggal']; ?>">
                <?= form_error('date1','<small class="text-danger pl-3">','</small>'); ?>
              </div>
                <input type="submit" value="<?= $btn; ?>" class="btn btn-primary btn-user btn-block"></input>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
