<div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-5 d-none d-lg-block "style="background-image: url('<?php echo base_url()?>upload/randompic/<?= $pic; ?>')"></div>
          <div class="col-lg-7">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Input Departemen</h1>
              </div>
                <?= $this->session->flashdata('message'); ?>
                <?= $this->session->flashdata('error'); ?>
              <form class="user" method="post" action="<?php echo base_url();?>admin/<?= $path ?>" enctype="multipart/form-data">
                <div class="form-group">
                    <input type="text" name="jabatan" class="form-control form-control-user" id="exampleFirstName" placeholder="Nama Jabatan" value="<?= $edit['jabatan']; ?>" >
                    <?= form_error('jabatan','<small class="text-danger pl-3">','</small>'); ?>
                </div>
                <div class="form-group">
                  <input type="number" name="jmlsk" class="form-control form-control-user"  placeholder="Jumlah Sekertaris " value="<?= $edit['jml_sekertaris']; ?>">
                  <?= form_error('jmlsk','<small class="text-danger pl-3">','</small>'); ?>
                </div>
                <div class="form-group">
                  <input type="number" name="jmlst" class="form-control form-control-user"  placeholder="Jumlah Staff " value="<?= $edit['jml_staff']; ?>">
                  <?= form_error('jmlst','<small class="text-danger pl-3">','</small>'); ?>
                </div>
                <div class="form-group">
                  <textarea name="ket" class="form-control form-control-user" placeholder="Keterangan"><?= $edit['keterangan']; ?></textarea>
                  <?= form_error('ket','<small class="text-danger pl-3">','</small>'); ?>
                </div>
                <input type="submit" value="<?= $btn; ?>" class="btn btn-primary btn-user btn-block"></input>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
