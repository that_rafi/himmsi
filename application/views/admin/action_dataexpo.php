<div class="container">

  <!-- Outer Row -->
  <div class="row justify-content-center">

    <div class="col-xl-10 col-lg-12 col-md-9">

      <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
          <!-- Nested Row within Card Body -->
          <div class="row">
            <div class="col-lg-12">
              <div class="p-5">
                <div class="text-center">
                  <h1 class="h4 text-gray-900 mb-2"><?= $title; ?></h1>
                  <p class="mb-4"></p>
                </div>
                  <?= $this->session->flashdata('message'); ?>
                <form class="user" method="post" action="<?php echo base_url();?>admin/<?= $path; ?>">
                  <div class="form-group">
                    <input type="number" class="form-control form-control-user" name="bayar" placeholder="Rp." ><?php echo set_value('bayar'); ?></input>
                  </div>
                    <?= form_error('bayar','<small class="text-danger pl-3">','</small>'); ?>
                    <select class="custom-select mb-3" name="status">
                        <option value="0" selected="selected" >Status</option>
                        <option value="lunas">Lunas</option>
                        <option value="belum_lunas">Belum Lunas</option>
                    </select>
                      <?= form_error('status','<small class="text-danger pl-3">','</small>'); ?>
                  <input type="submit" value="Change Status" name="submit" class="btn btn-primary btn-user btn-block"></input>
                </form>
                <hr>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>

  </div>

</div>
