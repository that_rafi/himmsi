<!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800"> <?= $tabletitle ?> PBT</h1>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary" ><a href="<?php echo base_url();?>admin/Pbt/inputpbt/<?= strtolower($tabletitle); ?>">Tambah <?= $tabletitle ?></a></h6>
            </div>
            <div class="card-body">
              <?= $this->session->flashdata('message'); ?>
              <div class="table-responsive" >
                <table class="table table-sm table-bordered" id="dataTable" width="100%" cellspacing="0" >
                  <thead>
                    <tr>
                      <?php for($i=0;$i<count($header);$i++){ ?>
                      <th><?= $header[$i]; ?></th>
                    <?php }?>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <?php for($i=0;$i<count($header);$i++){ ?>
                      <th><?= $header[$i]; ?></th>
                    <?php }?>
                    </tr>
                  </tfoot>
                  <tbody> <!-- Tabel Relasi-->
                    <?php if($akses['m_pbtmateri'] ){?><!-- Tabel Artikel-->
                  <?php if($flag){   ?>
              <?php $count=1; foreach($pbt as $data): ?>
              <tr>
                <td><?= $count; ?></td>
                <td><?= $data['matakuliah']; ?></td>
                <td><?= $data['jurusan']; ?></td>
                <td><?= $data['mhs_semester']; ?></td>
                <td><?= $data['keterangan']; ?></td>
                <td><?= $data['tanggal']; ?></td>
                <td><iframe src="<?= $data['video']; ?>/preview" width="64" height="48"></iframe></td>
                <td><?php if($iduser == 4){?>
                      <a href="<?php echo base_url();?>admin/Pbt/editpbt/materi/<?= $data['id_pbt']; ?>"class="badge badge-info">Edit</a>
                  <?php }?>
                      <a href="<?php echo base_url();?>admin/Pbt/deletepbt/materi/<?= $data['id_pbt']; ?>" class="badge badge-danger" onclick="return confirm('Are You Sure?')">Delete</a></td>
              </tr>
            <?php $count++; endforeach;?>
          <?php }else{?>
            <?php $count=1; foreach($pbt as $data): ?>
            <tr>
              <td><?= $count; ?></td>
              <td><?= $data['nama']; ?></td>
              <td><?= $data['nim']; ?></td>
              <td><?= $data['jurusan']; ?></td>
              <td><?= $data['no_hp']; ?></td>
              <td><?= $data['tanggal']; ?></td>
              <td><?php if($iduser == 4){?>
                    <a href="<?php echo base_url();?>admin/Pbt/editpbt/peserta/<?= $data['id_mhs']; ?>"class="badge badge-info">Edit</a>
                <?php }?>
                    <a href="<?php echo base_url();?>admin/Pbt/deletepbt/peserta/<?= $data['id_mhs']; ?>" class="badge badge-danger" onclick="return confirm('Are You Sure?')">Delete</a></td>
            </tr>
          <?php $count++; endforeach;}}?>
                  </tbody>
                </table>
                <?= $this->pagination->create_links(); ?>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
