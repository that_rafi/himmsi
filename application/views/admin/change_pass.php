<div class="container">

  <!-- Outer Row -->
  <div class="row justify-content-center">

    <div class="col-xl-10 col-lg-12 col-md-9">

      <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
          <!-- Nested Row within Card Body -->
          <div class="row">
            <div class="col-lg-6 d-none d-lg-block bg-password-image"></div>
            <div class="col-lg-6">
              <div class="p-5">
                <div class="text-center">
                  <h1 class="h4 text-gray-900 mb-2">Change Your Password?</h1>
                  <p class="mb-4"></p>
                </div>
                  <?= $this->session->flashdata('message'); ?>
                <form class="user" method="post" action="<?php echo base_url();?>admin/Table_admin/changepass/<?= $id;?>">
                  <div class="form-group">
                    <input type="password" name="old_pass" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Old Password...">
                  </div>
                    <?= form_error('old_pass','<small class="text-danger pl-3">','</small>'); ?>
                  <div class="form-group row">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                      <input type="password" name="pass1" class="form-control form-control-user" id="exampleInputPassword" placeholder="New Password">
                    </div>
                    <div class="col-sm-6">
                      <input type="password" name="pass2" class="form-control form-control-user" id="exampleRepeatPassword" placeholder="Repeat New Password">
                    </div>
                      <?= form_error('pass1','<small class="text-danger pl-3">','</small>'); ?>
                        <?= form_error('pass2','<small class="text-danger pl-3">','</small>'); ?>
                  </div>



                  <input type="submit" value="Change Password" name="submit" class="btn btn-primary btn-user btn-block"></input>
                </form>
                <hr>
                <div class="text-center">
                  <a class="small" href="<?php echo base_url();?>admin/Table_admin/">Back to Home</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>

  </div>

</div>
