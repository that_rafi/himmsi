<!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Data <?= $tabletitle ?></h1>
          <p class="mb-4">Description.</p>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary" ><a href="<?php echo base_url();?>admin/Relasi/insertrelasi">Insert New Relasi</a></h6>
            </div>
            <div class="card-body">
              <?= $this->session->flashdata('message'); ?>
              <div class="table-responsive" >
                <table class="table table-sm table-bordered" id="dataTable" width="100%" cellspacing="0" >
                  <thead>
                    <tr>
                      <?php for($i=0;$i<count($header);$i++){ ?>
                      <th><?= $header[$i]; ?></th>
                    <?php }?>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <?php for($i=0;$i<count($header);$i++){ ?>
                      <th><?= $header[$i]; ?></th>
                    <?php }?>
                    </tr>
                  </tfoot>
                  <tbody> <!-- Tabel Relasi-->
                    <?php if($akses['m_relasi'] ==1 ){ $datarelasi = $relasi;?><!-- Tabel Relasi-->
              <?php foreach($datarelasi as $data): ?>
              <tr>
                <td><?= $data['id_relasi']; ?></td>
                <td><?= $data['nama_relasi']; ?></td>
                <td><?= $data['organisasi']; ?></td>
                <td><?= $data['no_hp']; ?></td>
                <td><?= $data['email']; ?></td>
                <td><?= $data['alamat']; ?></td>
                <td><?= $data['jurusan']; ?></td>
                <td><?= $data['kampus']; ?></td>
                <td><?= $data['keterangan']; ?></td>
                <td><?= $data['jabatan']; ?></td>
                <?php if($data['id_user'] == $iduser){ ?>
                <td><a href="<?php echo base_url();?>admin/Relasi/editrelasi/<?= $data['id_relasi']; ?>"class="badge badge-info">Edit</a>
                <a href="<?php echo base_url();?>admin/Relasi/deleterelasi/<?= $data['id_relasi']; ?>" class="badge badge-danger" onclick="return confirm('Are You Sure?')">Delete</a></td>
              <?php }else{?>
                <td></td>
              <?php }?>
              </tr>
            <?php endforeach;?>
            <?php }?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
