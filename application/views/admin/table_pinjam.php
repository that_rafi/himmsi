<!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Data <?= $tabletitle ?></h1>
          <p class="mb-4">Description.</p>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary" ><a href="<?php echo base_url();?>admin/Barang/inputpinjaman">Insert New Pinjaman</a></h6>
            </div>
            <div class="card-body">
              <?= $this->session->flashdata('message'); ?>
              <div class="table-responsive" >
                <table class="table table-sm table-bordered" id="dataTable" width="100%" cellspacing="0" >
                  <thead>
                    <tr>
                      <?php for($i=0;$i<count($header);$i++){ ?>
                      <th><?= $header[$i]; ?></th>
                    <?php }?>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <?php for($i=0;$i<count($header);$i++){ ?>
                      <th><?= $header[$i]; ?></th>
                    <?php }?>
                    </tr>
                  </tfoot>
                  <tbody> <!-- Tabel Relasi-->
                    <?php if($akses['m_pinjambarang'] ==1){ ?><!-- Tabel Relasi-->
              <?php foreach($pinjaman as $data): ?>
              <tr>
                <td><?= $data['id_pinjam']; ?></td>
                <td><?= $data['nama']; ?></td>
                <td><?= $data['nama_peminjam']; ?></td>
                <td><?= $data['tgl_pinjam']; ?></td>
                <td><?= $data['tgl_kembali']; ?></td>
                <td><?= $data['keadaan_pinjam']; ?></td>
                <td><?= $data['keadaan_kembali']; ?></td>
                <td><?= $data['jml_pinjam']; ?></td>
                <td><?= $data['jml_kembali']; ?></td>
                <td>
                    <a href="<?php echo base_url();?>admin/Barang/deletepinjaman/<?= $data['id_pinjam']; ?>" class="badge badge-danger" onclick="return confirm('Are You Sure?')">Delete</a>
                    <a href="<?php echo base_url();?>admin/Barang/editpinjaman/<?= $data['id_pinjam']; ?>"class="badge badge-info">Edit Pinjaman</a>
                  </td>
              </tr>
            <?php endforeach;?>
            <?php }?>
                  </tbody>
                </table>
                  <?= $this->pagination->create_links(); ?>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
