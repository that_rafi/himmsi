<div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-5 d-none d-lg-block " style="background-image: url('<?php echo base_url()?>upload/randompic/cats_black2.jpg'); background-size:cover"></div>
          <div class="col-lg-7">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4"><?= $title; ?></h1>
              </div>
                <?= $this->session->flashdata('message'); ?>
                <?= $this->session->flashdata('error'); ?>
              <form class="profile" method="post" action="<?php echo base_url();?>admin/Contact" enctype="multipart/form-data">
                <div class="form-group">
                    <input type="text" name="addname" class="form-control form-control-profile" id="exampleFirstName" placeholder="Nama ALamat" value="<?= $edit['address_name']; ?>">
                    <?= form_error('addname','<small class="text-danger pl-3">','</small>'); ?>
                </div>
                <div class="form-group">
                    <input type="text" name="add1" class="form-control form-control-profile" id="exampleFirstName" placeholder="Alamat 1" value="<?= $edit['address_1']; ?>">
                    <?= form_error('add1','<small class="text-danger pl-3">','</small>'); ?>
                </div>
                <div class="form-group">
                    <input type="text" name="add2" class="form-control form-control-profile" id="exampleFirstName" placeholder="Alamat 2" value="<?= $edit['address_2']; ?>">
                    <?= form_error('add2','<small class="text-danger pl-3">','</small>'); ?>
                </div>
                <div class="form-group">
                    <input type="text" name="add3" class="form-control form-control-profile" id="exampleFirstName" placeholder="Alamat 3" value="<?= $edit['address_3']; ?>">
                    <?= form_error('add3','<small class="text-danger pl-3">','</small>'); ?>
                </div>
                <div class="form-group">
                  <input type="email" name="email" class="form-control form-control-profile" id="exampleInputEmail" placeholder="Email Address" value="<?= $edit['email']; ?>">
                  <?= form_error('email','<small class="text-danger pl-3">','</small>'); ?>
                </div>
                <div class="form-group">
                    <input type="text" name="cn1" class="form-control form-control-profile" id="exampleFirstName" placeholder="Contact Person 1" value="<?= $edit['contactname_1']; ?>">
                    <?= form_error('cn1','<small class="text-danger pl-3">','</small>'); ?>
                </div>
                <div class="form-group">
                  <input type="tel" name="c1" class="form-control form-control-profile"  placeholder="No Hp CP1" value="<?= $edit['contact_1']; ?>">
                  <?= form_error('c1','<small class="text-danger pl-3">','</small>'); ?>
                </div>
                <div class="form-group">
                    <input type="text" name="cn2" class="form-control form-control-profile" id="exampleFirstName" placeholder="Contact Person 2" value="<?= $edit['contactname_2']; ?>">
                    <?= form_error('cn2','<small class="text-danger pl-3">','</small>'); ?>
                </div>
                <div class="form-group">
                  <input type="tel" name="c2" class="form-control form-control-profile"  placeholder="No HP CP2" value="<?= $edit['contact_2']; ?>">
                  <?= form_error('c2','<small class="text-danger pl-3">','</small>'); ?>
                </div>
                <div class="form-group">
                    <input type="text" name="link1" class="form-control form-control-profile" id="exampleFirstName" placeholder="Link Youtube" value="<?= $edit['link_youtube']; ?>">
                    <?= form_error('link1','<small class="text-danger pl-3">','</small>'); ?>
                </div>
                <div class="form-group">
                    <input type="text" name="link2" class="form-control form-control-profile" id="exampleFirstName" placeholder="Link Facebook" value="<?= $edit['link_facebook']; ?>">
                    <?= form_error('link2','<small class="text-danger pl-3">','</small>'); ?>
                </div>
                <div class="form-group">
                    <input type="text" name="link3" class="form-control form-control-profile" id="exampleFirstName" placeholder="Link Instragram" value="<?= $edit['link_ig']; ?>">
                    <?= form_error('link3','<small class="text-danger pl-3">','</small>'); ?>
                </div>
                <input type="submit" value="Update Contact" class="btn btn-primary btn-profile btn-block"></input>
              </form>
          </div>
        </div>
      </div>
    </div>

  </div>
