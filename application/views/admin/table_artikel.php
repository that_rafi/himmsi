<!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800"><?= $tabletitle ?></h1>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary" ><a href="<?php echo base_url();?>admin/Artikel/inputartikel">Tambah Artikel</a></h6>
            </div>
            <div class="card-body">
              <?= $this->session->flashdata('message'); ?>
              <div class="table-responsive" >
                <table class="table table-sm table-bordered" id="dataTable" width="100%" cellspacing="0" >
                  <thead>
                    <tr>
                      <?php for($i=0;$i<count($header);$i++){ ?>
                      <th><?= $header[$i]; ?></th>
                    <?php }?>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <?php for($i=0;$i<count($header);$i++){ ?>
                      <th><?= $header[$i]; ?></th>
                    <?php }?>
                    </tr>
                  </tfoot>
                  <tbody> <!-- Tabel Relasi-->
                    <?php if($akses['m_artikel']==1){ ?><!-- Tabel Artikel-->
              <?php $count=1; foreach($artikel as $data): ?>
              <tr>
                <td><?= $count; ?></td>
                <td><?= $data['kategori']; ?></td>
                <td><?= $data['jabatan']; ?></td>
                <td><?= $data['tanggal']; ?></td>
                <td><?= $data['judul']; ?></td>
                <td><?= mb_strimwidth($data['isi'], 0, 50, "..."); ?></td>
                <td><?= $data['tipe']; ?></td>
                <td><?= $data['status']; ?></td>
                <td><?= $data['tema']; ?></td>
                <td><?php if($iduser == 8){?>
                      <a href="<?php echo base_url();?>admin/Artikel/updatestatusartikel/<?= $data['id_artikel']; ?>"class="badge badge-info">Edit Status</a>
                  <?php }?>
                  <?php if($iduser == $data['id_jabatan']){?>
                    <a href="<?php echo base_url();?>admin/Artikel/updateartikel/<?= $data['id_artikel']; ?>"class="badge badge-info">Edit</a>
                  <?php }?>

                <a href="<?php echo base_url();?>admin/Artikel/deleteartikel/<?= $data['id_artikel']; ?>" class="badge badge-danger" onclick="return confirm('Are You Sure?')">Delete</a></td>
              </tr>
            <?php $count++; endforeach;?>
            <?php }?>
                  </tbody>
                </table>
                <?= $this->pagination->create_links(); ?>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
