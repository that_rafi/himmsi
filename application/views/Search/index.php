<!--================ Start banner Area =================-->
<section class="banner-area relative">
    <div class="overlay overlay-bg"></div>
  <div class="banner-content text-center">
    <h1>Artikel Details</h1>
  </div>
</section>
<?php $check =""; $path = "artikel/index/"; ?>
<!--================ End banner Area =================-->

    <!--================Blog Area =================-->
    <section class="blog_area section-gap single-post-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                  <?php if($search){ ?>
                    <h1>Search Result</h1>
                    <p></p><p>Nothing here matches your search, Try other keywords!</p>
                  <?php }?>
                  <div class="row">
                    <?php foreach($result as $a):?>
                    <div class="col-lg-6 col-md-6">
                      <div class="single-amenities " >
                        <div class="amenities-thumb">
                          <img
                            class="img-fluid w-100"
                            src="<?php echo base_url();
      ?>upload/artikel/<?= $a['gambar']; ?>"
                            alt=""
                          />
                        </div>
                        <div class="amenities-details">
                          <h5>
                            <a href="<?php echo base_url();?>Artikel/detail/<?= $a['kategori']; ?>/<?= $a['id_artikel']; ?>"
                              ><?= $a['judul']; ?></a
                            >
                          </h5>
                          <div class="amenities-meta mb-10">
                            <a href="<?php echo base_url();?>Artikel/detail/<?= $a['kategori']; ?>/<?= $a['id_artikel']; ?>" class="">
                              <span class="ti-calendar"></span><?= $a['tanggal'] ?>
                            </a>
                          </div>

                           <p><?php $str = mb_strimwidth($a['isi'], 0, 5, "...");
                            echo $str;
                            ?></p>

                          <div class="d-flex justify-content-between mt-20">
                            <div>
                              <a href="<?php echo base_url();?>Artikel/detail/<?= $a['kategori']; ?>/<?= $a['id_artikel']; ?>" class="blog-post-btn">
                                Read More <span class="ti-arrow-right"></span>
                              </a>
                            </div>
                            <div class="category">
                              <a href="<?php echo base_url();?>Artikel/Search/<?= $a['kategori']; ?>">
                                <span class="ti-folder mr-1"></span> <?= $a['kategori']; ?>
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  <?php endforeach;?>
                </div>
                <div class="row">
                  <div class="col-lg-12">
                    <?= $this->pagination->create_links(); ?>
                  </div>
                </div>
            </div>

      <div class="col-lg-4 sidebar-widgets">
          <div class="widget-wrap">
            <div class="single-sidebar-widget search-widget">
              <form class="search-form" action="<?php echo base_url();?>Artikel/Search/" method="post">
                <input placeholder="Search Posts"  name="search" value="<?php echo set_value('search'); ?>" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Search Posts'">
                <button type="submit"><i class="fa fa-search"></i></button>
              </form>
            </div>


            <div class="single-sidebar-widget post-category-widget">
              <h4 class="category-title">Catgories</h4>
              <ul class="cat-list mt-20">
                <li>
                  <a href="<?= base_url(); ?>Artikel/Search/All" class="d-flex justify-content-between">
                    <p>All</p>
                    <p><?= $countall ?></p>
                  </a>
                </li>
                <?php $i=0; foreach($kat as $k): ?>
                <li>
                  <a href="<?= base_url(); ?>Artikel/Search/<?= $k['kategori']; ?>" class="d-flex justify-content-between">
                    <p><?= $k['kategori'] ;?></p>
                    <p><?= $count[$i]; $i++;?></p>
                  </a>
                </li>
              <?php endforeach; ?>
              </ul>
            </div>

            <div class="single-sidebar-widget popular-post-widget">
              <h4 class="popular-title">Agenda</h4>
              <div class="popular-post-list">
                <?php foreach($agenda as $agd): ?>
                <div class="single-post-list">
                  <div class="thumb">
                    <img class="img-fluid" src="<?php echo base_url();
?>upload/artikel/<?= $agd['gambar']; ?>" alt="">
                  </div>
                  <div class="details mt-20">
                    <a href="<?php echo base_url();?>Artikel/detail/<?= $agd['kategori']?>/<?= $agd['id_artikel']; ?>">
                      <h6><?= $agd['judul']; ?>"</h6>
                    </a>
                    <p><?= $agd['jabatan']; ?> | <?= $agd['tanggal']; ?></p>
                  </div>
                </div>
              <?php endforeach; ?>
              </div>
            </div>

            </div>
          </div>
            </div>
        </div>
    </section>
    <!--================Blog Area =================-->
