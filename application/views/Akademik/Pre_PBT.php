<!--================ Start banner Area =================-->
<section class="banner-area relative">
    <div class="overlay overlay-bg"></div>
  <div class="banner-content text-center">
    <?php
    if($major == null && $mhs_s == null){?>
    <h1>Pilih Program Studi</h1>
  <?php }else if($mhs_s == null && $major !=null){ ?>
    <h1>Pilih Semester</h1>
  <?php }?>
  </div>
</section>
<!--================ End banner Area =================-->

    <!--================Blog Area =================-->
    <section class="blog_area section-gap single-post-area">
        <div class="container">
            <div class="row">
              <?php if($major == null && $mhs_s == null){?>
                <?php foreach($jurusankat as $j):?>
              <div class="col-md-6 mb-20">
                <h4 class="mb-20"><?= $j['jurusan']; ?></h4>
                <div class="single-amenities " >
                  <div class="amenities-thumb">
                    <img
                      class="img-fluid w-100 h-100"
                      src="<?php echo base_url();
  ?>public/img/aspirasi2.jpg"
                      alt=""
                    />
                  </div>
                  <div class="amenities-details">
                      <?php $jur = ($j['jurusan'] == "Manajemen Informatika") ? "MI" : "SI"; ?>
                    <div class="d-flex justify-content-between ">
                      <div class="container mb-30">
                        <a href="<?php echo base_url();?>Akademik/materipbt/<?= $jur ?>" class="genric-btn primary circle arrow">Pilih<span class="lnr lnr-arrow-right"></span></a>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            <?php endforeach; ?>
              <?php }else if($mhs_s == null && $major !=null){ ?>
                <?php foreach($semesterkat as $s):?>
              <div class="col-md-3 mb-20">
                <h4 class="mb-20"><?= $s['mhs_semester']; ?></h4>
                <div class="single-amenities " >
                  <div class="amenities-thumb">
                    <img
                      class="img-fluid w-100 h-50"
                      src="<?php echo base_url();
  ?>public/img/aspirasi2.jpg"
                      alt=""
                    />
                  </div>
                  <div class="amenities-details">
                    <div class="d-flex justify-content-between">
                      <div class="container mb-30" >
                        <a href="<?php echo base_url();?>Akademik/materipbt/<?= $major; ?>/smt<?= $s['mhs_semester']; ?>" class="genric-btn primary circle arrow">Pilih<span class="lnr lnr-arrow-right"></span></a>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            <?php endforeach; ?>
              <?php }?>
            </div>
        </div>
    </section>
    <!--================Blog Area =================-->
