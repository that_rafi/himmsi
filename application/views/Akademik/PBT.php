<!--================ Start banner Area =================-->
<section class="banner-area relative">
    <div class="overlay overlay-bg"></div>
  <div class="banner-content text-center">
    <h1>PBT Online</h1>
  </div>
</section>
<!--================ End banner Area =================-->

    <!--================Blog Area =================-->
    <section class="blog_area section-gap single-post-area">
        <div class="container">
            <div class="row">
              <div class="col-lg-8">
                <?php foreach($materi as $m): ?>
          <div class="main_blog_details " style="margin-top:-30px">
            <a href="#"><h4><?= $m['matakuliah']; ?></h4></a>
            <?php if($m['video'] != ""){ ?>
            <iframe src="<?= $m['video']; ?>/preview" class="embed-responsive embed-responsive-16by9" height="480px" placeholder="Hello"></iframe>
          <?php }else{?>
              <iframe src="https://www.youtube.com/embed/OV88FhFGEPY" class="embed-responsive embed-responsive-16by9" height="480px" placeholder="Hello"></iframe>
          <?php }?>
            <div class="user_details">
              <div class="float-left">
                <a href="<?php echo base_url();?>Artikel/index/<?= $m['matakuliah']; ?>"><?= $m['keterangan']; ?></a>
              </div>
              <div class="float-right ">
                <div class="media">
                  <div class="media-body">
                    <p><?= $m['tanggal']; ?></p>
                  </div>
                </div>
              </div>
            </div>
            <p></p>

      <!--  <blockquote class="blockquote">
          <p class="mb-0">MCSE boot camps have its supporters and its detractors. Some people do not understand why you should have to spend money on boot camp when you can get the MCSE study materials yourself at a fraction of the camp price. However, who has the willpower to actually sit through a self-imposed MCSE training.</p>
        </blockquote> -->
            <div class="news_d_footer flex-column flex-sm-row">
              <a href="#"><!--<i class="lnr lnr lnr-heart"></i>Lily and 4 people like this--></a>
              <a class="justify-content-sm-center ml-sm-auto mt-sm-0 mt-2" href="#"><!--<i class="lnr lnr lnr-bubble"></i>06 Comments--></a>

            </div>
          </div>
        <?php endforeach;?>
        <div class="row">
          <div class="col-lg-12">
            <?= $this->pagination->create_links(); ?>

          </div>
        </div>
              </div>
              <div class="col-lg-4 sidebar-widgets">
                  <div class="widget-wrap">
                    <div class="single-sidebar-widget search-widget">
                      <form class="search-form" action="<?php echo base_url();?>Himmsi/Search/" method="post">
                        <input placeholder="Search Posts" name="search" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Search Posts'">
                        <button type="submit"><i class="fa fa-search"></i></button>
                      </form>
                    </div>


                    <div class="single-sidebar-widget post-category-widget">
                      <h4 class="category-title">Catgories</h4>
                      <ul class="cat-list mt-20">
                        <li data-toggle="collapse" data-target="#matkul">
                          Matakuliah
                        </li>
                        <?php $i=0; foreach($matkulkat as $k): ?>
                        <li id="matkul" class="collapse" style="width:250px;margin-left:35px">
                          <a href="<?php echo base_url();?>Akademik/materipbt/<?= $major; ?>/smt<?= $mhs_s;?>/<?= $k['matakuliah'] ;?>" class="d-flex justify-content-between">
                            <p><?= $k['matakuliah'] ;?></p>
                            <p><?= $k['COUNT(id_pbt)'];?></p>
                          </a>
                        </li>
                        <?php endforeach; ?>
                    <li data-toggle="collapse" data-target="#ket">
                      Ujian
                    </li>
                    <?php $i=0; foreach($ketkat as $ket): ?>
                    <li id="ket" class="collapse" style="width:250px;margin-left:35px">
                      <a href="<?php echo base_url();?>Akademik/materipbt/<?= $major; ?>/smt<?= $mhs_s;?>/<?= $ket['keterangan'] ;?>" class="d-flex justify-content-between">
                        <p><?= $ket['keterangan'] ;?></p>
                        <p><?= $ket['COUNT(id_pbt)'];?></p>
                      </a>
                    </li>
                  <?php endforeach; ?>
                      </ul>
                    </div>


                    </div>
                  </div>
            </div>
        </div>
    </section>
    <!--================Blog Area =================-->
