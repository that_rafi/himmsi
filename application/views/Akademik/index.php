<!--================ Start banner Area =================-->
<section class="banner-area relative">
    <div class="overlay overlay-bg"></div>
  <div class="banner-content text-center">
    <h1>Akademik</h1>
  </div>
</section>
<!--================ End banner Area =================-->

    <!--================Blog Area =================-->
    <section class="blog_area section-gap single-post-area">
        <div class="container">
            <div class="row">
              <div class="section-top-border">
                <div class="row mt-20">
                  <div class="col-md-4 mb-20">
                    <h4 class="mb-20">Aspirasi Online</h4>
                    <div class="single-amenities " >
                      <div class="amenities-thumb">
                        <img
                          class="img-fluid w-100 h-100"
                          src="<?php echo base_url();
    ?>public/img/aspirasi2.jpg"
                          alt=""
                        />
                      </div>
                      <div class="amenities-details">
                        <div class="amenities-meta mb-10">
                          <a href="<?php echo base_url();?>Akademik/form_aspirasi" class=""></a>
                        </div>
                         <p></p>
                        <div class="d-flex justify-content-between mt-20">
                          <div>
                            <a href="<?php echo base_url();?>Akademik/form_aspirasi" class="genric-btn primary circle arrow">Kirim Aspirasi<span class="lnr lnr-arrow-right"></span></a>
                            <a href="<?php echo base_url();?>Akademik/form_aspirasi" class="blog-post-btn">
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4 mb-20">
                    <h4 class="mb-20">PBT Online</h4>
                    <div class="single-amenities " >
                      <div class="amenities-thumb">
                        <img
                          class="img-fluid w-100 h-100"
                          src="<?php echo base_url();
    ?>public/img/pbt2.jpg"
                          alt=""
                        />
                      </div>
                      <div class="amenities-details">
                        <div class="amenities-meta mb-10">
                          <a href="<?php echo base_url();?>Akademik/pbt_online" class=""></a>
                        </div>
                         <p></p>
                        <div class="d-flex justify-content-between mt-20">
                          <div>
                            <a href="<?php echo base_url();?>Akademik/pbt_online" class="genric-btn primary circle arrow">Mulai Belajar<span class="lnr lnr-arrow-right"></span></a>
                            <a href="<?php echo base_url();?>Akademik/pbt_online" class="blog-post-btn">
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <h4 class="mb-20">Bank Soal</h4>
                    <div class="single-amenities " >
                      <div class="amenities-thumb">
                        <img
                          class="img-fluid w-100"
                          src="<?php echo base_url();?>public/img/bank3.png"
                          alt=""
                        />
                      </div>
                      <div class="amenities-details">
                        <div class="amenities-meta mb-10">
                          <a href="<?php echo base_url();?>Akademik/banksoal" class=""></a>
                        </div>
                         <p></p>
                        <div class="d-flex justify-content-between mt-20">
                          <div>
                            <a href="<?php echo base_url();?>Akademik/banksoal" class="genric-btn primary circle arrow">Mulai Belajar<span class="lnr lnr-arrow-right"></span></a>
                            <a href="<?php echo base_url();?>Akademik/banksoal" class="blog-post-btn">
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>


            </div>
        </div>
    </section>
    <!--================Blog Area =================-->
