<section class="banner-area relative">
    <div class="overlay overlay-bg"></div>
  <div class="banner-content text-center">
    <h1>Departemen</h1>
  </div>
</section>

<style>
h1 {
  margin-top: 0;
  font-size: 3rem;
  display: inline-block;
}
h1 div {
  position: relative;
  float: left;
}
h1 div:first-child {
  color: #3498db;
  margin-right: 1rem;
}
/* PROFIL */
.blog .carousel-indicators {
	left: 0;
	top: auto;
    bottom: -40px;

}

/* The colour of the indicators */
.blog .carousel-indicators li {
    background: #a3a3a3;
    border-radius: 50%;
    width: 8px;
    height: 8px;
    margin-bottom:10px;

}

.blog .carousel-indicators .active {
background: #707070;
margin-bottom:10px;
}

.our-team-section {
  position: relative;
  padding-top: 40px;
  padding-bottom: 40px;
}
.our-team-section:before {
  position: absolute;
  top: -0;
  left: 0;
  content: " ";
  background: url(img/service-section-bottom.png);
  background-size: 100% 100px;
  width: 100%;
  height: 100px;
  float: left;
  z-index: 99;
}
.our-team {
  padding: 0 0 40px;
  background: #f9f9f9;
  text-align: center;
  overflow: hidden;
  position: relative;
  border-bottom: 5px solid #00325a;
}
.our-team:hover {
  border-bottom: 5px solid #2f2f2f;
}

.our-team .pic {
  display: inline-block;
  width: 130px;
  height: 130px;
  margin-bottom: 50px;
  z-index: 1;
  position: relative;
}
.our-team .pic:before {
  content: "";
  width: 100%;
  height: 100%;
  border-radius: 50%;
  background: #233f92;
  position: absolute;
  bottom: 135%;
  right: 0;
  left: 0;
  opacity: 1;
  transform: scale(3);
  transition: all 0.3s linear 0s;
}
.our-team:hover .pic:before {
  height: 100%;
  background: #2f2f2f;
}
.our-team .pic:after {
  content: "";
  width: 100%;
  height: 100%;
  border-radius: 50%;
  background: #ffffff00;
  position: absolute;
  top: 0;
  left: 0;
  z-index: 1;
  transition: all 0.3s linear 0s;
}
.our-team:hover .pic:after {
  background: #f4ab35;
}
.our-team .pic img {
  width: 100%;
  height: 100%;
  border-radius: 50%;
  transform: scale(1);
  transition: all 0.9s ease 0s;
  box-shadow: 0 0 0 14px #f7f5ec;
  transform: scale(0.7);
  position: relative;
  z-index: 2;
}
.our-team:hover .pic img {
  box-shadow: 0 0 0 14px #f7f5ec;
  transform: scale(0.7);
}
.our-team .team-content {
  margin-bottom: 30px;
}
.our-team .title {
  font-size: 22px;
  font-weight: 700;
  color: #4e5052;
  letter-spacing: 1px;
  text-transform: capitalize;
  margin-bottom: 5px;
}
.our-team .post {
  display: block;
  font-size: 15px;
  color: #4e5052;
  text-transform: capitalize;
}
.our-team .social {
  width: 100%;
  padding-top: 10px;
  margin: 0;
  background: #2f2f2f;
  position: absolute;
  bottom: -100px;
  left: 0;
  transition: all 0.5s ease 0s;
}
.our-team:hover .social {
  bottom: 0;
}
.our-team .social li {
  display: inline-block;
}
.our-team .social li a {
  display: block;
  padding-top: 6px;
  font-size: 15px;
  color: #fff;
  transition: all 0.3s ease 0s;
}
.our-team .social li a:hover {
  color: #2f2f2f;
  background: #f7f5ec;
}
@media only screen and (max-width: 990px) {
  .our-team {
    margin-bottom: 10px;
  }
}


button,
input {
    
}

a {
    color: #f96332;
}

a:hover,
a:focus {
    color: #f96332;
}

p {
    line-height: 1.61em;
    font-weight: 300;
    font-size: 1.2em;
}

.category {
    text-transform: capitalize;
    font-weight: 700;
    color: #9A9A9A;
}

body {
    color: #2c2c2c;
    font-size: 10px;
    overflow-x: hidden;
    -moz-osx-font-smoothing: grayscale;
    -webkit-font-smoothing: antialiased;
}

.nav-item .nav-link,
.nav-tabs .nav-link {
    -webkit-transition: all 300ms ease 0s;
    -moz-transition: all 300ms ease 0s;
    -o-transition: all 300ms ease 0s;
    -ms-transition: all 300ms ease 0s;
    transition: all 300ms ease 0s;
}

.card a {
    -webkit-transition: all 150ms ease 0s;
    -moz-transition: all 150ms ease 0s;
    -o-transition: all 150ms ease 0s;
    -ms-transition: all 150ms ease 0s;
    transition: all 150ms ease 0s;
}

[data-toggle="collapse"][data-parent="#accordion"] i {
    -webkit-transition: transform 150ms ease 0s;
    -moz-transition: transform 150ms ease 0s;
    -o-transition: transform 150ms ease 0s;
    -ms-transition: all 150ms ease 0s;
    transition: transform 150ms ease 0s;
}

[data-toggle="collapse"][data-parent="#accordion"][aria-expanded="true"] i {
    filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=2);
    -webkit-transform: rotate(180deg);
    -ms-transform: rotate(180deg);
    transform: rotate(180deg);
}


.now-ui-icons {
    display: inline-block;
    font-size: inherit;
    speak: none;
    text-transform: none;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
}

@-webkit-keyframes nc-icon-spin {
    0% {
        -webkit-transform: rotate(0deg);
    }

    100% {
        -webkit-transform: rotate(360deg);
    }
}

@-moz-keyframes nc-icon-spin {
    0% {
        -moz-transform: rotate(0deg);
    }

    100% {
        -moz-transform: rotate(360deg);
    }
}

@keyframes nc-icon-spin {
    0% {
        -webkit-transform: rotate(0deg);
        -moz-transform: rotate(0deg);
        -ms-transform: rotate(0deg);
        -o-transform: rotate(0deg);
        transform: rotate(0deg);
    }

    100% {
        -webkit-transform: rotate(360deg);
        -moz-transform: rotate(360deg);
        -ms-transform: rotate(360deg);
        -o-transform: rotate(360deg);
        transform: rotate(360deg);
    }
}

.now-ui-icons.objects_umbrella-13:before {
    content: "\ea5f";
}

.now-ui-icons.shopping_cart-simple:before {
    content: "\ea1d";
}

.now-ui-icons.shopping_shop:before {
    content: "\ea50";
}

.now-ui-icons.ui-2_settings-90:before {
    content: "\ea4b";
}

.nav-tabs {
    border: 0;
    padding: 15px 0.7rem;
    margin-left:50px;
}

.nav-tabs:not(.nav-tabs-neutral)>.nav-item>.nav-link.active {
    box-shadow: 0px 5px 5px 0px rgba(0, 0, 0, 0.3);
}

.card .nav-tabs {
    border-top-right-radius: 0.1875rem;
    border-top-left-radius: 0.1875rem;
}

.nav-tabs>.nav-item>.nav-link {
    color: #888888;
    margin: 0;
    margin-right: 5px;
    background-color: transparent;
    border: 1px solid transparent;
    border-radius: 30px;
    font-size: 13px;
    padding: 11px 23px;
    line-height: 1.5;
}

.nav-tabs>.nav-item>.nav-link:hover {
    background-color: transparent;
}

.nav-tabs>.nav-item>.nav-link.active {
    background-color: #444;
    border-radius: 30px;
    color: #FFFFFF;
}

.nav-tabs>.nav-item>.nav-link i.now-ui-icons {
    font-size: 14px;
    position: relative;
    top: 1px;
    margin-right: 3px;
}

.nav-tabs.nav-tabs-neutral>.nav-item>.nav-link {
    color: #FFFFFF;
}

.nav-tabs.nav-tabs-neutral>.nav-item>.nav-link.active {
    background-color: rgba(255, 255, 255, 0.2);
    color: #FFFFFF;
}

.card {
    border: 0;
    border-radius: 0.1875rem;
    display: inline-block;
    position: relative;
    width: 100%;
    margin-bottom: 30px;
    box-shadow: 0px 5px 25px 0px rgba(0, 0, 0, 0.2);
}

.card .card-header {
    background-color: transparent;
    border-bottom: 0;
    background-color: transparent;
    border-radius: 0;
    padding: 0;
}

.card[data-background-color="orange"] {
    background-color: #f96332;
}

.card[data-background-color="red"] {
    background-color: #FF3636;
}

.card[data-background-color="yellow"] {
    background-color: #FFB236;
}

.card[data-background-color="blue"] {
    background-color: #2CA8FF;
}

.card[data-background-color="green"] {
    background-color: #15b60d;
}

[data-background-color="orange"] {
    background-color: #e95e38;
}

[data-background-color="black"] {
    background-color: #2c2c2c;
}

[data-background-color]:not([data-background-color="gray"]) {
    color: #FFFFFF;
}

[data-background-color]:not([data-background-color="gray"]) p {
    color: #FFFFFF;
}

[data-background-color]:not([data-background-color="gray"]) a:not(.btn):not(.dropdown-item) {
    color: #FFFFFF;
}

[data-background-color]:not([data-background-color="gray"]) .nav-tabs>.nav-item>.nav-link i.now-ui-icons {
    color: #FFFFFF;
}


@font-face {
  font-weight: normal;
  font-style: normal;

}

.now-ui-icons {
  display: inline-block;
  font-size: inherit;
  speak: none;
  text-transform: none;
  /* Better Font Rendering */
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
}


footer{
    margin-top:50px;
    color: #555;
    background: #fff;
    padding: 25px;
    font-weight: 300;
    background: #f7f7f7;

}
.footer p{
    margin-bottom: 0;
}
footer p a{
    color: #555;
    font-weight: 400;
}

footer p a:hover{
    color: #e86c42;
}

@media screen and (max-width: 768px) {

    .nav-tabs {
        display: inline-block;
        width: 100%;
        padding-left: 100px;
        padding-right: 100px;
        text-align: center;
        margin-left:0px;
    }

    .nav-tabs .nav-item>.nav-link {
        margin-bottom: 5px;
    }
}
</style>

<!--================ End banner Area =================-->

<!--================ Start Blog Post Area =================-->

<section class="sample-text-area">
<!------ Include the above in your HEAD tag ---------->
<div class="container-fluid">
  <ul class="nav nav-tabs" role="tablist">
    <li class="nav-item " style="margin-bottom:10px" >
      <a class="nav-link active"  href="<?= base_url(); ?>Departemen/index/0/#0">PENGURUS HARIAN</a>
    </li>
    <?php foreach($departemen as $d ): ?>
    <li class="nav-item " style="margin-bottom:10px" >
      <a class="nav-link active"  href="<?= base_url(); ?>Departemen/index/<?=  $d['id_jabatan']; ?>/#<?=  $d['id_jabatan']; ?>"><?= $d['jabatan']; ?></a>
    </li>
  <?php endforeach;?>
  </ul>
</div>
    <?php if($flagph){ ?>
      <div class="container" id="0">
    <?php }else{ ?>
      <div class="container" id="<?= $depbyid['id_jabatan']; ?>">
    <?php }?>
        <div class="row">

              <div class="section-top-border" style="margin-left:-5px">

                <div class="row">
              <!------ Include the above in your HEAD tag ---------->
                  <div class="container">
                    <div class="row blog">
                        <div class="col-md-12">
                            <div id="blogCarousel" class="carousel slide" data-ride="carousel">

                                <ol class="carousel-indicators">
                                    <li data-target="#blogCarousel" data-slide-to="0" class="active"></li>
                                    <li data-target="#blogCarousel" data-slide-to="1"></li>
                                </ol>
                                <?php if($flagph){ ?>
                                  <div class="container">
                                  <h2><font color="#001694">PENGURUS HARIAN</font></h2>
                                  </div>
                                  <div class="container mb-30">
                                  <img class="img-fluid" src="<?= base_url(); ?>public/img/garisbiru.png"/>
                                  </div>
                                  <!-- Carousel items -->
                                  <div class="carousel-inner mb-30" >
                                      <div class="carousel-item active">
                                          <div class="row ml-10 ">
                                            <?php foreach($ph as $a): ?>
                                            <div class="col-lg-3 col-md-6 col-sm-6">
                                              <div class="our-team">
                                                <div class="pic">
                                                  <img src="<?= base_url(); ?>upload/adminpic/<?= $a['foto']; ?>">
                                                </div>
                                                <div class="team-content">
                                                  <h3 class="title"><?= $a['nama']; ?></h3>
                                                  <span class="post"><?= $a['jabatan'];?></span>
                                                </div>
                                              </div>
                                            </div>
                                          <?php  endforeach; ?>

                                          </div>
                                      </div>



                                      <!--.item-->
                                      <!--.item-->
                                  </div>
                                <?php }else{ ?>
                                  <div class="container">
                                  <h2><font color="#001694"><?= $depbyid['jabatan']; ?></font></h2>
                                  </div>
                                  <div class="container mb-30">
                                  <img class="img-fluid" src="<?= base_url(); ?>public/img/garisbiru.png"/>
                                  </div>
                                  <!-- Carousel items -->
                                  <div class="carousel-inner mb-30" >
                                      <div class="carousel-item active">
                                          <div class="row ml-10 ">
                                            <div class="col-lg-3 col-md-6 col-sm-6">
                                              <div class="our-team">
                                                <div class="pic">
                                                  <img src="<?= base_url(); ?>upload/adminpic/<?= $depbyid['foto']; ?>">
                                                </div>
                                                <div class="team-content">
                                                  <h3 class="title"><?= $depbyid['nama']; ?></h3>
                                                  <span class="post">Kepala Departemen</span>
                                                </div>
                                              </div>
                                            </div>
                                            <?php foreach($anggota as $a): ?>
                                            <div class="col-lg-3 col-md-6 col-sm-6">
                                              <div class="our-team">
                                                <div class="pic">
                                                  <img src="<?= base_url(); ?>upload/adminpic/<?= $a['foto']; ?>">
                                                </div>
                                                <div class="team-content">
                                                  <h3 class="title"><?= $a['nama']; ?></h3>
                                                  <span class="post"><?= $a['posisi'];?></span>
                                                </div>
                                              </div>
                                            </div>
                                          <?php  endforeach; ?>

                                          </div>
                                      </div>
                                      <?php if($flag){ ?>
                                      <div class="carousel-item">
                                        <div class="row ml-10 ">
                                          <?php foreach($anggota2 as $a): ?>
                                        <div class="col-lg-3 col-md-6 col-sm-6">
                                          <div class="our-team">
                                            <div class="pic">
                                              <img src="<?= base_url(); ?>upload/adminpic/<?= $a['foto']; ?>">
                                            </div>
                                            <div class="team-content">
                                              <h3 class="title"><?= $a['nama']; ?></h3>
                                              <span class="post"><?= $a['posisi'];?></span>
                                            </div>
                                          </div>
                                        </div>
                                          <?php  endforeach; ?>
                                        </div>
                                      </div>
                                    <?php }?>


                                      <!--.item-->
                                      <!--.item-->
                                  </div>
                                <?php }?>

                                <!--.carousel-inner-->
                            </div>
                            <!--.Carousel-->
                        </div>
                    </div>
                    </div>

                    <div class="container text-center">
                      <?php if($flagph){ ?>
                        <br><font size="4px" color="#2C2C2C"><b>Pengurus harian (PH)</b> memiliki peran sebagai pengawas dalam setiap persiapan pelaksanaan program kerja selain itu memiliki peran dalam melakukan managerial organisasi</font></br>
                      <?php }else{ ?>
                      <br><font size="4px" color="#2C2C2C"><?= $depbyid['keterangan'];?></font></br>
                      <?php }?>

                    </div>


                </div>
      </div>
        </div>
    </div>
</section>


<!--================ End Blog Post Area =================-->

<!--================ Start Footer Area =================-->
