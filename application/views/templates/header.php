<!DOCTYPE html>
<html lang="zxx" class="no-js">
  <head>
    <!-- Mobile Specific Meta -->
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />
    <!-- Favicon-->
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url();
?>public/img/logobaru.png"/>
    <!-- Author Meta -->
    <meta name="author" content="CodePixar" />
    <!-- Meta Description -->
    <meta name="description" content="" />
    <!-- Meta Keyword -->
    <meta name="keywords" content="" />
    <!-- meta character set -->
    <meta charset="UTF-8" />
    <!-- Site Title -->
    <title>HIMMSI UNIVERSITAS AMIKOM YK</title>

    <link
      href="https://fonts.googleapis.com/css?family=Open+Sans:400,600|Playfair+Display:700,700i"
      rel="stylesheet"
    />
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <!--
			CSS
			============================================= -->

    <link rel="stylesheet" href="<?php echo base_url();
?>public/css/linearicons.css" />
    <link rel="stylesheet" href="<?php echo base_url();
?>public/css/font-awesome.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();
?>public/css/magnific-popup.css" />
    <link rel="stylesheet" href="<?php echo base_url();
?>public/css/nice-select.css" />
    <link rel="stylesheet" href="<?php echo base_url();
?>public/css/owl.carousel.css" />
    <link rel="stylesheet" href="<?php echo base_url();
?>public/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php echo base_url();
?>public/css/bootstrap-datepicker.css" />
    <link rel="stylesheet" href="<?php echo base_url();
?>public/css/themify-icons.css" />
    <link rel="stylesheet" href="<?php echo base_url();
?>public/css/main.css" />



  </head>

  <body >

    <!--================ Start Header Area =================-->
   <style>
     .home-banner-area .container-fluid:before {
       background-color: #233F92;
     }
     .genric-btn.success{
    background-color:#233F92;
    font-size : 14px;
    }
    .genric-btn.success:hover{
      color:#233F92;border:1px solid #233F92;background:#fff
    }
   </style>
    <header class="header-area">
      <div class="container">
        <div class="header-wrap">
          <div
            class="header-top d-flex justify-content-between align-items-lg-center navbar-expand-lg"
          >
          <div class="col menu-left">
            <nav class="col navbar navbar-expand-lg justify-content-end">
              <!-- Toggler/collapsibe Button -->
              <!-- Navbar links -->
              <div
                class="collapse navbar-collapse menu-right"
                id="collapsibleNavbar"
              >
                <ul class="navbar-nav justify-content-center w-100" >
                  <li class="nav-item">
                    <a class="nav-link no-italics" href="<?php echo base_url();
  ?>Himmsi">Home</a>
                  </li>
                  <li class="nav-item dropdown">
                    <a class="nav-link" href="<?= base_url(); ?>Profile"  id="dropdownp" >Profile</a>
                    <!--<div class="dropdown-menu" aria-labelledby="dropdownp">
                      <?php foreach($headerprofile as $h):?>
                      <a class="dropdown-item" href="<?= base_url(); ?>Profile/#<?= $h['id_artikel']?>"><?= $h['judul'];?></a>
                    <?php endforeach;?>
                  </div>-->
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url();
  ?>Departemen/index/">Departemen</a>
                  </li>
                </ul>
              </div>
            </nav>
            </div>
            <!-- logo -->
            <div class="col-5 text-lg-center mt-2 mt-lg-0">

                  <a href="<?= base_url(); ?>Himmsi"
                    ><img class="mx-auto" src="<?php echo base_url();
?>public/img/logobaru.png" alt=""
                  /></a>

            </div>
            <div class="col menu-right">
            <nav class="col navbar navbar-expand-lg justify-content-end">
              <!-- Toggler/collapsibe Button -->
              <button
                class="navbar-toggler"
                type="button"
                data-toggle="collapse"
                data-target="#collapsibleNavbar"
              >
                <span class="lnr lnr-menu"></span>
              </button>

              <!-- Navbar links -->
              <div
                class="collapse navbar-collapse menu-right"
                id="collapsibleNavbar"
              >
                <ul class="navbar-nav justify-content-center w-100" >
                  <li class="nav-item hide-lg ">
                    <a class="nav-link "  href="<?php echo base_url();?>Himmsi">Home</a>
                  </li>
                  <li class="nav-item hide-lg">
                    <a class="nav-link" href="<?= base_url(); ?>Profile">Profile</a>
                  </li>
                  <li class="nav-item hide-lg">
                    <a class="nav-link" href="<?php echo base_url();?>Departemen">Departemen</a>
                  </li>
                  <!-- Dropdown -->
                  <!-- <li class="nav-item dropdown">
                    <a
                      class="nav-link dropdown-toggle"
                      href="#"
                      id="navbardrop"
                      data-toggle="dropdown"
                    >
                      Pages
                    </a>
                    <div class="dropdown-menu">
                      <a class="dropdown-item" href="elements.html">Elements</a>
                    </div>
                  </li> -->
                  <li class="nav-item dropdown">
                    <a class="nav-link" href="<?php echo base_url();?>Akademik/" id="dropdown08" >Akademik</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown08">
                      <a class="dropdown-item" href="<?= base_url(); ?>Akademik/form_aspirasi">Aspirasi Online</a>
                      <a class="dropdown-item" href="<?= base_url(); ?>Akademik/pbt_online">PBT</a>
                      <a class="dropdown-item" href="<?= base_url(); ?>Akademik/banksoal">Bank Soal</a>
                    </div>

                  </li>
                  <li class="nav-item">
                    <a class="nav-link"  href="<?php echo base_url();
?>Artikel/index/0">Artikel</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url();
?>Kontak">Kontak</a>
                  </li>
                </ul>
              </div>
            </nav>
          </div>
          </div>
        </div>
      </div>
    </header>
