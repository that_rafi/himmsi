<!--================ Start Footer Area =================-->
<footer class="footer-area section-gap">
	<div class="container">
		<div class="row">
			<div class="col-lg-3  col-md-6 col-sm-6">
				<div class="single-footer-widget">
					<a href="<?= base_url();?>Himmsi"
						><img class="mx-auto" src="<?php echo base_url();
?>public/img/logoputih.png" alt="" width="170px" height="150px"/></a>

				</div>
			</div>
			<div class="col-lg-4  col-md-6 col-sm-6">
				<div class="single-footer-widget">
					<h6><?= $contact['address_name']; ?></h6>
					<p><?= $contact['address_1']; ?><br>
					<?= $contact['address_2']; ?><br>
					<?= $contact['address_3']; ?>
						</p>
						<p>Phone : <?= $contact['contact_1']; ?> (<?= $contact['contactname_1']; ?>)<br>
											 <?= $contact['contact_2']; ?> (<?= $contact['contactname_2']; ?>)<br>
							 Email : <?= $contact['email']; ?>
						</p>
				</div>
			</div>
			<div class="col-lg-3  col-md-6 col-sm-6">
				<div class="single-footer-widget mail-chimp">
					<h6 class="mb-20">Partner List</h6>
					<ul class="instafeed d-flex flex-wrap">
						<?php foreach($banner as $b): ?>
						<li><img src="<?php echo base_url();?>upload/banner/<?= $b['gambar']; ?>" width ="60px" height="60px"></li>
						<?php endforeach;?>
					</ul>
				</div>
			</div>
			<div class="col-lg-2 col-md-6 col-sm-6">
				<div class="single-footer-widget">
					<h6>Follow Us</h6>
					<div class="footer-social d-flex align-items-center">
						<a href="<?= $contact['link_facebook']; ?> "><i class="fa fa-facebook"></i></a>
						<a href="<?= $contact['link_youtube']; ?> "><i class="fa fa-youtube"></i></a>
						<a href="<?= $contact['link_ig']; ?> "><i class="fa fa-instagram"></i></a>
					</div>

				</div>
			</div>
		</div>
		<div class="footer-bottom d-flex justify-content-center align-items-center flex-wrap">
			<p class="footer-text m-0"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;1997-<script>document.write(new Date().getFullYear());</script> All rights reserved | HIMMSI UNIVERSITAS AMIKOM YOGYAKARTA
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
		</div>
	</div>
</footer>
<!--================ End Footer Area =================-->

<script src="<?php echo base_url();
?>public/js/vendor/jquery-2.2.4.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"
	integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
	crossorigin="anonymous"
></script>
<script src="<?php echo base_url();
?>public/js/vendor/bootstrap.min.js"></script>
<script src="<?php echo base_url();
?>public/js/owl.carousel.min.js"></script>
<script src="<?php echo base_url();
?>public/js/jquery.sticky.js"></script>
<script src="<?php echo base_url();
?>public/js/jquery.tabs.min.js"></script>
<script src="<?php echo base_url();
?>public/js/parallax.min.js"></script>
<script src="<?php echo base_url();
?>public/js/jquery.nice-select.min.js"></script>
<script src="<?php echo base_url();
?>public/js/jquery.ajaxchimp.min.js"></script>
<script src="<?php echo base_url();
?>public/js/jquery.magnific-popup.min.js"></script>
<script
	type="text/javascript"
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"
></script>
<script src="<?php echo base_url();
?>public/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url();
?>public/js/main.js"></script>
<script src='https://www.google.com/recaptcha/api.js' ></script>

</body>
</html>
