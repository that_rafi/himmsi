<!--================ Start banner Area =================-->
<section class="banner-area relative">
  <div class="overlay overlay-bg"></div>
  <div class="banner-content text-center">
    <h1>Profile</h1>
  </div>
</section>
<!--================ End banner Area =================-->

<!-- Start Sample Area -->

<section class="sample-text-area" id="26">
<div class="container">
  <div class="section-top-border">
    <div class="row">
      <!-- if id = 26 -->
      <?php foreach($himmsi as $p):?>
        <?php if($p['id_artikel']==26){?>
        <div class="container mb-30">
          <h1><font color="#001891"><?= $p['judul']; ?></h1>
          <img class="img-fluid" src="<?= base_url(); ?>public/img/garisbiru.png"/>
        </div>
        <div class="col-lg-12 " style="margin-bottom:120px;font-size:16px;text-align: justify;">
        <?php }?>
        <?php if($p['id_artikel']==26){?> <!-- apa itu himmsi -->         
            <?= $p['isi']; ?>
        </div>
      <?php }else if($p['id_artikel']== 108){?>
          <div class="section-top-border" id="<?= $p['id_artikel']; ?>">
            <div class="container">
              <h1 ><font color="#001891"><?= $p['judul']; ?></h1>
            </div>
            <div class="container mb-30">
            <img class="img-fluid"  src="<?= base_url(); ?>public/img/garisbiru.png"/>
            </div>
            <div class="row">
              <div class="col-md-3" >
                <div class="container">
                <img src="<?php echo base_url();
        ?>upload/artikel/<?= $p['gambar']; ?>" alt="" class="img-fluid">
              </div>
              </div>
              <div class="col-md-9 mt-sm-2 left-align-p">
                <div class="container" style="font-size:16px;text-align: justify;">
                <?= $p['isi']; ?>
                </div>
              </div>
            </div>
          </div>
        <?php }else if($p['id_artikel'] == 109){?>
          <div class="col-lg-12 mb-90" id="<?= $p['id_artikel']; ?>">
            <div class="container">
              <h1 ><font color="#001891"><?= $p['judul']; ?></h1>
            </div>
            <div class="container mb-30">
            <img class="img-fluid"  src="<?= base_url(); ?>public/img/garisbiru.png"/>
            </div>
            <div id="accordion">
        <div class="card">
          <div class="card-header" id="headingOne">
            <h5 class="mb-0">
              <button class="btn btn-link" style="color:black;" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                2011 - Sekarang
              </button>
            </h5>
          </div>

          <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
            <div class="card-body">
              <img class="img-fluid mb-30" width="300px" height="300px" src="<?= base_url(); ?>upload/artikel/<?= $p['gambar']; ?>"/>
              <?= $p['isi']; ?>
            </div>
          </div>
        </div>
        <?php for($i=0;$i<COUNT($logo['year']);$i++){ ?>
        <div class="card">
          <div class="card-header" id="heading<?=$logo['tag'][$i];?>">
            <h5 class="mb-0">
              <button class="btn btn-link collapsed" style="color:black;" data-toggle="collapse" data-target="#collapse<?=$logo['tag'][$i];?>" aria-expanded="false" aria-controls="collapse<?=$logo['tag'][$i];?>">
                <?= $logo['year'][$i]; ?>
              </button>
            </h5>
          </div>
          <div id="collapse<?=$logo['tag'][$i];?>" class="collapse" aria-labelledby="heading<?=$logo['tag'][$i];?>" data-parent="#accordion">
            <div class="card-body">
              <img class="img-fluid mb-30"  width="300px" height="300px"  src="<?= base_url(); ?>public/img/logo/<?= $logo['img'][$i]; ?>"/>
          </div>
        </div>
      </div>
    <?php };?>
          </div>
        <?php }else if($p['id_artikel'] == 110){?>
          <div class="section-top-border" id="<?= $p['id_artikel']; ?>" >
            <div class="container">
              <h1 ><font color="#001891"><?= $p['judul']; ?></h1>
            </div>
            <div class="container mb-30">
            <img class="img-fluid"  src="<?= base_url(); ?>public/img/garisbiru.png"/>
            </div>
            <div class="col-lg-12 mb-100" >
            <p class="sample-text">
              <img class="img-fluid"  src="<?= base_url(); ?>upload/artikel/<?= $p['gambar']; ?>"/>
            </p>
          </div>
      </div>
        <?php }else{?>
          <div class="section-top-border" id="<?= $p['id_artikel']; ?>">
            <div class="container">
              <h1 ><font color="#001891"><?= $p['judul']; ?></h1>
            </div>
            <div class="container mb-30">
            <img class="img-fluid"  src="<?= base_url(); ?>public/img/garisbiru.png"/>
            </div>
            <div class="col-lg-12 mb-100" style="font-size:16px;text-align: justify;" >
            <p class="sample-text">
            <?= $p['isi']; ?>
            </p>
          </div>
      </div>
    <?php }?>
    <?php endforeach; ?>
    </div>

  </div>

</div>
</section>
<!-- End Sample Area -->



<!-- Start Align Area -->


</font></font>
</font>

<!-- End Align Area -->
