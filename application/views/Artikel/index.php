<!--================ Start banner Area =================-->
<section class="banner-area relative">
    <div class="overlay overlay-bg"></div>
  <div class="banner-content text-center">
    <h1>Artikel</h1>
  </div>
</section>
<?php $check =""; $path = "artikel/index/"; ?>
<!--================ End banner Area =================-->

    <!--================Blog Area =================-->
    <section class="blog_area section-gap single-post-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                  <?php foreach($artikel as $a): ?>
            <div class="main_blog_details">
              <img class="img-fluid" src="<?php echo base_url();
?>upload/artikel/<?= $a['gambar'];?>" alt="">
              <a href="#"><h4><?= $a['judul']; $check =$a['judul'];  ?></h4></a>
              <div class="user_details">
                <div class="float-left">
                  <a href="<?php echo base_url();?>Artikel/index/<?= $a['kategori']; ?>"><?= $a['kategori']; ?></a>
                </div>
                <div class="float-right mt-sm-0 mt-3">
                  <div class="media">
                    <div class="media-body">
                      <h5><?= $a['jabatan']; ?></h5>
                      <p><?= $a['tanggal']; ?></p>
                    </div>
                    <div class="d-flex">
                      <img src="<?php echo base_url();
        ?>upload/adminpic/<?= $a['foto']; ?>" alt="" width="50px" height="50px">
                    </div>
                  </div>
                </div>
              </div>
              <p><?= $a['isi']; ?></p>

        <!--  <blockquote class="blockquote">
            <p class="mb-0">MCSE boot camps have its supporters and its detractors. Some people do not understand why you should have to spend money on boot camp when you can get the MCSE study materials yourself at a fraction of the camp price. However, who has the willpower to actually sit through a self-imposed MCSE training.</p>
          </blockquote> -->
              <div class="news_d_footer flex-column flex-sm-row">
                <a href="#"><!--<i class="lnr lnr lnr-heart"></i>Lily and 4 people like this--></a>
                <a class="justify-content-sm-center ml-sm-auto mt-sm-0 mt-2" href="#"><!--<i class="lnr lnr lnr-bubble"></i>06 Comments--></a>

              </div>
            </div>
          <?php endforeach;?>
          <?php if(!$detail){
            if($flag){
                $path = "artikel/index/".$a['kategori']."/";
            }
            ?>
            <div class="navigation-area">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-12 nav-left flex-row d-flex justify-content-start align-items-center">
                              <?php
                                foreach($prev_artikel as $p):
                                if($p['judul']!=$check){
                                ?>
                                <div class="thumb" style="width:60px; height:60px;background-size:cover;background-image: url('<?php echo base_url()?>upload/artikel/<?=$p['gambar'];  ?>');" >
                                    <a href="<?php echo base_url(); ?><?= $path; ?><?= $prev_num ?>"><img class="img-fluid" src="<?php echo base_url()?>upload/artikel/" alt="" ></a>
                                </div>
                                <div class="arrow">
                                    <a href="<?php echo base_url(); ?><?= $path; ?><?= $prev_num ?>"><span class="lnr text-white lnr-arrow-left"></span></a>
                                </div>
                                <div class="detials">
                                    <p>Prev Post</p>
                                    <a href="<?php echo base_url(); ?><?= $path; ?><?= $prev_num ?>"><h4><?= $p['judul']; ?></h4></a>
                                </div>
                              <?php }?>
                              <?php endforeach;?>
                            </div>
                            <div class="col-lg-6 col-md-6 col-12 nav-right flex-row d-flex justify-content-end align-items-center">
                              <?php  foreach($next_artikel as $n):
                                if($n['judul']!=$check){
                                ?>
                                <div class="detials">
                                    <p>Next Post</p>
                                    <a href="<?php echo base_url(); ?><?= $path; ?><?= $next_num ?>"><h4><?= $n['judul']; ?></h4></a>
                                </div>
                                <div class="arrow">
                                    <a href="<?php echo base_url(); ?><?= $path; ?><?= $next_num ?>"><span class="lnr text-white lnr-arrow-right"></span></a>
                                </div>
                                <div class="thumb" style="width:60px; height:60px;background-size:cover;background-image: url('<?php echo base_url()?>upload/artikel/<?=$n['gambar'];  ?>');" >
                                    <a href="<?php echo base_url(); ?><?= $path; ?><?= $next_num ?>"><img class="img-fluid" src="<?php echo base_url()?>upload/artikel/" alt="" ></a>
                                </div>
                              <?php }endforeach; ?>
                            </div>
                        </div>
                    </div>
                  <?php }?>
                  <!--  <div class="comments-area">
                        <h4>05 Comments</h4>
                        <div class="comment-list">
                            <div class="single-comment justify-content-between d-flex">
                                <div class="user justify-content-between d-flex">
                                    <div class="thumb">
                                        <img src="public/img/blog/c1.jpg" alt="">
                                    </div>
                                    <div class="desc">
                                        <h5><a href="#">Emilly Blunt</a></h5>
                                        <p class="date">December 4, 2017 at 3:12 pm </p>
                                        <p class="comment">
                                            Never say goodbye till the end comes!
                                        </p>
                                    </div>
                                </div>
                                <div class="reply-btn">
                                       <a href="" class="btn-reply text-uppercase">reply</a>
                                </div>
                            </div>
                        </div>
                        <div class="comment-list left-padding">
                            <div class="single-comment justify-content-between d-flex">
                                <div class="user justify-content-between d-flex">
                                    <div class="thumb">
                                        <img src="public/img/blog/c2.jpg" alt="">
                                    </div>
                                    <div class="desc">
                                        <h5><a href="#">Elsie Cunningham</a></h5>
                                        <p class="date">December 4, 2017 at 3:12 pm </p>
                                        <p class="comment">
                                            Never say goodbye till the end comes!
                                        </p>
                                    </div>
                                </div>
                                <div class="reply-btn">
                                       <a href="" class="btn-reply text-uppercase">reply</a>
                                </div>
                            </div>
                        </div>

                    <div class="comment-form">
                        <h4>Leave a Reply</h4>
                        <form>
                            <div class="form-group form-inline">
                              <div class="form-group col-lg-6 col-md-6 name">
                                <input type="text" class="form-control" id="name" placeholder="Enter Name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Name'">
                              </div>
                              <div class="form-group col-lg-6 col-md-6 email">
                                <input type="email" class="form-control" id="email" placeholder="Enter email address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter email address'">
                              </div>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="subject" placeholder="Subject" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Subject'">
                            </div>
                            <div class="form-group">
                                <textarea class="form-control mb-10" rows="5" name="message" placeholder="Messege" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Messege'" required=""></textarea>
                            </div>
                            <a href="#" class="primary-btn submit_btn">Post Comment</a>
                        </form>
                    </div>-->
      </div>

      <div class="col-lg-4 sidebar-widgets">
          <div class="widget-wrap">
            <div class="single-sidebar-widget search-widget">
              <form class="search-form" action="<?php echo base_url();?>Artikel/Search/" method="post">
                <input placeholder="Search Posts" name="search" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Search Posts'">
                <button type="submit"><i class="fa fa-search"></i></button>
              </form>
            </div>


            <div class="single-sidebar-widget post-category-widget">
              <h4 class="category-title">Catgories</h4>
              <ul class="cat-list mt-20">
                <li>
                  <a href="<?= base_url(); ?>Artikel/index/0" class="d-flex justify-content-between">
                    <p>All</p>
                    <p><?= $countall ?></p>
                  </a>
                </li>
                <?php $i=0; foreach($kat as $k): ?>
                <li>
                  <a href="<?= base_url(); ?>Artikel/index/<?= $k['kategori']; ?>" class="d-flex justify-content-between">
                    <p><?= $k['kategori'] ;?></p>
                    <p><?= $count[$i]; $i++;?></p>
                  </a>
                </li>
              <?php endforeach; ?>
              </ul>
            </div>

            <div class="single-sidebar-widget popular-post-widget">
              <h4 class="popular-title">Agenda</h4>
              <div class="popular-post-list">
                <?php foreach($agenda as $agd): ?>
                <div class="single-post-list">
                  <div class="thumb">
                    <img class="img-fluid" src="<?php echo base_url();
?>upload/artikel/<?= $agd['gambar']; ?>" alt="">
                  </div>
                  <div class="details mt-20">
                    <a href="<?php echo base_url();?>Artikel/detail/<?= $agd['kategori']?>/<?= $agd['id_artikel']; ?>">
                      <h6><?= $agd['judul']; ?>"</h6>
                    </a>
                    <p><?= $agd['jabatan']; ?> | <?= $agd['tanggal']; ?></p>
                  </div>
                </div>
              <?php endforeach; ?>
              </div>
            </div>

            </div>
          </div>
            </div>
        </div>
    </section>
    <!--================Blog Area =================-->
