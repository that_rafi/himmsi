<?php
class Help extends CI_Controller{
  public function __construct(){
    parent::__construct();
    $this->load->helper('url');
    $this->load->database();
    $this->load->model('Content_model');
    $this->data['title'] = "Himmsi Homepage";
    $this->data['contact'] = $this->Content_model->getAllContact();
    $this->data['headerprofile'] = $this->Content_model->getArtikelProfileID();
    $this->data['banner'] = $this->Content_model->getAllBanner();
  }
  public function index(){
    $this->load->view('templates/header',$this->data);
    $this->load->view('Help/index',$this->data);
    $this->load->view('templates/footer');
  }
}
