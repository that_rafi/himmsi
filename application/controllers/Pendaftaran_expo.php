<?php
class Pendaftaran_expo extends CI_Controller{
  private $data=null ;
  public function __construct(){
    parent::__construct();
    $this->load->helper('url');
    $this->load->database();
    $this->load->model('Content_model');
    $this->load->library('form_validation');
  }
  public function index(){
    $this->form_validation->set_rules('name','Nama Mahasiswa','trim|required');
    $this->form_validation->set_rules('nim','NIM Mahasiswa','trim|required');
    $this->form_validation->set_rules('kelas','Kelas Mahasiswa','trim|required');
    $this->form_validation->set_rules('birthday','TTL Mahasiswa','trim|required');
    $this->form_validation->set_rules('gender','Jenis Kelamin Mahasiswa','trim|required');
    $this->form_validation->set_rules('nomerhp','Nomer WA Mahasiswa','trim|required');
    $this->form_validation->set_rules('prodi','Prodi Mahasiswa','trim|required');
    $this->form_validation->set_rules('email','Email','trim|required');
    $this->form_validation->set_rules('departemen','departemen HIMMSI','trim|required');
    $this->data['jabatan'] = $this->Content_model->getJabatan();
    $this->data['jurusan'] = $this->Content_model->getJurusan();
    //$captcha_answer = $this->input->post('g-recaptcha-response');
    //$response = $this->recaptcha->verifyResponse($captcha_answer);
      if($this->form_validation->run() == false ){ //&& $response['success']==false
        $this->load->view('Form/form_expo',$this->data);
      }else{
        $this->_input();
      }

  }

  private function _input(){
    $name = $this->input->post('name');
    $nim = $this->input->post('nim');
    $kelas = $this->input->post('kelas');
    $bday = $this->input->post('birthday');
    $gender = $this->input->post('gender');
    $alamatA = $this->input->post('alamat_asal');
    $alamatT = $this->input->post('alamat_tinggal');
    $no_wa = $this->input->post('nomerhp');
    $email = $this->input->post('email');
    $prodi = $this->input->post('prodi');
    $dpt = $this->input->post('departemen');
    $alasan = $this->input->post('alasan');

    if($this->Content_model->insertExpo($name,$nim,$kelas,$gender,$prodi,$bday,$alamatA,$alamatT,$dpt,$no_wa,$email,$alasan,0,'belum_lunas')){
      $this->session->set_flashdata('message','<div class="alert alert-success" role="alert"><font color="green">Terimakasih Sudah Mendaftar Silahkan Langsung Membayar Ya!</font></div>');
      redirect('Pendaftaran_expo');
    }else{
      $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert"><font color="red">Mohon Maaf ulangi lagi!</font></div>');
      redirect('Pendaftaran_expo');
    }
  }


}
?>
