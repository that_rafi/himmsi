<?php
class Artikel extends CI_Controller{
  private $data=null ;
  public function __construct(){
    parent::__construct();
    $this->load->helper('url');
    $this->load->database();
    $this->load->library('pagination');
    $this->load->helper('form');
    $this->load->model('Content_model');
    $this->data['slider'] = $this->Content_model->getAllSlider();
    $this->data['title'] = "Himmsi Homepage";
    $this->data['agenda'] = $this->Content_model->getAllAgenda();
    $this->data['kat'] = $this->Content_model->getAllKategori();
    $this->data['detail'] = false;
    $this->data['search'] = false;
    $i = 0;
    foreach($this->data['kat'] as $k) :
      $this->data['count'][$i] = $this->Content_model->getCountKategori($k['id_kategori']);
      $i++;
    endforeach;
    $this->data['countall']=$this->Content_model->getCountAllPublishArtikel();
    $this->data['contact'] = $this->Content_model->getAllContact();
    $this->data['headerprofile'] = $this->Content_model->getArtikelProfileID();
    $this->data['banner'] = $this->Content_model->getAllBanner();
  }
  public function index(){
    $rowsPerPage = 1;
    $this->data['flag'] = false;
    // by kategori -- checking if kategori exists or no
    $kat=$this->uri->segment(3);
    $flag = false;
    foreach($this->data['kat'] as $k) :
      if($kat == $k['kategori']){
        $flag = true;
        $this->data['flag']=true;
      }
    endforeach;
    if($flag){
      // Set page number
      if($kat != "" || $kat != null)
          $pageNum = $this->uri->segment(4);
      else
          $pageNum = 1;
      if($pageNum !=0){
              $PreviousPageNumber = $pageNum - 1;
              $NextPageNumber = $pageNum + 1;
              $GetPreviousRecord = ($pageNum - 1) * $rowsPerPage;
      }else{
              $PreviousPageNumber = 0;
              $NextPageNumber =$pageNum + 1 ;
              $GetPreviousRecord = (0) * $rowsPerPage;
      }
        // pagination
      $nextq =$this->Content_model->getArtikelByKategori($rowsPerPage,$NextPageNumber,$kat);
      $prevq =$this->Content_model->getArtikelByKategori($rowsPerPage,$GetPreviousRecord,$kat);
      $artikel =$this->Content_model->getArtikelByKategori($rowsPerPage,$pageNum,$kat);
      // end pagination
    }else{
      // Set page number
      if($kat != "" || $kat != null)
          $pageNum = $this->uri->segment(3);
      else
          $pageNum = 1;
      if($pageNum !=0){
                $PreviousPageNumber = $pageNum - 1;
                $NextPageNumber = $pageNum + 1;
                $GetPreviousRecord = ($pageNum - 1) * $rowsPerPage;
      }else{
                $PreviousPageNumber = 0;
                $NextPageNumber =$pageNum + 1 ;
                $GetPreviousRecord = (0) * $rowsPerPage;
      }
      // pagination
      $nextq =$this->Content_model->getAllPublishArtikel($rowsPerPage,$NextPageNumber);
      $prevq =$this->Content_model->getAllPublishArtikel($rowsPerPage,$GetPreviousRecord);
      $artikel =$this->Content_model->getAllPublishArtikel($rowsPerPage,$pageNum);
      // end pagination
    }
    // execute
    $this->data['next_num'] = $NextPageNumber;
    $this->data['next_artikel']=$nextq;
    $this->data['prev_num'] = $PreviousPageNumber;
    $this->data['prev_artikel'] = $prevq;
    $this->data['artikel'] = $artikel;

    $this->load->view('templates/header',$this->data);
    $this->load->view('Artikel/index',$this->data);
    $this->load->view('templates/footer');
  }

  public function detail(){
    $id = $this->uri->segment(4);
      // pagination
    $artikel =$this->Content_model->getArtikel($id);
    $this->data['artikel'] = $artikel;
    $this->data['detail'] = true;
    $this->load->view('templates/header');
    $this->load->view('Artikel/index',$this->data);
    $this->load->view('templates/footer');
  }
  function Search(){
    $perpage = 8;
    // by kategori -- checking if kategori exists or no
    $kat=$this->uri->segment(3);
    $flag = false;
    $count = 0;
    foreach($this->data['kat'] as $k) :
      if($kat == $k['kategori'] || $kat == "All" ){
        $count = $this->Content_model->getCountKategori($k['id_kategori']);
        $flag = true;
      }
    endforeach;

    $key = (trim($this->input->post('search',true)))? trim($this->input->post('search',true)) : '';
    $key =  ($this->uri->segment(3)) ? $this->uri->segment(3) : $key;
    if($flag){ // kalau doi mencet category
      $data['start'] = $this->uri->segment(4);
      $key = "a";
      if($kat != "All"){
        $queryc = $count;
        $query = $this->Content_model->getArtikelByKategori($perpage,$data['start'],$kat);
      }else{
        $queryc = $this->Content_model->getCountAllPublishArtikel();
        $query = $this->Content_model->getAllPublishArtikel($perpage,$data['start']);
      }
      $base = "http://himmsi.org/artikel/Search/".$kat;
      //var_dump($data['start']);
      //var_dump($kat);
    }else{
      $data['start'] = $this->uri->segment(4);
      $queryc = $this->Content_model->getCountSearchArtikel($key);
      $query = $this->Content_model->getSearchArtikel($perpage,$data['start'],$key);
      $base = "http://himmsi-beta.cf/artikel/Search/".$key;
      //var_dump($data['start'] );
      //var_dump($kat);
    }
    if($data['start']!=null || $key != ""){
      if($queryc == 0){
        $this->data['search'] = true;
      }
      $this->pagination->initialize($this->initPagination($perpage,$base,$queryc));
      $this->data['result'] = $query;
    }else{
      $this->data['search'] = true;
      $this->data['result'] =    $this->Content_model->getSearchArtikel(0,0,$key);
    }



    $this->load->view('templates/header',$this->data);
    $this->load->view('Search/index',$this->data);
    $this->load->view('templates/footer');
  }
  function initPagination($perpage,$base,$query){
      $config['base_url'] = $base;
      $config['total_rows'] = $query;
      $config['per_page'] =$perpage;
      $config['first_link']       = 'First';
      $config['last_link']        = 'Last';
      $config['next_link']        = 'Next';
      $config['prev_link']        = 'Prev';
      $config['full_tag_open']    = '<div class="pagging text-center">  <nav class="blog-pagination justify-content-center d-flex"><ul class="pagination">';
      $config['full_tag_close']   = '</ul></nav></div>';
      $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
      $config['num_tag_close']    = '</span></li>';
      $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
      $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
      $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
      $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
      $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
      $config['prev_tagl_close']  = '</span>Next</li>';
      $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
      $config['first_tagl_close'] = '</span></li>';
      $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
      $config['last_tagl_close']  = '</span></li>';
      return $config;
 }



}
?>
