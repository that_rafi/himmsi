<?php
class Departemen extends CI_Controller{
  private $data=null ;
  public function __construct(){
    parent::__construct();
    $this->load->helper('url');
    $this->load->database();
    $this->load->model('Content_model');
    $this->data['headerprofile'] = $this->Content_model->getArtikelProfileID();
    $this->data['contact'] = $this->Content_model->getAllContact();
    $this->data['banner'] = $this->Content_model->getAllBanner();
  }
  public function index(){
    $this->data['flag'] = false;
    $this->data['flagph'] = false;
    $this->data['title'] = "Himmsi Departemen";
    $this->data['departemen'] = $this->Content_model->getDepartemen();
    $id = $this->uri->segment(3);
    if($id == 0){
      $this->data['flagph'] = true;
      $this->data['ph'] = $this->Content_model->getPH();
    }else if($id != 0){
      $this->data['depbyid'] = $this->Content_model->getDepartemenByID($id);
      //var_dump($id);
      $this->data['anggota'] = $this->Content_model->getAnggota($id,true);
      $this->data['anggota2'] = $this->Content_model->getAnggota($id,false);
    }

    if($this->Content_model->getCountAnggota($id)>3){
      $this->data['flag'] = true;
    }
    $this->load->helper('url');
    $this->load->view('templates/header',$this->data);
    $this->load->view('Departemen/index',$this->data);
    $this->load->view('templates/footer');
  }
}
?>
