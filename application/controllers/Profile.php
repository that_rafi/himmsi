<?php
class Profile extends CI_Controller{
  private $data=null ;
  public function __construct(){
    parent::__construct();
    $this->load->helper('url');
    $this->load->database();
    $this->load->model('Content_model');
    $this->data['title'] = "Himmsi Profile";
    $this->data['headerprofile'] = $this->Content_model->getArtikelProfileID();
    $this->data['contact'] = $this->Content_model->getAllContact();
    $this->data['banner'] = $this->Content_model->getAllBanner();
  }
  public function index(){
    $data['title'] = "Himmsi Profile";
    $this->load->helper('url');
    $this->data['himmsi'] = $this->Content_model->getProfile();
    $this->data['logo'] = array();
    $this->data['logo']['year'] = array("2007 - 2011","2003 - 2004","2002 - 2003","2001 - 2002","1997 - 2001");
    $this->data['logo']['img'] = array("2007.png","2003.jpg","2002.png","2001.jpg","97.png");
    $this->data['logo']['tag'] = array("two","three","four","five","six");
    $this->load->view('templates/header',$this->data);
    $this->load->view('Profile/index',$this->data);
    $this->load->view('templates/footer');
  }
}
?>
