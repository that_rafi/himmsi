<?php
class Docs extends CI_Controller{
  private $user = null,$id = null;

  public function __construct(){
    parent::__construct();
    $this->load->helper('url');
    $this->load->database();
    $this->load->model('Admin_model');
    $this->id =$this->session->userdata('id_user');
    if($this->id != null){
      $this->user =$this->Admin_model->getAdmin($this->id);
    }
  }
  public function index(){
    $data['profile'] = $this->user;
    if($data['profile'] !=null){
        $this->load->view('admin/Docs/header');
        $this->load->view('admin/Docs/docs.php');
        $this->load->view('admin/Docs/footer');
    }else{
        redirect('admin');
    }
  }

  public function page(){
    $data['profile'] = $this->user;
    if($data['profile'] !=null){
        $this->load->view('admin/Docs/header');
        $target = 'admin/Docs/docs-page.php';
        $this->load->view($target);
        $this->load->view('admin/Docs/footer');
    }else{
        redirect('admin');
    }
  }




}
?>
