<?php
class Aspirasi extends CI_Controller{
  private $data = null,$id = null;

  public function __construct(){
    parent::__construct();
    $this->load->helper('url');
    $this->load->database();
    $this->load->library('form_validation');
    $this->load->model('Aspirasi_model');
    $this->load->model('Pengurus_model');
    $this->load->library('recaptcha');
    $this->load->library('pagination');
    $this->id =$this->session->userdata('id_user');
    if($this->id != NULL){
      $this->data['profile'] =$this->data['profile'] =$this->Pengurus_model->getPengurus($this->id);
      $this->data['akses'] = $this->Pengurus_model->getAkses($this->data['profile']['id_jabatan']);
      if($this->data['akses']['m_aspirasi'] != 1){
        redirect('admin/Dashboard');
      }
    }else{
      redirect('admin/Dashboard');
    }
  }
  public function index(){
      $this->data['header'] = array("ID Aspirasi","Jurusan","Angkatan","Nama","Nim","Email","Hp","Aspirasi","Tanggal","Jawaban","Status","Action");
      $this->data['tabletitle'] = "Aspirasi";
      // pagination
      $config['base_url'] = "http://himmsi.org/admin/aspirasi/index";
      $this->data['start'] = $this->uri->segment(4);
      $queryc = $this->Aspirasi_model->getCountAllAspirasi();
      $config['total_rows'] = $queryc;
      $config['per_page'] = 10;
      $this->pagination->initialize($config);
      $this->data['aspirasi'] = $this->Aspirasi_model->getAllAspirasi($config['per_page'],$this->data['start']);
      // end pagination
        $this->load->view('templates/admin/header',$this->data);
        $this->load->view('admin/table_aspirasi',$this->data);
        $this->load->view('templates/admin/footer');
  }
  public function inputaspirasi(){
    $this->form_validation->set_rules('nama','Nama Mahasiswa','trim|required');
    $this->form_validation->set_rules('nim','Nim Mahasiswa','trim|required');
    $this->form_validation->set_rules('aspirasi','Aspirasi Mahasiswa','trim|required');
    $this->form_validation->set_rules('email','Email Mahasiswa','trim|required|valid_email');
    $this->form_validation->set_rules('jurusan','Jurusan','required|callback_check_default_jurusan');
    $this->form_validation->set_message('check_default_jurusan', 'Please select something other than Jurusan');
    $this->form_validation->set_rules('angkatan','Angkatan','required|callback_check_default_angkatan');
    $this->form_validation->set_message('check_default_angkatan', 'Please select something other than Angkatan');
    //$captcha_answer = $this->input->post('g-recaptcha-response');
    //$response = $this->recaptcha->verifyResponse($captcha_answer);
      if($this->form_validation->run()==false ){ //&& $response['success']==false
        $this->data['pic'] = "cats_black.jpg";
        $this->data['title'] = "Input Aspirasi";
        $this->data['path'] = "aspirasi/inputaspirasi/";
        $this->data['jurusan'] = $this->Aspirasi_model->getJurusan();
        $this->data['angkatan'] = $this->Aspirasi_model->getAngkatan();
        $this->load->view('templates/header',$this->data);
        $this->load->view('Form/form_aspirasi',$this->data);
        $this->load->view('templates/footer');
      }else{
        $this->_input();
      }
  }
  public function updatestatus($id){
    $this->form_validation->set_rules('status','Status','required|callback_check_default_status');
    $this->form_validation->set_message('check_default_status', 'Please select something other than Status');
    if($this->form_validation->run()==false){
      $this->data['result'] = $this->Aspirasi_model->getAspirasi($id);
      $this->data['status']= array("setujui","tolak","draft");
      $this->data['title'] = "Edit Status Aspirasi";
      $this->data['path'] ="aspirasi/updatestatus/";
      $this->data['home'] = "Aspirasi";
      $this->load->view('templates/admin/header',$this->data);
      $this->load->view('admin/ubah_status',$this->data);
      $this->load->view('templates/admin/footer');
    }else{
      $this->_update($id,true); // true for update status
    }
  }
  public function updatejawaban($id){
    $this->form_validation->set_rules('jwb','Jawaban','required|trim');
    if($this->form_validation->run()==false){
      $this->data['result'] = $this->Aspirasi_model->getAspirasi($id);
      $this->data['title'] = "Update Jawaban Aspirasi";
      $this->data['path'] ="aspirasi/updatejawaban/";
      $this->data['home'] = "Aspirasi";
      $this->load->view('templates/admin/header',$this->data);
      $this->load->view('admin/ubah_jawaban',$this->data);
      $this->load->view('templates/admin/footer');
    }else{
      $this->_update($id,false); // false for update jawaban
    }
  }
  public function deleteaspirasi($id){
    if($this->Aspirasi_model->deleteAspirasi($id)){
      $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
      Successfully Deleting Proker</div>');
      redirect('admin/Aspirasi/');
    }else{
      $this->session->set_flashdata('message','<div class="alert alert-dager" role="alert">
      Unsuccessfully Deleting Proker</div>');
      redirect('admin/Aspirasi/');
    }
  }
  private function _update($id,$flag){
  $email = $this->Aspirasi_model->getAspirasi($id)['email'];
    if($flag){
      $status = $this->input->post('status');
      $subject = "Status Aspirasi HIMMSI";
      if($this->Aspirasi_model->updateStatus($id,$status)){
        if($status == 'setujui'){
          $isi = "Selamat Aspirasi Anda Sudah disetujui,Aspirasimu akan kami sampaikan kepada lembaga,Mohon ditunggu Jawaban atas aspirasi mu ya!";
          if($this->_sendemail($email,$subject,$isi)){
            $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
                        Successfully Updating Status</div>');
                        redirect('admin/Aspirasi/');
          }else{
            $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
                      Unsuccessfully Sending Email!</div>');
                      redirect('admin/Aspirasi/updatestatus/'.$id);
          }
        }else if($status =='tolak'){
          $isi = "Mohon maaf aspirasi anda kami tolak,hal ini disebabkan mungkin karena penggunaan kata yang vulgar/tidak sopan,tidak berkaitan dengan akademik,mengandung unsur SARA,dll. Jika ada pertanyaan silahkan reply pada email ini";
          if($this->_sendemail($email,$subject,$isi)){
            $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
                        Successfully Updating Status</div>');
                        redirect('admin/Aspirasi/');
          }else{
            $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
                      Unsuccessfully Sending Email!</div>');
                      redirect('admin/Aspirasi/updatestatus/'.$id);
          }
        }else{
          $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
                      Successfully Updating Status</div>');
                      redirect('admin/Aspirasi/');
        }
      }else{
        $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
        Unsuccessfully Updating Jawaban!</div>');
        redirect('admin/Aspirasi/updatestatus/'.$id);
      }
    }else{ // update jawaban
        $jwb = $this->input->post('jwb');
        $subject = "Jawaban Aspirasi HIMMSI";
        $isi = "Menanggapi atas Aspirasi yang anda kirimkan sebagai berikut : <br>".$this->Aspirasi_model->getAspirasi($id)['aspirasi']."<p>Berikut adalah jawaban dari lembaga :<br> ".$jwb."<p>Terimakasih atas Aspirasi yang anda sampaikan, HIMMSI BERKREASI DAN BERKUALITAS";
        if($this->Aspirasi_model->updateJawaban($id,$jwb)){
          if($this->_sendemail($email,$subject,$isi)){
            $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
            Successfully Updating Jawaban</div>');
            redirect('admin/Aspirasi/');
          }else{
            $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
            Unsuccessfully Sending Email!</div>');
            redirect('admin/Aspirasi/updatejawaban/'.$id);
          }
        }else{
          $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
          Unsuccessfully Updating Jawaban!</div>');
          redirect('admin/Aspirasi/updatejawaban/'.$id);
          }
        }
  }
  private function _input(){
    $nama = $this->input->post('nama');
    $nim = $this->input->post('nim');
    $e = $this->input->post('email');
    $tel = $this->input->post('tel');
    $j = $this->input->post('jurusan');
    $a = $this->input->post('angkatan');
    $aspirasi = $this->input->post('aspirasi');
    $tgl = date('y-m-d');
    if($this->Aspirasi_model->insertAspirasi($j,$a,$nama,$nim,$e,$tel,$aspirasi,$tgl)){
      $subject = "Aspirasi HIMMSI";
      $isi = "Terimakasih Telah Mengirim Aspirasi,Kami akan tidak lanjuti,ditunggu ya informasi selanjutnya";
      if($this->_sendemail($e,$subject,$isi)){
        $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
        Successfully Submitting Aspirasi</div>');
        redirect('admin/Aspirasi/');
      }else{
        $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
        Unsuccessfully Sending Email</div>');
        redirect('admin/Aspirasi');
      }
    }else{
      $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
      Unsuccessfully Submitting Aspirasi!</div>');
      redirect('admin/Aspirasi');
    }
}
  private function _sendemail($email,$subject,$isi){
    // Load PHPMailer library
        $this->load->library('phpmailer_lib');

        // PHPMailer object
        $mail = $this->phpmailer_lib->load();

        // SMTP configuration
        $mail->isSMTP();
        $mail->Host     = 'smtp.gmail.com';
        $mail->SMTPAuth = true;
        $mail->SMTPSecure = 'ssl';
        $mail->Port     = 465;

        $mail->setFrom('himmsiwebsite@gmail.com', 'Aspirasi');
        $mail->addReplyTo('himmsiwebsite@gmail.com', 'Aspirasi');

        // Add a recipient
        $mail->addAddress($email);

        

        // Email subject
        $mail->Subject = $subject;

        // Set email format to HTML
        $mail->isHTML(true);

        // Email body content
        $mailContent = $isi;
        $mail->Body = $mailContent;

        // Send email
        if(!$mail->send()){
            return false; 
        }else{
            return true;
        }
  }

  function check_default_status($post_string)
  {
    return $post_string == '0' ? FALSE : TRUE;
  }
  function check_default_jurusan($post_string)
  {
    return $post_string == '0' ? FALSE : TRUE;
  }
  function check_default_angkatan($post_string)
  {
    return $post_string == '0' ? FALSE : TRUE;
  }
}

?>
