<?php
class Artikel extends CI_Controller{
  private $data = null,$id = null,$flag = false;

  public function __construct(){
    parent::__construct();
    $this->load->helper('url');
    $this->load->database();
    $this->load->library('form_validation');
    $this->load->library('pagination');
    $this->load->model('Pengurus_model');
    $this->data['tipe'] = array("artikel","event","loker","profil");
    $this->data['kategori'] = $this->Pengurus_model->getKatgeori();
    $this->id =$this->session->userdata('id_user');
    if($this->id != NULL){
      $this->data['profile'] =$this->data['profile'] =$this->Pengurus_model->getPengurus($this->id);
      $this->data['akses'] = $this->Pengurus_model->getAkses($this->data['profile']['id_jabatan']);
      if($this->data['akses']['m_artikel'] != 1){
        redirect('admin/Dashboard');
      }
      if($this->data['profile']['id_jabatan'] == 1){
        $this->data['tabletitle'] = "Artikel Ketua";
        $this->data['titleartikel'] = "Add artikel Ketua";
      }else if($this->data['profile']['id_jabatan'] == 2){
        $this->data['tabletitle'] = "Artikel Wakil";
        $this->data['titleartikel'] = "Add artikel Wakil";
      }else if($this->data['profile']['id_jabatan'] == 3 || $this->data['profile']['id_jabatan'] == 12){
        $this->data['tabletitle'] = "Artikel Sekertaris";
        $this->data['titleartikel'] = "Add Artikel Sekertaris";
      }else if($this->data['profile']['id_jabatan'] == 4){
        $this->data['tabletitle'] = "Artikel Ristek";
        $this->data['titleartikel'] = "Add Artikel Ristek";
      }else if($this->data['profile']['id_jabatan'] == 5){
        $this->data['tabletitle'] = "Artikel Keuangan";
        $this->data['titleartikel'] = "Add Artikel Keuangan";
      }else if($this->data['profile']['id_jabatan'] == 6){
        $this->data['tabletitle'] = "Artikel Internal";
        $this->data['titleartikel'] = "Add Artikel Internal";
      }else if($this->data['profile']['id_jabatan'] == 7){
        $this->data['tabletitle'] = "Artikel Aspirasi";
        $this->data['titleartikel'] = "Add Artikel Aspirasi";
      }else if($this->data['profile']['id_jabatan'] == 8){
        $this->data['tabletitle'] = "Artikel Eksternal";
        $this->data['titleartikel'] = "Add Artikel Eksternal";
        $this->flag = true;
      }
    }else{
      redirect('admin/Dashboard');
    }

  }
  public function index(){
  $config['base_url'] = "http://himmsi.org/admin/artikel/index";
  $this->data['start'] = $this->uri->segment(4);
      if($this->flag){
        $queryc = $this->Pengurus_model->getCountAllArtikel();
        $config['total_rows'] = $queryc;
        $config['per_page'] = 10;
        $this->pagination->initialize($config);
        $this->data['artikel'] = $this->Pengurus_model->getAllArtikel($config['per_page'],$this->data['start'],$this->data['profile']['id_jabatan']);
      }else{
        $queryc = $this->Pengurus_model->getCountPengurusArtikel($this->id);
        $config['total_rows'] = $queryc;
        $config['per_page'] = 10;
        $this->pagination->initialize($config);
        $this->data['artikel'] = $this->Pengurus_model->getPengurusArtikel($this->id,$config['per_page'],$this->data['start']);
      }
      $this->data['header'] = array("No","Kategori","Departemen","Tanggal","Judul","Isi","Tipe","Status","Tema","Action");
      $this->data['iduser'] = $this->data['profile']['id_jabatan'];
        $this->load->view('templates/admin/header',$this->data);
        $this->load->view('admin/table_artikel',$this->data);
        $this->load->view('templates/admin/footer');
}
public function inputartikel(){
$access = false;
  if($this->validate($access)){
    $ida = "0";
    $this->_action($access,$ida);
  }else{
    $this->data['pic'] = "cats_nerd.jpg";
    $this->data['btn'] = "Add artikel";
    $this->data['edit'] = $this->Pengurus_model->getArtikel("0");
    $this->data['path'] = "artikel/inputartikel/";
    $this->load->view('templates/admin/header',$this->data);
    $this->load->view('admin/action_artikel',$this->data);
    $this->load->view('templates/admin/footer');
  }
}
public function updateartikel($ida){
$access = true;
  if($this->validate($access)){
    $this->_action($access,$ida);
  }else{
    $this->data['pic'] = "cats_nerd.jpg";
    $this->data['btn'] = "Update Artikel";
    $this->data['edit'] = $this->Pengurus_model->getArtikel($ida);
    $this->data['path'] = "artikel/updateartikel/".$ida;
    $this->data['titleartikel'] = "Edit Artikel ";
    $this->load->view('templates/admin/header',$this->data);
    $this->load->view('admin/action_artikel',$this->data);
    $this->load->view('templates/admin/footer');
  }
}

  public function deleteartikel($id){
    if($this->Pengurus_model->deleteartikel($id)){
      $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
      Successfully Deleting Artikel</div>');
      redirect('admin/Artikel/');
    }else{
      $this->session->set_flashdata('message','<div class="alert alert-dager" role="alert">
      Unsuccessfully Deleting Artikel</div>');
      redirect('admin/Artikel/');
    }
  }
  public function updatestatusartikel($ida){
    if( $this->data['profile']['id_jabatan'] != 8){
      redirect('admin/Artikel');
    }else{
      $this->form_validation->set_rules('status','Status','required|callback_check_default_status');
      $this->form_validation->set_message('check_default_status', 'Please select something other than Status');
      if($this->form_validation->run()==false){
        $this->data['result'] = $this->Pengurus_model->getArtikel($ida);
        $this->data['title'] = "Edit Status Artikel";
        $this->data['status'] = array("draft","publish");
        $this->data['path'] = "artikel/updatestatusartikel/";
        $this->data['home'] = "artikel";
        $this->load->view('templates/admin/header',$this->data);
        $this->load->view('admin/ubah_status',$this->data);
        $this->load->view('templates/admin/footer');
      }else{
        $this->_updatestatus($ida);
      }

    }
  }
  private function _updatestatus($ida){
    $status = $this->input->post('status');
      if($this->Pengurus_model->updateStatusArtikel($this->data['profile']['id_jabatan'],$ida,$status)){
        $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
        Successfully Updating Artikel</div>');
        redirect('admin/Artikel/');
      }else{
        $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
        Unsuccessfully Updating Artikel</div>');
        redirect('admin/Artikel/');
      }
  }

  private function _action($access,$id){
      $judul = $this->input->post('judul');
      $tema = $this->input->post('tema');
      $idc = $this->input->post('cat');
      $tipe = $this->input->post('tipe');
      $tgl = $this->input->post('date1');
      $isi =$this->input->post('isi');
      if($access){
        if (!empty($_FILES["foto"]["name"])) {
            $g = $this->Pengurus_model->uploadImage($judul."_".$tgl);
        } else {
            $g = $this->input->post('old_foto');
        }
        if($g != "default.jpg"){
                if($this->Pengurus_model->updateArtikel($idc,$tgl,$judul,$isi,$g,$tipe,$tema,$id)){
                  $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
                  Successfully Editing Artikel</div>');
                  redirect('admin/Artikel/');
                }else{
                  $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
                  Failed to Edit Artikel!</div>');
                  redirect('admin/Artikel/');
                }
        }else{
          $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
          Something Wrong While Uploading IMG!</div>');
          redirect('admin/Artikel');
          }
      }else{
          $g = $this->Pengurus_model->uploadImage($judul."_".$tgl);
          if($g != "default.jpg"){
                  if($this->Pengurus_model->insertArtikel($idc,$this->id,$tgl,$judul,$isi,$g,$tipe,$tema)){
                    $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
                    Successfully Adding Artikel</div>');
                    redirect('admin/Artikel/');
                  }else{
                    $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
                    Failed to Add Artikel!</div>');
                    redirect('admin/Artikel/');
                  }
          }else{
            $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
            Something Wrong While Uploading IMG!</div>');
            redirect('admin/Artikel');
            }
      }

  }
  public function validate($access){
    if(!$access){
      if (empty($_FILES['foto']['name']))
      {
          $this->form_validation->set_rules('foto', 'Foto Barang', 'required');
      }
    }
    $this->form_validation->set_rules('tipe','Tipe Artikel','required|callback_check_default_tipe');
    $this->form_validation->set_message('check_default_tipe', 'Please something other than the Tipe');
    $this->form_validation->set_rules('cat','Kategori Artikel','required|callback_check_default_cat');
    $this->form_validation->set_message('check_default_cat', 'Please something other than the Kategori');
    $this->form_validation->set_rules('judul','Judul Artikel','trim|required');
    $this->form_validation->set_rules('tema','Tema Artikel','trim|required');
    $this->form_validation->set_rules('isi','Isi Artikel','trim|required');
    $this->form_validation->set_rules('date1','Tanggal Artikel','trim|required');
    if($this->form_validation->run()==false){
      return false;
    }else{
      return true;
    }
  }

  function check_default_tipe($post_string)
  {
    return $post_string == '0' ? FALSE : TRUE;
  }
  function check_default_cat($post_string)
  {
    return $post_string == '0' ? FALSE : TRUE;
  }
  function check_default_status($post_string)
  {
    return $post_string == '0' ? FALSE : TRUE;
  }
}
?>
