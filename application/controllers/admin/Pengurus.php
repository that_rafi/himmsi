<?php
class Pengurus extends CI_Controller{
  private $data = null,$id = null;

  public function __construct(){
    parent::__construct();
    $this->load->helper('url');
    $this->load->database();
    $this->load->library('form_validation');
    $this->load->model('Pengurus_model');
    $this->id =$this->session->userdata('id_user');
    if($this->id !=NULL){
      $this->data['role'] ="pengurus";
      $this->data['profile'] =$this->Pengurus_model->getPengurus($this->id);
      $this->data['akses'] = $this->Pengurus_model->getAkses($this->data['profile']['id_jabatan']);
      if($this->data['profile']['id_jabatan'] == 0){
        redirect('admin/Dashboard');
      }
    }else{
      redirect('admin/Dashboard');
    }

  }
  public function index(){
      $this->data['title'] = "Edit Profile";
        $this->load->view('templates/admin/header',$this->data);
        $this->load->view('admin/edit_profile',$this->data);
        $this->load->view('templates/admin/footer');
  }
  public function updateprofile($id){
    $this->data['title'] = "Edit Profile";
    if(!$this->_validate()){
      $this->load->view('templates/admin/header',$this->data);
      $this->load->view('admin/edit_profile',$this->data);
      $this->load->view('templates/admin/footer');
    }else{
      $this->_update($id);
    }
  }
  public function anggota(){
  if($this->data['akses']['m_anggota'] != 1){
      redirect('admin/Dashboard');
  }
  

  $this->data['header'] = array("ID Anggota","Nama","NIK","Posisi","Foto","Status","Action");
  $pos = $this->uri->segment(4);
  $this->data['countquery'] = $this->Pengurus_model->getDepartemen($this->data['profile']['id_jabatan']);
  $this->data['countall'] = $this->data['countquery']['jml_sekertaris']+$this->data['countquery']['jml_staff'];
  $this->data['currentcount'] = $this->Pengurus_model->getAnggota("",false,$this->data['profile']['id_jabatan'])->num_rows();
  $this->data['flagcount'] = false;
  $this->data['tabletitle'] = "List Anggota";
  $this->data['pos'] = $pos;

  if($pos == NULL || $pos == "" || $pos =="all"){
    $this->data['anggota'] = $this->Pengurus_model->getAnggota("",false,$this->data['profile']['id_jabatan'])->result_array();
  }else{
    $this->data['anggota'] = $this->Pengurus_model->getAnggota($pos,true,$this->data['profile']['id_jabatan'])->result_array();
    $this->data['flagcount'] = true;
    $this->data['currentcount'] = $this->Pengurus_model->getAnggota($pos,true,$this->data['profile']['id_jabatan'])->num_rows();
    if($pos == "sekretaris"){
        $this->data['flagjml'] = true;
    }else if($pos == "staff"){
        $this->data['flagjml'] = false;
    }
  }

    $this->load->view('templates/admin/header',$this->data);
    $this->load->view('admin/table_anggota',$this->data);
    $this->load->view('templates/admin/footer');
}
  public function inputanggota(){
    if($this->data['akses']['m_anggota'] != 1){
        redirect('admin/Dashboard');
    }
    // all
    $this->data['countquery'] = $this->Pengurus_model->getDepartemen($this->data['profile']['id_jabatan']);
    $this->data['countall'] = $this->data['countquery']['jml_sekertaris']+$this->data['countquery']['jml_staff'];
    $this->data['currentcount'] = $this->Pengurus_model->getAnggota("",false,$this->data['profile']['id_jabatan'])->num_rows();
    // sekertaris
    //$this->data['countquerysk'] = $this->Pengurus_model->getAnggota("sekertaris",true)->row_array();;
    $this->data['currentcountsk'] = $this->Pengurus_model->getAnggota("sekertaris",true,$this->data['profile']['id_jabatan'])->num_rows();
    // staff
    //$this->data['countqueryst'] = $this->Pengurus_model->getAnggota("staff",true)->row_array();;
    $this->data['currentcountst'] = $this->Pengurus_model->getAnggota("staff",true,$this->data['profile']['id_jabatan'])->num_rows();

    $this->data['title'] = "Input Anggota";
    $this->data['posisi'] = array("sekretaris","staff");
    $this->data['status'] = array("aktif","nonaktif");
    $this->data['edit'] = $this->Pengurus_model->getAnggotaByID("");
    $this->data['pic'] = "cats_grey.jpg";
    $this->data['valid'] = true;
    $this->data['btn'] = "Input Anggota";
    $this->data['path'] = "pengurus/inputanggota/";
    if(!$this->_validateanggota(false)){
      $this->load->view('templates/admin/header',$this->data);
      $this->load->view('admin/action_anggota',$this->data);
      $this->load->view('templates/admin/footer');
    }else{
      $this->_action(false,0);
    }
  }
  public function editanggota($id){
    if($this->data['akses']['m_anggota'] != 1){
        redirect('admin/Dashboard');
    }

    $this->data['title'] = "Edit Anggota";
    $this->data['posisi'] = array("sekretaris","staff");
    $this->data['status'] = array("aktif","nonaktif");
    $this->data['valid'] = false;
    $this->data['edit'] = $this->Pengurus_model->getAnggotaByID($id);
    $this->data['pic'] = "cats_grey.jpg";
    $this->data['btn'] = "Update Anggota";
    $this->data['path'] = "pengurus/editanggota/".$id;

    if(!$this->_validateanggota(true)){
      $this->load->view('templates/admin/header',$this->data);
      $this->load->view('admin/action_anggota',$this->data);
      $this->load->view('templates/admin/footer');
    }else{
      $this->_action(true,$id);
    }
  }
  public function deleteanggota($id){
    if($this->data['akses']['m_anggota'] != 1){
        redirect('admin/Dashboard');
    }

    if($this->Pengurus_model->deleteAnggota($id)){
      $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
      Successfully Deleting Anggota</div>');
      redirect('admin/Pengurus/anggota');
    }else{
      $this->session->set_flashdata('message','<div class="alert alert-dager" role="alert">
      Unsuccessfully Deleting Anggota</div>');
      redirect('admin/Pengurus/anggota');
    }
  }

  public function print_data(){
    $this->load->library('pdf');
    /* $contn = array(
      "dataku" => array(
          "nama" => "Petani Kode",
          "url" => "http://petanikode.com"
      )
  ); */
    $this->pdf->setPaper('A4', 'potrait');
    $this->pdf->filename = "laporan-petanikode.pdf";
    $this->pdf->load_view( 'Hello WOrl');
  }

  private function _validate(){
    $this->form_validation->set_rules('nama','Full Name','trim|required');
    $this->form_validation->set_rules('telp','Phone Number','trim|required');
    $this->form_validation->set_rules('email','Email','trim|required|valid_email');

    if($this->form_validation->run()==false){
      return false;
    }else{
      return true;
    }
  }


  private function _update($id){
    $nama = $this->input->post('nama');
    $telp = $this->input->post('telp');
    $email = $this->input->post('email');
    if (!empty($_FILES["foto"]["name"])) {
        $foto = $this->Pengurus_model->uploadImageProfile($nama);
    } else {
        $foto = $this->input->post('old_image');
    }
    if($foto != "default.jpg"){
            if($this->Pengurus_model->updateProfile($nama,$email,$telp,$foto,$id)){
              $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
              Successfully Updating Admin</div>');
              redirect("admin/pengurus/");
            }else{
              $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
              Failed to Update Admin!</div>');
              redirect('admin/Pengurus/');
            }
    }else{
            $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
            Something Wrong While Uploading IMG!</div>');
            redirect('admin/Pengurus/');
      }
  }

  private function _validateanggota($access){
    if($this->data['akses']['m_anggota'] != 1){
        redirect('admin/Dashboard');
    }
    if(!$access){
      if (empty($_FILES['foto']['name']))
      {
          $this->form_validation->set_rules('foto', 'Foto Anggota', 'required');
      }
    }
    $this->form_validation->set_rules('nama','Nama Anggota','trim|required');
    $this->form_validation->set_rules('nik','NIK Anggota','trim|required');
    $this->form_validation->set_rules('posisi','Posisi Anggota','required|callback_check_default_posisi');
    $this->form_validation->set_message('check_default_posisi', 'Please something other than the Posisi');
    $this->form_validation->set_rules('status','Status Anggota','required|callback_check_default_status');
    $this->form_validation->set_message('check_default_status', 'Please something other than the Status');

    if($this->form_validation->run()==false){
      return false;
    }else{
      return true;
    }
  }

  private function _action($access,$id){
      $nama = $this->input->post('nama');
      $nik = $this->input->post('nik');
      $p = $this->input->post('posisi');
      $s = $this->input->post('status');
      $this->data['countquery'] = $this->Pengurus_model->getDepartemen($this->data['profile']['id_jabatan']);
      $check = $this->Pengurus_model->getAnggota($p,true,$this->data['profile']['id_jabatan'])->num_rows();
      if($p != $this->Pengurus_model->getAnggotaByID($id)['posisi']){
        if($p == "sekertaris"){
          if((int)$this->data['countquery']['jml_sekertaris'] <= $check){
            $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
            Cannot add another Sekertaris,Max is'.(int)$this->data['countquery']['jml_sekertaris'].' and yours is '.$check.' !</div>');
            redirect('admin/Pengurus/anggota');
          }
        }else if($p == "staff"){
            if((int)$this->data['countquery']['jml_staff'] <= $check){
            $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
            Cannot add another Staff,Max is'.(int)$this->data['countquery']['jml_staff'].' and yours is '.$check.' !</div>');
            redirect('admin/Pengurus/anggota');
          }
        }
      }
      if($access){
        if (!empty($_FILES["foto"]["name"])) {
            $g = $this->Pengurus_model->uploadImageProfile($nama."_Pengurus");
        } else {
            $g = $this->input->post('old_foto');
        }
        if($g != "default.jpg"){
                if($this->Pengurus_model->updateAnggota($nama,$nik,$p,$g,$s,$id)){
                  $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
                  Successfully Editing Anggota</div>');
                  redirect('admin/Pengurus/anggota/');
                }else{
                  $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
                  Failed to Edit Anggota!</div>');
                  redirect('admin/Pengurus/anggota');
                }
        }else{
          $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
          Something Wrong While Uploading IMG!</div>');
          redirect('admin/Pengurus/anggota');
          }
      }else{
          $g = $this->Pengurus_model->uploadImageProfile($nama."_Pengurus");
          if($g != "default.jpg"){
                  if($this->Pengurus_model->insertAnggota($nama,$nik,$p,$g,$s,$this->data['profile']['id_jabatan'])){
                    $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
                    Successfully Adding Anggota</div>');
                    redirect('admin/Pengurus/anggota');
                  }else{
                    $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
                    Failed to Add Anggota!</div>');
                    redirect('admin/Pengurus/anggota');
                  }
          }else{
            $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
            Something Wrong While Uploading IMG!</div>');
            redirect('admin/Pengurus/anggota');
            }
      }

  }

  function check_default_posisi($post_string)
  {
    return $post_string == '0' ? FALSE : TRUE;
  }
  function check_default_status($post_string)
  {
    return $post_string == '0' ? FALSE : TRUE;
  }


}
?>
