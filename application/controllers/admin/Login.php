<?php
class Login extends CI_Controller{
  public function __construct(){
    parent::__construct();
    $this->load->database();
    $this->load->model('Admin_model');
    $this->load->library('form_validation');
    $this->load->helper('form');
    $this->load->helper('url');

  }

  public function index(){
        $login =$this->Admin_model->getAdmin($this->session->userdata('id_user'));
        if($login!=null){
          redirect('admin/Dashboard');
        }
          $this->form_validation->set_rules('username','Username','trim|required');
          $this->form_validation->set_rules('pass','Password','trim|required');
          if($this->form_validation->run()==false){
            $data['title'] = "Login Admin Himmsi";
            $this->load->view('admin/index');
          }else{
            $this->_login();
          }
  }
  private function _login(){
    $username = $this->input->post('username');
    $password = $this->input->post('pass');
    $user = $this->Admin_model->getLogin($username);
    if($user){
      if($user['password'] == $password || password_verify($password,$user['password'])){
        $data =['id_user' => $user['id_user']];
        $this->session->set_userdata($data);
        redirect('admin/Dashboard');
      }else{
        $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
        Password is Invalid!</div>');
        redirect('admin');
      }

    }else{
      $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
      Username or Password is Invalid!</div>');
      redirect('admin');
    }
  }

  public function logout(){
    $this->session->unset_userdata('id_user');
    $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
    You have been logged out!</div>');
    redirect('admin');

  }


}
?>
