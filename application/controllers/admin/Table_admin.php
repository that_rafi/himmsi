<?php
class Table_admin extends CI_Controller{
  private $data = null,$id = null,$id_jabatan=null;

  public function __construct(){
    parent::__construct();
     $this->load->helper(array('form', 'url'));
    $this->load->database();
    $this->load->library('form_validation');
    $this->load->model('Admin_model');
    $this->data['jabatan'] =$this->Admin_model->getJabatan();
    $this->id =$this->session->userdata('id_user');
    $this->data['profile'] = $this->Admin_model->getAdmin($this->id);
    $this->id_jabatan  = $this->data['profile']['id_jabatan'];
    $this->data['akses'] = null;
    if($this->id_jabatan != 0 ){
      redirect('admin');
    }else if($this->id_jabatan == null){
      redirect('admin');
    }
  }
  public function index(){
    $this->data['admin_data'] = $this->Admin_model->getTableAdmin();
    $this->data['header'] = array("No","Name","Email","Phone No","Position","Action");
    $this->data['title'] = "Himmsi Admin Table";
    $this->data['tabletitle'] = "Admin";
        $this->load->view('templates/admin/header',$this->data);
        $this->load->view('admin/table_data',$this->data);
        $this->load->view('templates/admin/footer');

  }
  public function insertadmin(){
    $this->data['tahun'] = $this->Admin_model->getTahun();
        if(!$this->_validate(true)){
          $this->load->view('templates/admin/header',$this->data);
          $this->load->view('admin/insert_admin',$this->data);
          $this->load->view('templates/admin/footer');
        }else{
          $this->_insert();
        }
  }
  // --- HAK AKSES --- //
  public function inputakses(){
    $this->data['title'] = "Input Hak Akses Admin";
    $this->data['home'] = 'table_admin/edithakakses/1';
    $this->data['path'] = 'table_admin/inputakses/';
    $this->data['edit'] = "";
    $this->data['btn'] = "Input Nama Akses";
    $this->form_validation->set_rules('nama','Nama Hak Akses','trim|required');
    if(!$this->form_validation->run()){
      $this->load->view('templates/admin/header',$this->data);
      $this->load->view('admin/action_akses',$this->data);
      $this->load->view('templates/admin/footer');
    }else{
      $this->_hakakses(true);
    }
  }
  public function editnamaakses($id){
    $this->data['title'] = "Edit Nama Hak Akses Admin";
    $this->data['home'] = 'table_admin/edithakakses/1';
    $this->data['path'] = 'table_admin/editnamaakses/'.$id;
    $this->data['edit'] = $id;
    $this->data['btn'] = "Update Nama Akses";
    $this->form_validation->set_rules('nama','Nama Hak Akses','trim|required');
    if(!$this->form_validation->run()){
      $this->load->view('templates/admin/header',$this->data);
      $this->load->view('admin/action_akses',$this->data);
      $this->load->view('templates/admin/footer');
    }else{
      $this->_hakakses(false);
    }
  }
  public function edithakakses(){
    $this->data['header'] = $this->Admin_model->getListField('akses');
    $this->data['access'] = $this->Admin_model->getAkses($this->uri->segment(4));
    $hakakses = array();
    //var_dump($this->data['akses']);
    $this->data['id'] = $id=$this->uri->segment(4);
    if($this->uri->segment(4)!= null || $this->uri->segment(4)!= 0){
      if($this->input->post('submit') != null){
        foreach($this->data['header'] as $h):
          if($h->name == "id_user" || $h->name == "id_akses"){
          }else{
              $hakakses[$h->name] = ($this->input->post($h->name) != null ) ?"1" : "0";
          }
        endforeach;

        if(  $this->data['access']!= null){
          if($this->Admin_model->updateAkses($hakakses,$id)){
            $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
            Successfully Updating Access</div>');
            redirect('admin/Table_admin/edithakakses/'.$id);
          }else{
            $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
            Failed to Update Access!</div>');
            redirect('admin/Table_admin/edithakakses/'.$id);
          }
        }else{
          $hakakses['id_user'] = $id;
          if($this->Admin_model->inputAkses($hakakses)){
            $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
            Successfully Adding Access</div>');
            redirect('admin/Table_admin/edithakakses/'.$id);
          }else{
            $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
            Failed to Add Access!</div>');
            redirect('admin/Table_admin/edithakakses'.$id);
          }
        }
      }
    }


    $this->data['departemen'] = $this->Admin_model->getTableAdmin();
    $this->data['title'] = "Himmsi Admin Hak Akses";

    $this->load->view('templates/admin/header',$this->data);
    $this->load->view('admin/edit_hakakses',$this->data);
    $this->load->view('templates/admin/footer');

  }
  public function deletehakakses($id){
    if($this->Admin_model->deleteTable($id)){
      $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
      Successfully Deleting Hak Akses!</div>');
      redirect('admin/Table_admin/edithakakses');
    }else{
      $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
      Failed to Delete Hak Akses!</div>');
      redirect('admin/Table_admin/edithakakses');
    }
  }
  private function _hakakses($flag){
    $nama = $this->input->post('nama');
    $oldnama = $this->input->post('old_nama');
    if($flag){ //input
      if($this->Admin_model->addField($nama)){
        $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
        Successfully Adding Access</div>');
        redirect('admin/Table_admin/edithakakses/');
      }else{
        $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
        Failed to Add Access!</div>');
        redirect('admin/Table_admin/edithakakses');
      }
    }else{ // update
      if($this->Admin_model->renameTable($oldnama,$nama)){
        $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
        Successfully Editing Access Name</div>');
        redirect('admin/Table_admin/edithakakses');
      }else{
        $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
        Failed to Edit Access Name!</div>');
        redirect('admin/Table_admin/edithakakses');
      }
    }
  }
// --- HAK AKSES --///

  public function editadmin($id){

    $this->data['tahun'] = $this->Admin_model->getTahun();
    $this->data['user'] = $this->Admin_model->getAdmin($id);
    if(!$this->_validate(false)){
      $this->load->view('templates/admin/header',$this->data);
      $this->load->view('admin/edit_admin',$this->data);
      $this->load->view('templates/admin/footer');
    }else{
      $this->_update($id);
    }

  }

  public function deleteadmin($id){
    if($this->Admin_model->deleteAdmin($id)){
      $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
      Successfully Deleting Admin!</div>');
      redirect('admin/Table_admin');
    }else{
      $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
      Failed to Delete Admin!</div>');
      redirect('admin/Table_admin');
    }
  }
  public function changepass($id){
        $this->form_validation->set_rules('pass1','Password','trim|required');
        $this->form_validation->set_rules('pass2','Confirmation Password','trim|required');
        $this->form_validation->set_rules('old_pass','Old Password','trim|required');
        $this->data['id'] = $id;
        if($this->form_validation->run()==false){
          $this->load->view('templates/admin/header',$this->data);
          $this->load->view('admin/change_pass',$this->data);
          $this->load->view('templates/admin/footer');
        }else{
          $this->_updatepass($id);
        }

  }
  public function departemen(){
    $this->data['header'] = array("No","Jabatan","Jml Sekertaris","Jml Staff","Action");
    $this->data['title'] = "Himmsi Departemen Table";
    $this->data['tabletitle'] = "Departemen";
        $this->load->view('templates/admin/header',$this->data);
        $this->load->view('admin/table_departemen',$this->data);
        $this->load->view('templates/admin/footer');

  }
  public function inputdepartemen(){
    $this->data['edit'] = $this->Admin_model->getManageJabatan(0);
    $this->data['btn'] = "Input Departemen";
    $this->data['pic'] = "cats_grey.jpg";
    $this->data['path'] = "table_admin/inputdepartemen";
        if(!$this->_validatedepartemen()){
          $this->load->view('templates/admin/header',$this->data);
          $this->load->view('admin/action_departemen',$this->data);
          $this->load->view('templates/admin/footer');
        }else{
          $this->_actiondepartemen(true,0);
        }
  }
  public function editdepartemen($id){

    $this->data['edit'] = $this->Admin_model->getManageJabatan($id);
    $this->data['btn'] = "Edit Departemen";
    $this->data['pic'] = "cats_grey.jpg";
    $this->data['path'] = "table_admin/editdepartemen/".$id;
        if(!$this->_validatedepartemen()){
          $this->load->view('templates/admin/header',$this->data);
          $this->load->view('admin/action_departemen',$this->data);
          $this->load->view('templates/admin/footer');
        }else{
          $this->_actiondepartemen(false,$id);
        }

  }
  public function deletedepartemen($id){
    if($this->Admin_model->deleteJabatan($id)){
      $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
      Successfully Deleting Departemen!</div>');
      redirect('admin/Table_admin/departemen');
    }else{
      $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
      Failed to Delete Departemen!</div>');
      redirect('admin/Table_admin/departemen');
    }
  }
  private function _validate($access){
    $this->form_validation->set_rules('nama','Full Name','trim|required');
    if($access){
      $this->form_validation->set_rules('pass1','Password','trim|required');
      $this->form_validation->set_rules('pass2','Confirmation Password','trim|required');
      if (empty($_FILES['foto']['name']))
      {
          $this->form_validation->set_rules('foto', 'Photo Profile', 'required');
      }
    }
    $this->form_validation->set_rules('telp','Phone Number','trim|required');
    $this->form_validation->set_rules('year','Tahun Angkatan','required|callback_check_default_year');
    $this->form_validation->set_message('check_default_year', 'Please select something other than Year');
    $this->form_validation->set_rules('jabatan','Jabatan','required|callback_check_default_jabatan');
    $this->form_validation->set_message('check_default_jabatan', 'Please something other than the Jabatan');
    $this->form_validation->set_rules('email','Email','trim|required|valid_email');

    if($this->form_validation->run()==false){
      return false;
    }else{
      return true;
    }
  }
  private function _validatedepartemen(){
    $this->form_validation->set_rules('jabatan','Nama Jabatan','trim|required');
    $this->form_validation->set_rules('jmlsk','Jumlah Sekertaris','trim|required');
    $this->form_validation->set_rules('jmlst','Jumlah Sekertaris','required|trim');
    $this->form_validation->set_rules('ket','Keterangan','trim|required');

    if($this->form_validation->run()==false){
      return false;
    }else{
      return true;
    }
  }
  private function _insert(){
    $nama = $this->input->post('nama');
    $pass1 = $this->input->post('pass1');
    $pass2 = $this->input->post('pass2');
    $telp = $this->input->post('telp');
    $year = $this->input->post('year');
    $jabatan = $this->input->post('jabatan');
    $email = $this->input->post('email');
    $foto = $this->Admin_model->uploadImage($nama);
    if($foto != "default.jpg"){
        if($pass1 == $pass2){
            if($this->Admin_model->insertAdmin($nama,$email,$telp,$pass1,$jabatan,$year,$foto)){
              $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
              Successfully Adding Admin</div>');
              redirect('admin/Table_admin/');
            }else{
              $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
              Failed to Add Admin!</div>');
              redirect('admin/Table_admin/insertadmin');
            }

        }else{
          $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
          Password and Confirmation Password are diffrent!</div>');
          redirect('admin/Table_admin/insertadmin');
        }
    }else{
      $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
      Something Wrong While Uploading IMG!</div>');
      redirect('admin/Table_admin/insertadmin');
      }
    }
    private function _update($id){
      $nama = $this->input->post('nama');
      $pass1 = $this->input->post('pass1');
      $telp = $this->input->post('telp');
      $year = $this->input->post('year');
      $jabatan = $this->input->post('jabatan');
      $email = $this->input->post('email');
      if (!empty($_FILES["foto"]["name"])) {
          $foto = $this->Admin_model->uploadImage($nama);
      } else {
          $foto = $this->input->post('old_image');
      }
      if($foto != "default.jpg"){
              if($this->Admin_model->updateAdmin($nama,$email,$telp,$pass1,$jabatan,$year,$foto,$id)){
                $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
                Successfully Updating Admin</div>');
                redirect("admin/table_admin/");
              }else{
                $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
                Failed to Update Admin!</div>');
                redirect('admin/Table_admin/editadmin/'.$id);
              }
      }else{
              $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
              Something Wrong While Uploading IMG!</div>');
              redirect('admin/Table_admin/editadmin/'.$id);
        }
      }
      private function _updatepass($id){
        $pass1 = $this->input->post('pass1');
        $pass2 = $this->input->post('pass2');
        $old = $this->input->post('old_pass');
        $data['dataadmin'] = $this->Admin_model->getAdmin($id);
        if($data['dataadmin']['password'] == $old || password_verify($old,$data['dataadmin']['password'])){
          if($pass1 == $pass2){
            if($pass1 != $old){
            if($this->Admin_model->updatePassAdmin($pass1,$id)){
              $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
              Successfully Updating Password</div>');
              redirect("admin/table_admin/");
            }else{
              $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
              Failed to Update Password!</div>');
              redirect('admin/Table_admin/changepass/'.$id);
            }
          }else{
            $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
            Please Input different Password</div>');
            redirect('admin/Table_admin/changepass/'.$id);
          }
          }else{
            $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
            Password and Password Confirmation is not Same!!</div>');
            redirect('admin/Table_admin/changepass/'.$id);
          }
        }else{
          $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
          Password is not recorded in Database!!</div>');
          redirect('admin/Table_admin/changepass/'.$id);
        }
      }
    // departemen
    private function _actiondepartemen($flag,$id){
      $j = $this->input->post('jabatan');
      $jmlsk = $this->input->post('jmlsk');
      $jmlst = $this->input->post('jmlst');
      $ket = $this->input->post('ket');
      if($flag){ // input
        if($this->Admin_model->insertJabatan($j,$jmlsk,$jmlst,$ket)){
          $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
          Successfully Adding Departemen</div>');
          redirect('admin/Table_admin/departemen');
        }else{
          $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
          Failed to Add Departemen!</div>');
          redirect('admin/Table_admin/departemen');
        }
      }else{ // edit
        if($this->Admin_model->updateJabatan($j,$jmlsk,$jmlst,$ket,$id)){
          $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
          Successfully Updating Departemen</div>');
          redirect('admin/Table_admin/departemen');
        }else{
          $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
          Failed to Update Departemen!</div>');
          redirect('admin/Table_admin/departemen');
        }
      }

      }

    // validate select form
    function check_default_jabatan($post_string)
    {
      return $post_string == '0' ? FALSE : TRUE;
    }
    function check_default_year($post_string)
    {
      return $post_string == '0' ? FALSE : TRUE;
    }

    // update Profile Admin

    public function updateprofile(){
      $this->data['title'] = "Edit Profile";
      $this->data['role'] ="table_admin";
      if(!$this->_validateprofile()){
        $this->load->view('templates/admin/header',$this->data);
        $this->load->view('admin/edit_profile',$this->data);
        $this->load->view('templates/admin/footer');
      }else{
        $id = $this->id;
        $this->_updateprofile($id);
      }
    }
    private function _validateprofile(){
      $this->form_validation->set_rules('nama','Full Name','trim|required');
      $this->form_validation->set_rules('telp','Phone Number','trim|required');
      $this->form_validation->set_rules('email','Email','trim|required|valid_email');

      if($this->form_validation->run()==false){
        return false;
      }else{
        return true;
      }
    }
    private function _updateprofile($id){
      $nama = $this->input->post('nama');
      $telp = $this->input->post('telp');
      $email = $this->input->post('email');
      if (!empty($_FILES["foto"]["name"])) {
          $foto = $this->Admin_model->uploadImage($nama);
      } else {
          $foto = $this->input->post('old_image');
      }
      if($foto != "default.jpg"){
              if($this->Admin_model->updateProfile($nama,$email,$telp,$foto,$id)){
                $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
                Successfully Updating Admin</div>');
                redirect("admin/table_admin/updateprofile");
              }else{
                $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
                Failed to Update Admin!</div>');
                redirect('admin/Table_admin/updateprofile');
              }
      }else{
              $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
              Something Wrong While Uploading IMG!</div>');
              redirect('admin/Table_admin/updateprofile');
        }
      }


}
?>
