<?php
class Dashboard extends CI_Controller{
  private $user = null,$id = null;

  public function __construct(){
    parent::__construct();
    $this->load->helper('url');
    $this->load->database();
    $this->load->model('Admin_model');
    $this->id =$this->session->userdata('id_user');
    if($this->id != null){
      $this->user =$this->Admin_model->getAdmin($this->id);
    }
  }
  public function index(){
    $data['profile'] = $this->user;
    $data['akses'] = $this->Admin_model->getAkses($data['profile']['id_jabatan']);
    $data['title'] = "Himmsi Admin Page";
    if($data['profile'] !=null){
        $this->load->view('templates/admin/header',$data);
        $this->load->view('admin/dashboard',$data);
        $this->load->view('templates/admin/footer');
    }else{
        redirect('admin');
    }
  }




}
?>
