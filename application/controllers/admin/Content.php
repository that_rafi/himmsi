<?php
class Content extends CI_Controller{
  private $data = null,$id = null;

  public function __construct(){
    parent::__construct();
    $this->load->helper('url');
    $this->load->database();
    $this->load->library('form_validation');
    $this->load->model('Pengurus_model');
    $this->load->model('Content_model');
    $this->id =$this->session->userdata('id_user');
    if($this->id != NULL){
      $this->data['profile'] =$this->data['profile'] =$this->Pengurus_model->getPengurus($this->id);
      $this->data['akses'] = $this->Pengurus_model->getAkses($this->data['profile']['id_jabatan']);
      if($this->data['akses']['m_banner'] != 1){
        redirect('admin/Dashboard');
      }
    }else{
      redirect('admin/Dashboard');
    }
  }
  public function index(){
    $this->banner();
  }
  public function banner(){
    $this->data['header'] = array("ID Banner","Nama Banner","Gambar","Link","Keterangan","Tipe","Action");
    $this->data['content'] = $this->Content_model->getAllBanner();
    $this->data['flag'] = true;
    $this->data['tabletitle'] = "Banner";
    $this->data['iduser'] = $this->data['profile']['id_jabatan'];
      $this->load->view('templates/admin/header',$this->data);
      $this->load->view('admin/table_content',$this->data);
      $this->load->view('templates/admin/footer');
  }
  /*public function slider(){
    $this->data['header'] = array("ID Slider","Judul","Keterangan","Gambar","Action");
    $this->data['content'] = $this->Content_model->getAllSlider();
    $this->data['flag'] = false;
    $this->data['tabletitle'] = "Slider";
    $this->data['iduser'] = $this->data['profile']['id_jabatan'];
      $this->load->view('templates/admin/header',$this->data);
      $this->load->view('admin/table_content',$this->data);
      $this->load->view('templates/admin/footer');
  }*/
public function inputcontent(){
    $this->data['pic'] = "cats_black.jpg";
    $access = false;
  if($this->uri->segment(4)=="banner"){
    $this->data['btn'] = "Add Banner";
    $this->data['edit'] = $this->Content_model->getBanner("0");
    $this->data['titlecontent'] = "Add Banner";
    $this->data['class'] = "Banner";
    $this->data['path'] = "content/inputcontent/banner";
    $this->data['flag'] = true;
    $this->data['tipe'] = array("partner","link");
  }else if($this->uri->segment(4)=="slider"){
    $this->data['btn'] = "Add Slider";
    $this->data['class'] = "Slider";
    $this->data['path'] = "content/inputcontent/slider";
    $this->data['edit'] = $this->Content_model->getSlider("0");
    $this->data['titlecontent'] = "Add Slider";
    $this->data['flag'] = false;
  }else{
    redirect('admin/Dashboard');
  }
    if($this->_validate($access,$this->data['flag'])){
      $id ="0";
      $this->_action($access,$this->data['flag'],$id);
    }else{
      $this->load->view('templates/admin/header',$this->data);
      $this->load->view('admin/action_content',$this->data);
      $this->load->view('templates/admin/footer');
  }


}
  public function editcontent($id){
    $this->data['pic'] = "cats_black.jpg";
    $access = true;
    $id = $this->uri->segment(5);
    if($id == null){
      redirect('admin/Dashboard');
    }
  if($this->uri->segment(4)=="banner"){
    $this->data['btn'] = "Edit Banner";
    $this->data['edit'] = $this->Content_model->getBanner($id);
    $this->data['titlecontent'] = "Edit Banner";
    $this->data['class'] = "Banner";
    $this->data['path'] = "content/editcontent/banner/".$id;
    $this->data['flag'] = true;
    $this->data['tipe'] = array("partner","link");
  }else if($this->uri->segment(4)=="slider"){
    $this->data['btn'] = "Edit Slider";
    $this->data['class'] = "Slider";
    $this->data['path'] = "content/editcontent/slider/".$id;
    $this->data['edit'] = $this->Content_model->getSlider($id);
    $this->data['titlecontent'] = "Edit Slider";
    $this->data['flag'] = false;
  }else{
    redirect('admin/Dashboard');
  }
    if($this->_validate($access,$this->data['flag'])){
      $this->_action($access,$this->data['flag'],$id);
    }else{
      $this->load->view('templates/admin/header',$this->data);
      $this->load->view('admin/action_content',$this->data);
      $this->load->view('templates/admin/footer');
  }
  }
  public function deletecontent($id){
    $id = $this->uri->segment(5);
    if($this->uri->segment(4)=="banner"){
      $query = $this->Content_model->deletebanner($id);
      $path= 'admin/content/banner';
    }else if($this->uri->segment(4)=="slider"){
      $query = $this->Content_model->deleteslider($id);
      $path= 'admin/content/slider';
    }
    if($query){
      $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
      Successfully Deleting Item</div>');
      redirect($path);
    }else{
      $this->session->set_flashdata('message','<div class="alert alert-dager" role="alert">
      Unsuccessfully Deleting item</div>');
      redirect($path);
    }
  }
  private function _action($access,$flag,$id){
    $nama = $this->input->post('nama');
    $j = $this->input->post('judul');
    $l = $this->input->post('link');
    $k = $this->input->post('ket');
    $t =$this->input->post('tipe');

      if($access){ // jika edit
        if($flag){ // jika gambar itu banner or slider
          if (!empty($_FILES["foto"]["name"])) {
              $g = $this->Content_model->uploadImage($nama,$flag);
          } else {
              $g = $this->input->post('old_foto');
          }
        }else{
          if (!empty($_FILES["foto"]["name"])) {
              $g = $this->Content_model->uploadImage($j,$flag);
          } else {
              $g = $this->input->post('old_foto');
          }
        }

        if($g != "default.jpg"){
          if($flag){ // banner
            if($this->Content_model->updateBanner($nama,$g,$l,$k,$t,$id)){
              $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
              Successfully Editing Banner</div>');
              redirect('admin/Content/banner');
            }else{
              $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
              Failed to Edit Banner!</div>');
              redirect('admin/Content/banner');
            }
          }else{ // slider
            if($this->Content_model->updateSlider($j,$k,$g,$id)){
              $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
              Successfully Editing Slider</div>');
              redirect('admin/Content/slider');
            }else{
              $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
              Failed to Edit Slider!</div>');
              redirect('admin/Content/slider');
            }
          }

        }else{
          if($flag){
            $path = 'admin/content/banner';
          }else{
            $path = 'admin/content/slider';
          }
          $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
          Something Wrong While Uploading IMG!</div>');
          redirect($path);
          }
      }else{ // jika input
          if($flag){
            $g = $this->Content_model->uploadImage($nama,$flag);
          }else{
            $g = $this->Content_model->uploadImage($j,$flag);
          }

          if($g != "default.jpg"){
            if($flag){ // banner
              if($this->Content_model->insertBanner($nama,$g,$l,$k,$t)){
                $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
                Successfully Adding Banner</div>');
                redirect('admin/Content/banner');
              }else{
                $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
                Failed to Edit Adding!</div>');
                redirect('admin/Content/banner');
              }
            }else{ // slider
              if($this->Content_model->insertSlider($j,$k,$g)){
                $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
                Successfully Adding Slider</div>');
                redirect('admin/Content/slider');
              }else{
                $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
                Failed to Add Slider!</div>');
                redirect('admin/Content/slider');
              }
            }
          }else{
            if($flag){
              $path = 'admin/content/banner';
            }else{
              $path = 'admin/content/slider';
            }
            $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
            Something Wrong While Uploading IMG!</div>');
            redirect($path);
            }
      }

  }

  private function _validate($access,$flag){
  if($flag){ // banner
    $this->form_validation->set_rules('nama','Nama Banner','trim|required');
    $this->form_validation->set_rules('link','Link Banner','trim|required');
    $this->form_validation->set_rules('tipe','Tipe Banner','required|callback_check_default_tipe');
    $this->form_validation->set_message('check_default_tipe', 'Please something other than the Tipe');
  }else{ // slider
    $this->form_validation->set_rules('judul','Judul Slider','trim|required');
    }
    $this->form_validation->set_rules('ket','Keterangan','trim|required');
    if(!$access){ // edit
      if (empty($_FILES['foto']['name']))
      {
          $this->form_validation->set_rules('foto', 'Foto', 'required');
      }
    }
    // validation
    if($this->form_validation->run()==false){
      return false;
    }else{
      return true;
    }
  }

  function check_default_tipe($post_string)
  {
    return $post_string == '0' ? FALSE : TRUE;
  }
}
?>
