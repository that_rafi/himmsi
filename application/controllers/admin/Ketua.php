<?php
class Ketua extends CI_Controller{
  private $data = null,$id = null;

  public function __construct(){
    parent::__construct();
    $this->load->helper('url');
    $this->load->database();
    $this->load->library('form_validation');
    $this->load->model('Admin_model');
    $this->load->model('Ketua_model');
    $this->id =$this->session->userdata('id_user');
    if($this->id != NULL){
      $this->data['profile'] =$this->Admin_model->getAdmin($this->id);
      $this->data['akses'] = $this->Admin_model->getAkses($this->data['profile']['id_jabatan']);
      if($this->data['akses']['m_prokermanage'] != 1){
        redirect('admin/Dashboard');
      }
    }else{
      redirect('admin/Dashboard');
    }
  }
  public function index(){
      $this->data['header'] = array("ID Proker","Proker Name","Detail","Status","Price","Date Start","Date Held","Position","Action");
      $this->data['Allproker'] = $this->Ketua_model->getAllProker();
      $this->data['tabletitle'] = "Proker";
        $this->load->view('templates/admin/header',$this->data);
        $this->load->view('admin/table_data',$this->data);
        $this->load->view('templates/admin/footer');
  }

  public function updatestatus($id){
    $this->form_validation->set_rules('status','Status','required|callback_check_default_status');
    $this->form_validation->set_message('check_default_status', 'Please select something other than Status');
    if($this->form_validation->run()==false){
      $this->data['result'] = $this->Ketua_model->getProker($id);
      $this->data['status']= array("disetujui","ditolak","menunggu");
      $this->data['title'] = "Edit Status Proker";
      $this->data['path'] ="ketua/updatestatus/";
      $this->data['home'] = "artikel";
      $this->load->view('templates/admin/header',$this->data);
      $this->load->view('admin/ubah_status',$this->data);
      $this->load->view('templates/admin/footer');
    }else{
      $this->_update($id);
    }
  }
  public function deletestatus($id){
    if($this->Ketua_model->deleteProker($id)){
      $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
      Successfully Deleting Proker</div>');
      redirect('admin/Ketua/');
    }else{
      $this->session->set_flashdata('message','<div class="alert alert-dager" role="alert">
      Unsuccessfully Deleting Proker</div>');
      redirect('admin/Ketua/');
    }
  }
  private function _update($id){
    $status = $this->input->post('status');
    if($this->Ketua_model->updateStatus($id,$status)){
      $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
      Successfully Updating Status</div>');
      redirect('admin/Ketua/');
    }else{
      $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
      Unsuccessfully Updating Status!</div>');
      redirect('admin/Ketua/updateStatus/'.$id);
    }
  }

  function check_default_status($post_string)
  {
    return $post_string == '0' ? FALSE : TRUE;
  }
}
?>
