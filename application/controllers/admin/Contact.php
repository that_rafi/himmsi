<?php
class Contact extends CI_Controller{
  private $data = null,$id = null;

  public function __construct(){
    parent::__construct();
    $this->load->helper('url');
    $this->load->database();
    $this->load->library('form_validation');
    $this->load->model('Relasi_model');
    $this->load->model('Content_model');
    $this->load->model('Pengurus_model');
    $this->id =$this->session->userdata('id_user');
    if($this->id != NULL){
      $this->data['profile'] =$this->data['profile'] =$this->Pengurus_model->getPengurus($this->id);
      $this->data['akses'] = $this->Pengurus_model->getAkses($this->data['profile']['id_jabatan']);
      if($this->data['akses']['m_contact'] != 1){
        redirect('admin/Dashboard');
      }
    }else{
      redirect('admin/Dashboard');
    }
  }
  public function index(){
    $this->data['title'] = "Edit Contact";
    $this->data['edit'] = $this->Content_model->getAllContact();
    if(!$this->_validate()){
      $this->load->view('templates/admin/header',$this->data);
      $this->load->view('admin/edit_contact',$this->data);
      $this->load->view('templates/admin/footer');
    }else{
      $this->_update(1);
    }
  }

  private function _validate(){
    $this->form_validation->set_rules('addname','Nama ALamat','trim|required');
    $this->form_validation->set_rules('add1','Alamat 1','trim|required');
    $this->form_validation->set_rules('email','Email','trim|required|valid_email');
    $this->form_validation->set_rules('cn1','Nama Kontak 1','trim|required');
    $this->form_validation->set_rules('c1','Alamat 1','trim|required');
    $this->form_validation->set_rules('link1','Link Youtube','trim|required');
    $this->form_validation->set_rules('link2','Link Facebook','trim|required');
    $this->form_validation->set_rules('link3','Link IG','trim|required');
    if($this->form_validation->run()==false){
      return false;
    }else{
      return true;
    }
  }
  private function _update($id){
    $addname = $this->input->post('addname');
    $add1 = $this->input->post('add1');
    $add2 = $this->input->post('add2');
    $add3 = $this->input->post('add3');
    $email = $this->input->post('email');
    $cn1 = $this->input->post('cn1');
    $c1 = $this->input->post('c1');
    $cn2 = $this->input->post('cn2');
    $c2 = $this->input->post('c2');
    $linky = $this->input->post('link1');
    $linkf= $this->input->post('link2');
    $linki = $this->input->post('link3');
            if($this->Relasi_model->updateContact($addname,$add1,$add2,$add3,$email,$cn1,$c1,$cn2,$c2,$linky,$linkf,$linki,$id)){
              $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
              Successfully Updating Admin</div>');
              redirect("admin/Contact/");
            }else{
              $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
              Failed to Update Admin!</div>');
              redirect('admin/Contact/');
            }
    }

}
?>
