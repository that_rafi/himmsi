<?php
class Proker extends CI_Controller{
  private $data = null,$id = null;

  public function __construct(){
    parent::__construct();
    $this->load->helper('url');
    $this->load->database();
    $this->load->library('form_validation');
    $this->load->model('Pengurus_model');
    $this->id =$this->session->userdata('id_user');
    if($this->id != NULL){
      $this->data['profile'] =$this->data['profile'] =$this->Pengurus_model->getPengurus($this->id);
      $this->data['akses'] = $this->Pengurus_model->getAkses($this->data['profile']['id_jabatan']);
      if($this->data['akses']['m_proker'] != 1){
        redirect('admin/Dashboard');
      }
      if($this->data['profile']['id_jabatan'] == 2){
        $this->data['tabletitle'] = "Proker Wakil";
        $this->data['titleproker'] = "Add Proker Wakil";
      }else if($this->data['profile']['id_jabatan'] == 3 || $this->data['profile']['id_jabatan'] ==12){
        $this->data['tabletitle'] = "Proker Sekertaris";
        $this->data['titleproker'] = "Add Proker Sekertaris";
      }else if($this->data['profile']['id_jabatan'] == 4){
        $this->data['tabletitle'] = "Proker Ristek";
        $this->data['titleproker'] = "Add Proker Ristek";
      }else if($this->data['profile']['id_jabatan'] == 5){
        $this->data['tabletitle'] = "Proker Keuangan";
        $this->data['titleproker'] = "Add Proker Keuangan";
      }else if($this->data['profile']['id_jabatan'] == 6){
        $this->data['tabletitle'] = "Proker Internal";
        $this->data['titleproker'] = "Add Proker Internal";
      }else if($this->data['profile']['id_jabatan'] == 7){
        $this->data['tabletitle'] = "Proker Aspirasi";
        $this->data['titleproker'] = "Add Proker Aspirasi";
      }else if($this->data['profile']['id_jabatan'] == 8){
        $this->data['tabletitle'] = "Proker Eksternal";
        $this->data['titleproker'] = "Add Proker Eksternal";
      }else{
        redirect('admin/Dashboard');
      }
    }else{
      redirect('admin/Dashboard');
    }

}

public function index(){
      $this->data['header'] = array("ID Proker","Proker Name","Detail","Status","Price (in Rp)","Date Start","Date Held","Position","Action");
      $this->data['proker'] = $this->Pengurus_model->getProkerPengurus($this->id);
        $this->load->view('templates/admin/header',$this->data);
        $this->load->view('admin/table_data',$this->data);
        $this->load->view('templates/admin/footer');

}
public function inputproker(){

  if($this->validate()){
    $this->_action($idp);
  }else{
    $this->data['pic'] = "cats_black.jpg";
    $this->data['btn'] = "Add Proker";
    $this->data['edit'] = $this->Pengurus_model->getProker("0");
    $this->data['path'] = "proker/inputproker/";
    $this->load->view('templates/admin/header',$this->data);
    $this->load->view('admin/action_proker',$this->data);
    $this->load->view('templates/admin/footer');
  }
}
public function updateproker($idp){

  if($this->validate()){
    $this->_action($idp);
  }else{
    $this->data['pic'] = "cats_black2.jpg";
    $this->data['btn'] = "Update Proker";
    $this->data['edit'] = $this->Pengurus_model->getProker($idp);
    $this->data['path'] = "proker/updateproker/".$idp;
    $this->data['titleproker'] = "Edit Proker proker";
    $this->load->view('templates/admin/header',$this->data);
    $this->load->view('admin/action_proker',$this->data);
    $this->load->view('templates/admin/footer');
  }
}

  public function deleteproker($id){
    if($this->Pengurus_model->deleteProker($id)){
      $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
      Successfully Deleting Proker</div>');
      redirect('admin/Proker/');
    }else{
      $this->session->set_flashdata('message','<div class="alert alert-dager" role="alert">
      Unsuccessfully Deleting Proker</div>');
      redirect('admin/Proker/');
    }
  }

  private function _action($idp){
    $nama = $this->input->post('nama');
    $ket = $this->input->post('ket');
    $price = $this->input->post('price');
    $date1 = $this->input->post('date1');
    $date2 = $this->input->post('date2');
    if($idp != null){
      if($this->Pengurus_model->updateProkerPengurus($nama,$ket,$price,$date1,$date2,$idp)){
        $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
        Successfully Updating Proker proker</div>');
        redirect('admin/Proker/');
      }else{
        $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
        Unsuccessfully Updating Proker proker</div>');
        redirect('admin/Proker/');
      }
    }else{
      if($this->Pengurus_model->insertProker($nama,$ket,$price,$date1,$date2,$this->id)){
        $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
        Successfully Adding Proker proker</div>');
        redirect('admin/Proker/');
      }else{
        $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
        Unsuccessfully Adding Proker proker</div>');
        redirect('admin/Proker/');
      }
    }

  }
  public function validate(){
    $this->form_validation->set_rules('nama','Proker Name','trim|required');
    $this->form_validation->set_rules('ket','Detail Proker','trim|required');
    $this->form_validation->set_rules('price','Proker Price','trim|required');
    $this->form_validation->set_rules('date1','Start Date','trim|required');
    $this->form_validation->set_rules('date2','Implementation Date','trim|required');
    if($this->form_validation->run()==false){
      return false;
    }else{
      return true;
    }
  }

  function check_default_status($post_string)
  {
    return $post_string == '0' ? FALSE : TRUE;
  }
}
?>
