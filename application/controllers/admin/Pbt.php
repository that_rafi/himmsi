<?php
class Pbt extends CI_Controller{
  private $data = null,$id = null;

  public function __construct(){
    parent::__construct();
    $this->load->helper('url');
    $this->load->database();
    $this->load->library('form_validation');
    $this->load->library('pagination');
    $this->load->model('Pengurus_model');
    $this->load->model('Pbt_model');
    $this->data['jurusan'] = $this->Pbt_model->getJurusan();
    $this->id =$this->session->userdata('id_user');
    if($this->id != NULL){
      $this->data['profile'] =$this->data['profile'] =$this->Pengurus_model->getPengurus($this->id);
      $this->data['akses'] = $this->Pengurus_model->getAkses($this->data['profile']['id_jabatan']);
      if($this->data['akses']['m_pbtmateri'] != 1){
        redirect('admin/Dashboard');
      }
      if($this->data['akses']['m_pesertapbt'] != 1){
        redirect('admin/Dashboard');
      }
    }else{
      redirect('admin/Dashboard');
    }

  }
  public function index(){
    $this->materi();
  }
  public function materi(){
    // pagination
    $config['base_url'] = "http://himmsi.org/admin/pbt/materi/";
    $this->data['start'] = $this->uri->segment(4);
    $queryc = $this->Pbt_model->getCountAllPbt();
    $config['total_rows'] = $queryc;
    $config['per_page'] = 8;
    $this->pagination->initialize($config);
    $this->data['pbt'] = $this->Pbt_model->getAllPbt($config['per_page'],$this->data['start']);
    // end pagination
    $this->data['header'] = array("No","Matakuliah","Jurusan","Mhs Semester","Keterangan","Tanggal","Video Pbt","Action");
    $this->data['flag'] = true;
    $this->data['tabletitle'] = "Materi";
    $this->data['iduser'] = $this->data['profile']['id_jabatan'];
      $this->load->view('templates/admin/header',$this->data);
      $this->load->view('admin/table_pbt',$this->data);
      $this->load->view('templates/admin/footer');
  }
  public function peserta(){
    // pagination
    $config['base_url'] = "http://himmsi.org/admin/pbt/peserta/";
    $this->data['start'] = $this->uri->segment(4);
    $queryc = $this->Pbt_model->getCountAllPesertaPbt();
    $config['total_rows'] = $queryc;
    $config['per_page'] = 20;
    $this->pagination->initialize($config);
    $this->data['pbt'] = $this->Pbt_model->getAllPesertaPbt($config['per_page'],$this->data['start']);
    // end pagination
    $this->data['header'] = array("No","Nama","Nim","Jurusan","No Hp","Tanggal","Action");
    $this->data['flag'] = false;
    $this->data['tabletitle'] = "Peserta";
    $this->data['iduser'] = $this->data['profile']['id_jabatan'];
      $this->load->view('templates/admin/header',$this->data);
      $this->load->view('admin/table_pbt',$this->data);
      $this->load->view('templates/admin/footer');
  }
public function inputpbt(){
    $this->data['pic'] = "cats_black.jpg";
    $access = false;
  if($this->uri->segment(4)=="materi"){
    $this->data['btn'] = "Add Materi";
    $this->data['edit'] = $this->Pbt_model->getPbt("0");
    $this->data['titlepbt'] = "Add Materi";
    $this->data['class'] = "Materi";
    $this->data['keterangan'] = array('UAS','UTS');
    $this->data['semester'] = array("1","2","3","4","5");
    $this->data['path'] = "pbt/inputpbt/materi";
    $this->data['flag'] = true;
    $this->data['tipe'] = array("partner","link");
  }else if($this->uri->segment(4)=="peserta"){
    $this->data['btn'] = "Add Peserta";
    $this->data['class'] = "Peserta";
    $this->data['path'] = "pbt/inputpbt/peserta";
    $this->data['edit'] = $this->Pbt_model->getPesertaPbt("0");
    $this->data['titlepbt'] = "Add Peserta";
    $this->data['flag'] = false;
  }else{
    redirect('admin/Dashboard');
  }
    if($this->_validate($access,$this->data['flag'])){
      $id ="0";
      $this->_action($access,$this->data['flag'],$id);
    }else{
      $this->load->view('templates/admin/header',$this->data);
      $this->load->view('admin/action_pbt',$this->data);
      $this->load->view('templates/admin/footer');
  }


}
  public function editpbt($id){
    $this->data['pic'] = "cats_black.jpg";
    $access = true;
    $id = $this->uri->segment(5);
    if($id == null){
      redirect('admin/Dashboard');
    }
  if($this->uri->segment(4)=="materi"){
    $this->data['btn'] = "Edit Materi";
    $this->data['semester'] = array("1","2","3","4","5");
    $this->data['edit'] = $this->Pbt_model->getPbt($id);
    $this->data['titlepbt'] = "Edit materi";
    $this->data['class'] = "Materi";
    $this->data['keterangan'] = array('UAS','UTS');
    $this->data['path'] = "pbt/editpbt/materi/".$id;
    $this->data['flag'] = true;
  }else if($this->uri->segment(4)=="peserta"){
    $this->data['btn'] = "Edit Peserta";
    $this->data['class'] = "Peserta";
    $this->data['path'] = "pbt/editpbt/peserta/".$id;
    $this->data['edit'] = $this->Pbt_model->getPesertaPbt($id);
    $this->data['titlepbt'] = "Edit peserta";
    $this->data['flag'] = false;
  }else{
    redirect('admin/Dashboard');
  }
    if($this->_validate($access,$this->data['flag'])){
      $this->_action($access,$this->data['flag'],$id);
    }else{
      $this->load->view('templates/admin/header',$this->data);
      $this->load->view('admin/action_pbt',$this->data);
      $this->load->view('templates/admin/footer');
  }
  }
  public function deletepbt($id){
    $id = $this->uri->segment(5);
    if($this->uri->segment(4)=="materi"){
      $query = $this->Pbt_model->deletePbt($id);
      $path= 'admin/pbt/materi';
    }else if($this->uri->segment(4)=="peserta"){
      $query = $this->Pbt_model->deletePesertaPbt($id);
      $path= 'admin/pbt/peserta';
    }
    if($query){
      $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
      Successfully Deleting Item</div>');
      redirect($path);
    }else{
      $this->session->set_flashdata('message','<div class="alert alert-dager" role="alert">
      Unsuccessfully Deleting Item</div>');
      redirect($path);
    }
  }
  private function _action($access,$flag,$id){
    $nama = $this->input->post('nama');
    $nim = $this->input->post('nim');
    $j = $this->input->post('jurusan');
    $l = str_replace("/view?usp=sharing","",$this->input->post('video'));
    $k = $this->input->post('ket');
    $matkul =$this->input->post('matakuliah');
    $mhss = $this->input->post('mhs_semester');
    $tgl = $this->input->post('date1');
    $no = $this->input->post('nohp');

      if($access){ // jika edit
          if($flag){ // materi
            if($this->Pbt_model->updatePbt($matkul,$j,$mhss,$k,$tgl,$l,$id)){
              $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
              Successfully Editing Materi</div>');
              redirect('admin/Pbt/materi');
            }else{
              $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
              Failed to Edit Materi!</div>');
              redirect('admin/Pbt/materi');
            }
          }else{ // peserta
            if($this->Pbt_model->updatePesertaPbt($nama,$nim,$j,$tgl,$no,$id)){
              $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
              Successfully Editing Peserta</div>');
              redirect('admin/Pbt/peserta');
            }else{
              $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
              Failed to Edit Peserta!</div>');
              redirect('admin/Pbt/peserta');
            }
          }
      }else{ // jika input
            if($flag){ // materi
              if($this->Pbt_model->insertPbt($matkul,$j,$mhss,$k,$tgl,$l)){
                $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
                Successfully Adding Materi</div>');
                redirect('admin/Pbt/materi');
              }else{
                $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
                Failed to Edit Adding!</div>');
                redirect('admin/Pbt/materi');
              }
            }else{ // peserta
              if($this->Pbt_model->insertPesertaPbt($nama,$nim,$j,$tgl,$no)){
                $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
                Successfully Adding Peserta</div>');
                redirect('admin/Pbt/peserta');
              }else{
                $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
                Failed to Add Peserta!</div>');
                redirect('admin/Pbt/peserta');
              }
            }
      }

  }

  private function _validate($access,$flag){
    $this->form_validation->set_rules('date1','Tanggal','trim|required');
  if($flag){ // materi
    $this->form_validation->set_rules('matakuliah','Matakuliah','trim|required');
    $this->form_validation->set_rules('ket','Keterangan','required|callback_check_default_ket');
    $this->form_validation->set_message('check_default_ket', 'Please something other than the Keterangan');
    $this->form_validation->set_rules('mhs_semester','Semester','required|callback_check_default_mhss');
    $this->form_validation->set_message('check_default_mhss', 'Please something other than the Semester');
  }else{ // peserta
    $this->form_validation->set_rules('nama','Nama Peserta','trim|required');
    $this->form_validation->set_rules('nim','Nim Peserta','trim|required');
    }
    $this->form_validation->set_rules('jurusan','Jurusan','required|callback_check_default_jur');
    $this->form_validation->set_message('check_default_jur', 'Please something other than the Jurusan');
    // validation
    if($this->form_validation->run()==false){
      return false;
    }else{
      return true;
    }
  }

  function check_default_ket($post_string)
  {
    return $post_string == '0' ? FALSE : TRUE;
  }
  function check_default_mhss($post_string)
  {
    return $post_string == '0' ? FALSE : TRUE;
  }
  function check_default_jur($post_string)
  {
    return $post_string == '0' ? FALSE : TRUE;
  }
}
?>
