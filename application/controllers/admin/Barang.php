<?php
class Barang extends CI_Controller{
  private $data = null,$id = null;

  public function __construct(){
    parent::__construct();
    $this->load->helper('url');
    $this->load->database();
    $this->load->library('form_validation');
    $this->load->library('pagination');
    $this->load->model('Pengurus_model');
    $this->load->model('Sekertaris_model');
    $this->id =$this->session->userdata('id_user');
    if($this->id != NULL){
      $this->data['profile'] =$this->data['profile'] =$this->Pengurus_model->getPengurus($this->id);
      $this->data['akses'] = $this->Pengurus_model->getAkses($this->data['profile']['id_jabatan']);
      if($this->data['akses']['m_inventaris'] != 1){
        redirect('admin/Dashboard');
      }
      if($this->data['akses']['m_pinjambarang'] != 1){
        redirect('admin/Dashboard');
      }
    }else{
      redirect('admin/Dashboard');
    }
  }
  public function index(){
      $this->data['header'] = array("ID Barang","Nama Barang","Jumlah","Gambar","Keterangan","Harga","Tipe","Action");
      $this->data['AllBarang'] = $this->Sekertaris_model->getAllBarang();
      $this->data['tabletitle'] = "Barang";
        $this->load->view('templates/admin/header',$this->data);
        $this->load->view('admin/table_inventaris',$this->data);
        $this->load->view('templates/admin/footer');

}

public function inputbarang(){
  $this->data['pic'] = "cats_black.jpg";
  $this->data['btn'] = "Add Barang";
  $this->data['edit'] = $this->Sekertaris_model->getBarang("0");
  $this->data['path'] = "barang/inputbarang/";
  $this->data['titleproker'] = "Add Barang";
  $this->data['tipe'] = array("inventaris","jual");
  $access = false;
  if($this->_validate($access)){
    $id ="0";
    $this->_action($access,$id);
  }else{
    $this->load->view('templates/admin/header',$this->data);
    $this->load->view('admin/action_sekertaris',$this->data);
    $this->load->view('templates/admin/footer');
  }
}

  public function editbarang($id){
    $this->data['pic'] = "cats_black.jpg";
    $this->data['btn'] = "Edit Barang";
    $this->data['edit'] = $this->Sekertaris_model->getBarang($id);
    $this->data['path'] = "barang/editbarang/".$id;
    $this->data['titleproker'] = "Edit Barang";
    $this->data['tipe'] = array("inventaris","jual");
    $access = true;
    if($this->_validate($access)){
      $this->_action($access,$id);
    }else{
      $this->load->view('templates/admin/header',$this->data);
      $this->load->view('admin/action_sekertaris',$this->data);
      $this->load->view('templates/admin/footer');
    }
  }
  public function deletebarang($id){
    if($this->Sekertaris_model->deleteBarang($id)){
      $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
      Successfully Deleting Barang</div>');
      redirect('admin/Barang/');
    }else{
      $this->session->set_flashdata('message','<div class="alert alert-dager" role="alert">
      Unsuccessfully Deleting Barang</div>');
      redirect('admin/Barang/');
    }
  }
  private function _action($access,$id){
      $nama = $this->input->post('nama');
      $j = $this->input->post('jml');
      $h = $this->input->post('hrg');
      $k = $this->input->post('ket');
      $t =$this->input->post('tipe');
      if($access){
        if (!empty($_FILES["foto"]["name"])) {
            $g = $this->Sekertaris_model->uploadImage($nama);
        } else {
            $g = $this->input->post('old_foto');
        }
        if($g != "default.jpg"){
                if($this->Sekertaris_model->updateBarang($nama,$j,$g,$k,$h,$t,$id)){
                  $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
                  Successfully Editing Barang</div>');
                  redirect('admin/Barang/');
                }else{
                  $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
                  Failed to Edit Barang!</div>');
                  redirect('admin/Barang/');
                }
        }else{
          $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
          Something Wrong While Uploading IMG!</div>');
          redirect('admin/Barang');
          }
      }else{
          $g = $this->Sekertaris_model->uploadImage($nama);
          if($g != "default.jpg"){
                  if($this->Sekertaris_model->insertBarang($nama,$j,$g,$k,$h,$t)){
                    $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
                    Successfully Adding Barang</div>');
                    redirect('admin/Barang/');
                  }else{
                    $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
                    Failed to Add Barang!</div>');
                    redirect('admin/Barang/');
                  }
          }else{
            $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
            Something Wrong While Uploading IMG!</div>');
            redirect('admin/Barang');
            }
      }

  }

  private function _validate($access){
    $this->form_validation->set_rules('nama','Full Name','trim|required');
    if(!$access){
      if (empty($_FILES['foto']['name']))
      {
          $this->form_validation->set_rules('foto', 'Foto Barang', 'required');
      }
    }
    $this->form_validation->set_rules('nama','Nama Barang','trim|required');
    $this->form_validation->set_rules('jml','Jumlah Barang','trim|required');
    $this->form_validation->set_rules('tipe','Tipe Barang','required|callback_check_default_tipe');
    $this->form_validation->set_message('check_default_tipe', 'Please something other than the Tipe');
    $this->form_validation->set_rules('ket','Keterangan','trim|required');
    $this->form_validation->set_rules('hrg','Harga Barang','trim|required');

    if($this->form_validation->run()==false){
      return false;
    }else{
      return true;
    }
  }

  function check_default_tipe($post_string)
  {
    return $post_string == '0' ? FALSE : TRUE;
  }

  // pinjaman Barang

  public function pinjaman(){
      $this->data['header'] = array("ID Pinjaman","Nama Barang","Nama Peminjam","Tgl Pinjam","Tgl Kembali","Keadaan Pinjam","Keadaan Kembali","Jml Pinjam","Jml Kembali","Action");
      $this->data['tabletitle'] = "Peminjaman Inventaris";
      // pagination
      $config['base_url'] = "http://himmsi.org/admin/barang/pinjaman/index";
      $this->data['start'] = $this->uri->segment(4);
      $queryc = $this->Sekertaris_model->getCountAllPinjaman();
      $config['total_rows'] = $queryc;
      $config['per_page'] = 8;
      $this->pagination->initialize($config);
      $this->data['pinjaman'] = $this->Sekertaris_model->getAllPinjaman($config['per_page'],$this->data['start']);
      //
        $this->load->view('templates/admin/header',$this->data);
        $this->load->view('admin/table_pinjam',$this->data);
        $this->load->view('templates/admin/footer');

  }
  public function inputpinjaman(){
    $this->data['pic'] = "cats_black.jpg";
    $this->data['btn'] = "Add Pinjaman";
    $this->data['edit'] = $this->Sekertaris_model->getPinjam("0");
    $this->data['path'] = "barang/inputpinjaman/";
    $this->data['title'] = "Add Pinjaman";
    $this->data['barang'] = $this->Sekertaris_model->getAllBarang();
    $access = false;
    if($this->_validate_pinjam()){
      $id ="0";
      $this->_action_pinjam($access,$id);
    }else{
      $this->load->view('templates/admin/header',$this->data);
      $this->load->view('admin/action_pinjam',$this->data);
      $this->load->view('templates/admin/footer');
    }
  }
  public function editpinjaman($id){
    $this->data['pic'] = "cats_black.jpg";
    $this->data['btn'] = "Edit Pinjaman";
    $this->data['edit'] = $this->Sekertaris_model->getPinjam($id);
    $this->data['path'] = "barang/editpinjaman/".$id;
    $this->data['title'] = "Edit Pinjaman";
    $this->data['barang'] = $this->Sekertaris_model->getAllBarang();
    $access = true;
    if($this->_validate_pinjam()){
      $this->_action_pinjam($access,$id);
    }else{
      $this->load->view('templates/admin/header',$this->data);
      $this->load->view('admin/action_pinjam',$this->data);
      $this->load->view('templates/admin/footer');
    }
  }
  public function deletePinjaman($id){
    if($this->Sekertaris_model->deletePinjaman($id)){
      $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
      Successfully Deleting Pinjaman</div>');
      redirect('admin/Barang/pinjaman');
    }else{
      $this->session->set_flashdata('message','<div class="alert alert-dager" role="alert">
      Unsuccessfully Deleting Pinjaman</div>');
      redirect('admin/Barang/pinjaman');
    }
  }
// validate
  private function _validate_pinjam(){
    $this->form_validation->set_rules('nama','Nama Peminjam','trim|required');
    $this->form_validation->set_rules('datep','Tanggal Pinjam','trim|required');
    //$this->form_validation->set_rules('datek','Tanggal Kembali','trim|required');
    $this->form_validation->set_rules('barang','Barang','required|callback_check_default_barang');
    $this->form_validation->set_message('check_default_barang', 'Please something other than Barang');
    $this->form_validation->set_rules('kpinjam','Keadaan Pinjam','trim|required');
    //$this->form_validation->set_rules('kkembali','Keadaan Kembali','trim|required');
    $this->form_validation->set_rules('jmlp','Jumlah Pinjam','trim|required');
    //$this->form_validation->set_rules('jmlk','Jumlah Kembali','trim|required');
    if($this->form_validation->run()==false){
      return false;
    }else{
      return true;
    }
  }

  function check_default_barang($post_string)
  {
    return $post_string == '0' ? FALSE : TRUE;
  }
  // end validate
  private function _action_pinjam($access,$id){
      $idb = $this->input->post('barang');
      $nama = $this->input->post('nama');
      $tglp = $this->input->post('datep');
      $tglk = $this->input->post('datek');
      $kp = $this->input->post('kpinjam');
      $kk = $this->input->post('kkembali');
      $jmlp =$this->input->post('jmlp');
      $jmlk =$this->input->post('jmlk');
      if($access){
                if($this->Sekertaris_model->updatePinjaman($idb,$nama,$tglp,$tglk,$kp,$kk,$jmlp,$jmlk,$id)){
                  $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
                  Successfully Editing Pinjaman</div>');
                  redirect('admin/Barang/pinjaman');
                }else{
                  $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
                  Failed to Edit Pinjaman!</div>');
                  redirect('admin/Barang/pinjaman');
                }
      }else{
              if($this->Sekertaris_model->insertPinjaman($idb,$nama,$tglp,$tglk,$kp,$kk,$jmlp,$jmlk)){
                  $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
                  Successfully Adding Pinjaman</div>');
                  redirect('admin/Barang/pinjaman');
              }else{
                  $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
                  Failed to Add Pinjaman!</div>');
                  redirect('admin/Barang/pinjaman');
              }
      }

  }



}
?>
