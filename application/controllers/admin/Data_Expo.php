<?php
class Data_Expo extends CI_Controller{
  private $data = null,$id = null;

  public function __construct(){
    parent::__construct();
    $this->load->helper('url');
    $this->load->database();
    $this->load->library('form_validation');
    $this->load->library('pagination');
    $this->load->model('Pengurus_model');
    $this->id =$this->session->userdata('id_user');
    if($this->id != NULL){
      $this->data['profile'] =$this->data['profile'] =$this->Pengurus_model->getPengurus($this->id);
      $this->data['akses'] = $this->Pengurus_model->getAkses($this->data['profile']['id_jabatan']);
      if($this->data['akses']['m_dataexpo'] != 1){
        redirect('admin/dashboard');
      }
    }else{
      redirect('admin/dashboard');
    }
  }
  public function index(){
      $this->data['header'] = array("ID Data Expo","nama","nim","kelas","gender","id Jurusan","tgl lahir","alamat asal","alamat sini","id jabatan", "No Wa","email","alasan","bayar","status","action");
      $config['base_url'] = "http://himmsi.org/admin/Data_Expo/index";
      $queryc = $this->Pengurus_model->getAllExpo()->num_rows();
      $config['total_rows'] = $queryc;
      $config['per_page'] = 20;
      $this->pagination->initialize($config);
      $this->data['start'] = $this->uri->segment(4);
      $this->data['AllDataExpo'] = $this->Pengurus_model->getDataExpo($config['per_page'],$this->data['start']);
      $this->data['tabletitle'] = "Data Expo";
        $this->load->view('templates/admin/header',$this->data);
        $this->load->view('admin/table_dataexpo',$this->data);
        $this->load->view('templates/admin/footer');
}


  public function update_status_expo($id){
    $this->data['edit'] = $this->Pengurus_model->getDataExpoById($id);
    $this->data['path'] = "Data_Expo/update_status_expo/".$id;
    $this->data['title'] = "Edit Status";
    $this->form_validation->set_rules('bayar','Jumlah uang yang dibayarkan','trim|required');
    $this->form_validation->set_rules('status','Status Pembayaran','required|callback_check_default_status');
    $this->form_validation->set_message('check_default_status', 'Please select something other than Status');
    if($this->form_validation->run() == false ){
      $this->load->view('templates/admin/header',$this->data);
      $this->load->view('admin/action_dataexpo',$this->data);
      $this->load->view('templates/admin/footer');
    }else{
      $this->_update($id);
    }
  }
  public function delete_expo($id){
    if($this->Pengurus_model->deleteExpo($id)){
      $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
      Successfully Deleting Peserta</div>');
      redirect('admin/Data_Expo/');
    }else{
      $this->session->set_flashdata('message','<div class="alert alert-dager" role="alert">
      Unsuccessfully Deleting Peserta</div>');
      redirect('admin/Data_Expo/');
    }
  }
  private function _update($id){
      $bayar = $this->input->post('bayar');
      $status = $this->input->post('status');
      if($this->Pengurus_model->updateStatusExpo($bayar,$status,$id)){
        $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
        Successfully Deleting Peserta</div>');
        redirect('admin/Data_Expo/');
      }else{
        $this->session->set_flashdata('message','<div class="alert alert-dager" role="alert">
        Unsuccessfully Deleting Peserta</div>');
        redirect('admin/Data_Expo/');
      }

  }

  function check_default_status($post_string)
  {
    return $post_string == '0' ? FALSE : TRUE;
  }



}
?>
