<?php
class Relasi extends CI_Controller{
  private $data = null,$id = null;

  public function __construct(){
    parent::__construct();
    $this->load->helper('url');
    $this->load->database();
    $this->load->library('form_validation');
    $this->load->model('Pengurus_model');
    $this->load->model('Relasi_model');
    $this->id =$this->session->userdata('id_user');
    if($this->id != NULL){
      $this->data['profile'] =$this->data['profile'] =$this->Pengurus_model->getPengurus($this->id);
      $this->data['akses'] = $this->Pengurus_model->getAkses($this->data['profile']['id_jabatan']);
      if($this->data['akses']['m_relasi'] != 1){
        redirect('admin/Dashboard');
      }
    }else{
      redirect('admin/Dashboard');
    }
  }
  public function index(){
    $this->data['header'] = array("ID Relation","Relation Name","Organization","Phone No","Email","Address","Major","University","Detail","Adder","Action");
    $this->data['relasi'] = $this->Relasi_model->getAllRelasi();
    $this->data['tabletitle'] = "Daftar Relasi HIMMSI";
    $this->data['iduser'] = $this->id;
      $this->load->view('templates/admin/header',$this->data);
      $this->load->view('admin/table_relasi',$this->data);
      $this->load->view('templates/admin/footer');
    }
  public function insertrelasi(){

    if($this->validaterelasi()){
      $this->_actionrelasi($idp);
    }else{
      $this->data['pic'] = "cats_together.jpg";
      $this->data['btn'] = "Add Relasi";
      $this->data['edit'] = $this->Relasi_model->getRelasi("0");
      $this->data['path'] = "relasi/insertrelasi/";
      $this->data['titleproker'] = "Add Relasi";
      $this->load->view('templates/admin/header',$this->data);
      $this->load->view('admin/action_relasi',$this->data);
      $this->load->view('templates/admin/footer');
    }
  }
  public function editrelasi($idp){

    if($this->validaterelasi()){
      $this->_actionrelasi($idp);
    }else{
      $this->data['pic'] = "cats_together.jpg";
      $this->data['btn'] = "Edit Relasi";
      $this->data['edit'] = $this->Relasi_model->getRelasi($idp);
      $this->data['path'] = "relasi/editrelasi/".$idp;
      $this->data['titleproker'] = "Edit Relasi";
      $this->load->view('templates/admin/header',$this->data);
      $this->load->view('admin/action_relasi',$this->data);
      $this->load->view('templates/admin/footer');
    }
  }
  public function validaterelasi(){
    $this->form_validation->set_rules('nama','Nama Relasi','trim|required');
    $this->form_validation->set_rules('org','Organisasi','trim|required');
    $this->form_validation->set_rules('no','Phone Number','trim|required');
    $this->form_validation->set_rules('email','Email','trim|required|valid_email');
    $this->form_validation->set_rules('address','Alamat','trim|required');
    $this->form_validation->set_rules('jurusan','Jurusan','trim|required');
    $this->form_validation->set_rules('kampus','Kampus','trim|required');
    $this->form_validation->set_rules('ket','Keterangan','trim|required');
    if($this->form_validation->run()==false){
      return false;
    }else{
      return true;
    }
  }
  private function _actionrelasi($idp){
    $nama = $this->input->post('nama');
    $ket = $this->input->post('ket');
    $org = $this->input->post('org');
    $no = $this->input->post('no');
    $email = $this->input->post('email');
    $add = $this->input->post('address');
    $jur = $this->input->post('jurusan');
    $kampus = $this->input->post('kampus');
    if($idp != null){
      if($this->Relasi_model->updateRelasi($idp,$nama,$org,$no,$email,$add,$jur,$kampus,$ket)){
        $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
        Successfully Updating Relasi</div>');
        redirect('admin/Relasi/');
      }else{
        $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
        Unsuccessfully Updating Relasi</div>');
        redirect('admin/Relasi/');
      }
    }else{
      if($this->Relasi_model->insertRelasi($nama,$org,$no,$email,$add,$jur,$kampus,$ket,$this->id)){
        $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
        Successfully Adding Relasi</div>');
        redirect('admin/Relasi/');
      }else{
        $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
        Unsuccessfully Adding Relasi</div>');
        redirect('admin/Relasi/');
      }
    }

  }
  public function deleterelasi($id){
    if($this->Relasi_model->deleteRelasi($id)){
      $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
      Successfully Deleting Relasi</div>');
      redirect('admin/Relasi/');
    }else{
      $this->session->set_flashdata('message','<div class="alert alert-dager" role="alert">
      Unsuccessfully Deleting Relasi</div>');
      redirect('admin/Relasi/');
    }
  }

  function check_default_status($post_string)
  {
    return $post_string == '0' ? FALSE : TRUE;
  }
}
?>
