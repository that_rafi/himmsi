<?php
class Kontak extends CI_Controller{
  private $data=null ;
  public function __construct(){
    parent::__construct();
    $this->load->helper('url');
    $this->load->database();
    $this->load->library('form_validation');
    $this->load->library('recaptcha');
    $this->load->helper('form');
    $this->load->model('Content_model');
    $this->data['headerprofile'] = $this->Content_model->getArtikelProfileID();
    $this->data['title'] = "Himmsi Contact";
    $this->data['contact'] = $this->Content_model->getAllContact();
    $this->data['banner'] = $this->Content_model->getAllBanner();
  }
  public function index(){
    $data['title'] = "Himmsi Contact";
    // $map['js'];  kasih ke head
    // kasih ke body $map['html'];
    $this->load->helper('url');
    $this->load->view('templates/header',$this->data);
    $this->load->view('Kontak/index',$this->data);
    $this->load->view('templates/footer');
  }
}
?>
