<?php
class Akademik extends CI_Controller{
  private $data=null ;
  public function __construct(){
    parent::__construct();
    $this->load->helper('url');
    $this->load->database();
    $this->load->library('pagination');
    $this->load->library('form_validation');
    $this->load->helper('cookie');
    $this->load->library('recaptcha');
    $this->load->helper('form');
    $this->load->model('Aspirasi_model');
    $this->load->model('Content_model');
    $this->load->model('Pbt_model');
    $this->data['title'] = "Himmsi Homepage";
    $this->data['jurusan'] = $this->Aspirasi_model->getJurusan();
    $this->data['contact'] = $this->Content_model->getAllContact();
    $this->data['headerprofile'] = $this->Content_model->getArtikelProfileID();
    $this->data['banner'] = $this->Content_model->getAllBanner();
  }
  public function index(){
    $this->load->view('templates/header',$this->data);
    $this->load->view('Akademik/index',$this->data);
    $this->load->view('templates/footer');
  }
  public function form_aspirasi(){
    $this->form_validation->set_rules('nama','Nama Mahasiswa','trim|required');
    $this->form_validation->set_rules('nim','Nim Mahasiswa','trim|required');
    $this->form_validation->set_rules('aspirasi','Aspirasi Mahasiswa','trim|required');
    $this->form_validation->set_rules('email','Email Mahasiswa','trim|required|valid_email');
    $this->form_validation->set_rules('jurusan','Jurusan','required|callback_check_default_jurusan');
    $this->form_validation->set_message('check_default_jurusan', 'Please select something other than Jurusan');
    $this->form_validation->set_rules('angkatan','Angkatan','required|callback_check_default_angkatan');
    $this->form_validation->set_message('check_default_angkatan', 'Please select something other than Angkatan');
    //$captcha_answer = $this->input->post('g-recaptcha-response');
    //$response = $this->recaptcha->verifyResponse($captcha_answer);
      if($this->form_validation->run()==false ){ //&& $response['success']==false
        $this->data['pic'] = "aspirasi.jpg";
        $this->data['title'] = "Form Aspirasi";
        $this->data['angkatan'] = $this->Aspirasi_model->getAngkatan();
        $this->data['jmlang'] =  $this->Aspirasi_model->getJmlAngkatan();
        $this->load->view('templates/header',$this->data);
        $this->load->view('Form/form_aspirasi',$this->data);
        $this->load->view('templates/footer');
      }else{
       
        $secret = '6LeBx68UAAAAAC2GZHLEQK3VZV4nZtZVypV5GZz7';
        $ch = curl_init('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $verifyResponse = curl_exec($ch);
        $responseData = json_decode($verifyResponse);
        if($responseData->success)
             {
             $this->_input("aspirasi");
            }
        else
            {
            $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">Please Check Captcha!</div>');redirect('Akademik/form_aspirasi');
            }
        
        
          
        
      }
  }
  public function pbt_online(){
    $user = $this->input->cookie('userpbt',true);
    if($user == "" || $user ==null){
      $this->form_validation->set_rules('nama','Nama Mahasiswa','trim|required');
      $this->form_validation->set_rules('nim','Nim Mahasiswa','trim|required');
      $this->form_validation->set_rules('nohp','No Hp Mahasiswa','trim|required');
      $this->form_validation->set_rules('jurusan','Jurusan','required|callback_check_default_jurusan');
      $this->form_validation->set_message('check_default_jurusan', 'Please select something other than Jurusan');
        if($this->form_validation->run()==false ){ //&& $response['success']==false
          $this->data['pic'] = "pbt2.jpg";
          $this->data['title'] = "Form Peserta PBT";
          $this->load->view('templates/header',$this->data);
          $this->load->view('Form/form_pbt',$this->data);
          $this->load->view('templates/footer');
        }else{
            $secret = '6LeBx68UAAAAAC2GZHLEQK3VZV4nZtZVypV5GZz7';
            $ch = curl_init('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $verifyResponse = curl_exec($ch);
        $responseData = json_decode($verifyResponse);
        if($responseData->success)
             {
             $this->_input("pbt");
            }
        else
            {
            $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">Please Check Captcha!</div>');redirect('Akademik/pbt_online');
            }
          
        }
    }else{
      redirect('Akademik/materipbt');
    }

  }
  public function materipbt(){
    $user = $this->input->cookie('userpbt',true);
    if($user != "" || $user !=null){
      $this->load->view('templates/header',$this->data);
      // kategori by jurusan
      $this->data['jurusankat'] = $this->Pbt_model->getKategoriMateri("jurusan")->result_array();
      // kategori by mhs_semester
      $this->data['semesterkat'] = $this->Pbt_model->getKategoriMateri("mhs_semester")->result_array();
      $jurusan = $this->uri->segment(3);
      $mhs_s = trim($this->uri->segment(4),"smt");
      $this->data['major'] = $jurusan;
      $this->data['mhs_s'] = $mhs_s;
      $flag = false;$flag1 = false;;
      if($jurusan == null || $mhs_s == null){
          $this->load->view('Akademik/Pre_PBT',$this->data);
      }else{
        // validasi

        foreach($this->data['jurusankat'] as $m):
          $short = ($this->data['major'] == "MI") ? "Manajemen Informatika" : "Sistem Informasi";
          if($m['jurusan'] == $short){
              $flag = true;
          }
        endforeach;
        foreach($this->data['semesterkat'] as $s):
          if($s['mhs_semester'] == $mhs_s){
              $flag1 = true;
          }
        endforeach;

        if($flag1 && $flag){
          $perpage =2;
          // kategori by matkul
          $this->data['matkulkat'] = $this->Pbt_model->getDataForKategori("matakuliah",$short,$mhs_s)->result_array();
          // kategori by keterangan
          $this->data['ketkat'] = $this->Pbt_model->getDataForKategori("keterangan",$short,$mhs_s)->result_array();
          $tagmatkul = $this->validatekat($this->data['matkulkat'],'matakuliah',str_replace("%20"," ",($this->uri->segment(5))));
          $tagket = $this->validatekat($this->data['ketkat'],'keterangan',str_replace("%20"," ",($this->uri->segment(5))));
          if($tagmatkul){
            $data['start'] = $this->uri->segment(6);
            $queryc = $this->Pbt_model->getCountMateriByKategori($mhs_s,$short,'matakuliah',str_replace("%20"," ",($this->uri->segment(5))));
            $base =  "http://himmsi.org/akademik/materipbt/".$this->data['major']."/".$this->uri->segment(4)."/".$this->uri->segment(5);
            $query = $this->Pbt_model->getMateriByKategori($perpage,$data['start'],$mhs_s,$short,'matakuliah',str_replace("%20"," ",($this->uri->segment(5))));
          }elseif ($tagket) {
            $data['start'] = $this->uri->segment(6);
            $queryc = $this->Pbt_model->getCountMateriByKategori($mhs_s,$short,'keterangan',str_replace("%20"," ",($this->uri->segment(5))));
            $base =  "http://himmsi.org/akademik/materipbt/".$this->data['major']."/".$this->uri->segment(4)."/".$this->uri->segment(5);
            $query = $this->Pbt_model->getMateriByKategori($perpage,$data['start'],$mhs_s,$short,'keterangan',str_replace("%20"," ",($this->uri->segment(5))));
          }else{
            //pagination
            $data['start'] = $this->uri->segment(5);
            $queryc = $this->Pbt_model->getCountMateri($mhs_s,$short);
            $base =  "http://himmsi.org/akademik/materipbt/".$this->data['major']."/".$this->uri->segment(4);
            $query = $this->Pbt_model->getMateri($perpage,$data['start'],$mhs_s,$short);
          }
          $this->pagination->initialize($this->initPagination($perpage,$base,$queryc));
          // end of pagination
          $this->data['materi'] = $query;
          $this->load->view('Akademik/PBT',$this->data);
        }else if($flag && !$flag1){
         redirect('Akademik/materipbt/'.$this->data['major'].'/');
       }else{
         redirect('Akademik/materipbt/');
        }


      }
        $this->load->view('templates/footer');
    }else{
      $this->session->set_flashdata('message','<div class="alert alert-warning" role="alert">
      Registerasi dulu ya!!</div>');
      redirect('Akademik/pbt_online');
    }

  }
  public function banksoal(){
    $this->load->view('templates/header',$this->data);
    $this->load->view('Akademik/Banksoal',$this->data);
    $this->load->view('templates/footer');
  }

  // Utility
  public function validatekat($data,$field,$compare){
    $flag1 = false;
    foreach($data as $s):
      if($s[$field] == $compare){
          $flag1 = true;
      }
    endforeach;
    return $flag1;
  }
  private function _input($type){
    $nama = $this->input->post('nama');
    $nim = $this->input->post('nim');
    $e = $this->input->post('email');
    $tel = $this->input->post('tel');
    $j = $this->input->post('jurusan');
    $a = $this->input->post('angkatan');
    $aspirasi = $this->input->post('aspirasi');
    $tgl = date('y-m-d');
    $no = $this->input->post('nohp');
    if($type == "aspirasi"){
      if($this->Aspirasi_model->insertAspirasi($j,$a,$nama,$nim,$e,$tel,$aspirasi,$tgl)){
        $subject = "Aspirasi HIMMSI";
        $isi = "Terimakasih Telah Mengirim Aspirasi,Kami akan tidak lanjuti,ditunggu ya informasi selanjutnya";
        $pass = "ristekhimmsi";
        if($this->_sendemail($e,$subject,$isi,$pass)){
          $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
          Terimakasih Aspirasi anda telah terkirim ! silahkan tunggu balasan dari kami via email ya!</div>');
          redirect('Akademik/form_aspirasi');
        }else{
          $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
          Aspirasi tidak terkirim, silahkan coba beberapa saat lagi!</div>');
          redirect('Akademik/form_aspirasi');
        }
      }else{
        $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
        Aspirasi tidak terkirim, silahkan coba beberapa saat lagi!</div>');
        redirect('Akademik/form_aspirasi');
      }
    }else if($type == "pbt"){
      if($this->Pbt_model->insertPesertaPbt($nama,$nim,$j,$tgl,$no)){
         $cookie= array(
           'name'   => 'userpbt',
           'value'  => $nim,
           'expire' => '86400',
         );
         $this->input->set_cookie($cookie);
         redirect('Akademik/materipbt');
      }else{
        $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
        Unsuccessfully Registering User,Try Again!</div>');
        redirect('Akademik/pbt_online');
      }
    }
  }
  private function _sendemail($email,$subject,$isi,$pass){
    // Load PHPMailer library
        $this->load->library('phpmailer_lib');

        // PHPMailer object
        $mail = $this->phpmailer_lib->load();

        // SMTP configuration
        $mail->isSMTP();
        $mail->Host     = 'smtp.gmail.com';
        $mail->SMTPAuth = true;
        $mail->Password = $pass;
        $mail->SMTPSecure = 'ssl';
        $mail->Port     = 465;

        $mail->setFrom('himmsiwebsite@gmail.com', 'Aspirasi');
        $mail->addReplyTo('himmsiwebsite@gmail.com', 'Aspirasi');

        // Add a recipient
        $mail->addAddress($email);

        

        // Email subject
        $mail->Subject = $subject;

        // Set email format to HTML
        $mail->isHTML(true);

        // Email body content
        $mailContent = $isi;
        $mail->Body = $mailContent;

        // Send email
        if(!$mail->send()){
            return false; 
        }else{
            return true;
        }
    }
    
  

  function check_default_status($post_string)
  {
    return $post_string == '0' ? FALSE : TRUE;
  }
  function check_default_jurusan($post_string)
  {
    return $post_string == '0' ? FALSE : TRUE;
  }
  function check_default_angkatan($post_string)
  {
    return $post_string == '0' ? FALSE : TRUE;
  }

  function initPagination($perpage,$base,$query){
      $config['base_url'] = $base;
      $config['total_rows'] = $query;
      $config['per_page'] =$perpage;
      $config['first_link']       = 'First';
      $config['last_link']        = 'Last';
      $config['next_link']        = 'Next';
      $config['prev_link']        = 'Prev';
      $config['full_tag_open']    = '<div class="pagging text-center">  <nav class="blog-pagination justify-content-center d-flex"><ul class="pagination">';
      $config['full_tag_close']   = '</ul></nav></div>';
      $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
      $config['num_tag_close']    = '</span></li>';
      $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
      $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
      $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
      $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
      $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
      $config['prev_tagl_close']  = '</span>Next</li>';
      $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
      $config['first_tagl_close'] = '</span></li>';
      $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
      $config['last_tagl_close']  = '</span></li>';
      return $config;
 }



}
?>
