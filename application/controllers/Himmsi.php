<?php
class Himmsi extends CI_Controller{
  private $data=null ;
  public function __construct(){
    parent::__construct();
    $this->load->helper('url');
    $this->load->database();
    $this->load->library('pagination');
    $this->load->model('Content_model');
    $this->data['slider'] = $this->Content_model->getAllSlider();
    $this->data['title'] = "Himmsi Homepage";
    $this->data['agenda'] = $this->Content_model->getAllAgenda();
    $this->data['headerprofile'] = $this->Content_model->getArtikelProfileID();
       $this->data['kat'] = $this->Content_model->getAllKategori();
       $i = 0;
       foreach($this->data['kat'] as $k) :
         $this->data['count'][$i] = $this->Content_model->getCountKategori($k['id_kategori']);
         $i++;
       endforeach;
       $this->data['countall']=$this->Content_model->getCountAllPublishArtikel();
       $this->data['search'] = false;
       $this->data['contact'] = $this->Content_model->getAllContact();
       $this->data['banner'] = $this->Content_model->getAllBanner();
  }
  public function index(){
    $perpage = 8;
    // by kategori -- checking if kategori exists or no
    $kat=$this->uri->segment(3);
    $flag = false;
    $count = 0;
    foreach($this->data['kat'] as $k) :
      if($kat == $k['kategori']){
        $count = $this->Content_model->getCountKategori($k['id_kategori']);
        $flag = true;
      }
    endforeach;
    // execute
    if($flag){
      $data['start'] = $this->uri->segment(4);
      $base = "http://himmsi.org/himmsi/index/".$kat;
      $queryc = $count;
      $this->data['artikel'] = $this->Content_model->getArtikelByKategori($perpage,$data['start'],$kat);
    }else{
      // pagination
      $data['start'] = $this->uri->segment(3);
      $base = "http://himmsi.org/himmsi/index";
      $queryc = $this->Content_model->getCountAllPublishArtikel();
      $this->data['artikel'] = $this->Content_model->getAllPublishArtikel($perpage,$data['start']);
        // end pagination
    }
    $this->pagination->initialize($this->initPagination($perpage,$base,$queryc));

    $this->load->view('templates/header',$this->data);
    $this->load->view('Home/index',$this->data);
    $this->load->view('templates/footer');
  }



 function initPagination($perpage,$base,$query){
     $config['base_url'] = $base;
     $config['total_rows'] = $query;
     $config['per_page'] =$perpage;
     $config['first_link']       = 'First';
     $config['last_link']        = 'Last';
     $config['next_link']        = 'Next';
     $config['prev_link']        = 'Prev';
     $config['full_tag_open']    = '<div class="pagging text-center">  <nav class="blog-pagination justify-content-center d-flex"><ul class="pagination">';
     $config['full_tag_close']   = '</ul></nav></div>';
     $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
     $config['num_tag_close']    = '</span></li>';
     $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
     $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
     $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
     $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
     $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
     $config['prev_tagl_close']  = '</span>Next</li>';
     $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
     $config['first_tagl_close'] = '</span></li>';
     $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
     $config['last_tagl_close']  = '</span></li>';
     return $config;
}

  function Search(){
    $perpage = 8;
    $key = (trim($this->input->post('search',true)))? trim($this->input->post('search',true)) : '';
    $key =  ($this->uri->segment(3)) ? $this->uri->segment(3) : $key;
    $data['start'] = $this->uri->segment(4);
    $base = "http://himmsi.org/himmsi/Search/$key";
    $queryc = $this->Content_model->getCountSearchArtikel($key);
    $this->data['artikel'] = $this->Content_model->getSearchArtikel($perpage,$data['start'],$key);
    if($queryc == 0){
      $this->data['search'] = true;
    }

    $this->pagination->initialize($this->initPagination($perpage,$base,$queryc));
    $this->load->view('templates/header',$this->data);
    $this->load->view('Home/index',$this->data);
    $this->load->view('templates/footer');
  }


}
?>
